var $sp = (function(module) {
    var router;

    function authRequired(whenAuthFunc) {
        return function() {
            if ($sp.context.account) {
                whenAuthFunc && whenAuthFunc.apply(this, arguments);
            } else {
                $sp.router.navigate("signin", true);
            }
        };
    }

    router = Backbone.Router.extend({
        routes: {
            "": "index",
            "signup-for-beta": "signup",
            "device": "deviceHome",
            "device/:deviceId": "deviceHome",
            "device/:deviceId/photos": "devicePhotoAlbums",
            "device/:deviceId/photo-album(/*path)": "devicePhotoAlbum",
            "device/:deviceId/music": "deviceMusic",
            "device/:deviceId/documents": "deviceDocuments",
            "device/:deviceId/videos": "deviceVideos",
            "device/:deviceId/directory(/*path)": "deviceDirectory",
            "device/:deviceId/events": "deviceEvents",
            "device/:deviceId/settings": "deviceSettings",
            "download": "download",
            "settings": "settings",
            "how-to-use": "howToUse",
            "reset-password": "resetPasswd",
            "404(/*path)": "broken404",
            "500(/:errorMsg)": "broken500",
            "broken(/:errorMsg)": "broken",
            //"*path":  "defaultRoute"
            "*path":  "broken404"
        },
        remoteRoutes: {
            "signin": "signin",
            "signup": "signup",
            "change-password": "changePasswd"
        },

        initialize: function() {
            this.bind("route", this.trackPageView);
        },

        navigate: function(fragment, options) {
            var pathname = window.location.pathname;
            if (pathname.startsWith("/")) {
                pathname = pathname.substring(1);
            }
            if (pathname.endsWith("/")) {
                pathname = pathname.substring(0, pathname.length - 1);
            }
            if (fragment.startsWith("/")) {
                fragment = fragment.substring(1);
            }
            var func = this.remoteRoutes[fragment];
            if (func) {
                if (pathname !== fragment) { 
                    window.location = "/" + fragment;
                    return;
                }
                this[func].call(this, fragment, options);
            } else if (pathname !== "") {
                window.location = "/#" + fragment;
                return;
            } else {
                Backbone.Router.prototype.navigate.call(
                    $sp.router, fragment, options);
            }
        },

        trackPageView: function() {
            var url = Backbone.history.getFragment();
            if (!/^\//.test(url) && url !== "") {
                url = "/" + url;
            }
            ga("send", "pageview", url);
        },

        defaultRoute: function() {
            $sp.router.navigate("", {trigger:true, replace: true });
        },

        index: function() {
            var that = this,
                pathname = window.location.pathname;
            if (pathname.startsWith("/")) {
                pathname = pathname.substring(1);
            }
            if (pathname.endsWith("/")) {
                pathname = pathname.substring(0, pathname.length - 1);
            }
            if (pathname !== "") {
                $sp.router.navigate(pathname, {trigger: true});
                return;
            }
            if ($sp.context.account) {
                $sp.router.navigate("device", {trigger: true, replace:true });
            } else {
                that._indexNonAuth();
            }
        },

        _indexNonAuth: function() {
            this.loadView(new $sp.View.IndexNonAuthView(
                    { context:{ path: "/" }}));
        },

        _indexAuth: function(account) {
            //this.loadView(new $sp.View.IndexAuthView());
            this.loadView(new $sp.View.DirectoryView(
                    { context:{ path: "/" }}));
        },

        signin: function() {
            var that = this;
            if ($sp.context.account) {
                window.location = "/";
            } else {
                that.loadView(new $sp.View.SigninView(
                    { context:{ path: "/signin" }}));
            }
        },

        signup: function() {
            var that = this;
            if ($sp.context.account) {
                window.location = "/";
            } else {
                that.loadView(new $sp.View.SignupView(
                    { context:{ path: "/signup" }}));
            }
        },

        deviceHome: function(deviceId) {
            $sp.router.navigate("/device/" + deviceId + "/photos", true);
        },

        deviceMusic: authRequired(function(deviceId) {
            this.loadView(new $sp.View.MusicView({
                    context:{ path: "/device-music" },
                    deviceId: deviceId
                }));
        }),

        deviceDocuments: authRequired(function(deviceId) {
            this.loadView(new $sp.View.DocumentView({
                    context:{ path: "/device-documents" },
                    deviceId: deviceId
                }));
        }),

        deviceVideos: authRequired(function(deviceId) {
            this.loadView(new $sp.View.VideoView({
                    context:{ path: "/device-videos" },
                    deviceId: deviceId
                }));
        }),

        devicePhotoAlbum: authRequired(function(deviceId, path) {
            if (path) {
                path = decodeURIComponent(path);
                path = "/" + path;
                if (!path.endsWith("/")) {
                    path += "/";
                }
            }
            this.loadView(new $sp.View.PhotoAlbumView({
                    context:{ path: "/device-photo-album" },
                    deviceId: deviceId,
                    path: path
                }));
        }),

        devicePhotoAlbums: authRequired(function(deviceId) {
            this.loadView(new $sp.View.PhotoAlbumsView({
                    context:{ path: "/device-photo-albums" },
                    deviceId: deviceId
                }));
        }),

        deviceEvents: authRequired(function(deviceId) {
            this.loadView(new $sp.View.DeviceEventsView({
                    context:{ path: "/device-events" },
                    deviceId: deviceId
                }));
        }),

        deviceSettings: authRequired(function(deviceId) {
            this.loadView(new $sp.View.DeviceSettingsView({
                    context:{ path: "/device-settings" },
                    deviceId: deviceId
                }));
        }),

        deviceDirectory: authRequired(function(deviceId, path) {
            if (path) {
                path = decodeURIComponent(path);
                path = "/" + path;
                if (!path.endsWith("/")) {
                    path += "/";
                }
            }
            this.loadView(new $sp.View.DirectoryView({
                    context:{ path: "/device-directory" },
                    deviceId: deviceId,
                    path: path
                }));
        }),

        notification: function() {
            var that = this;
            if ($sp.context.account) {
                that.loadView(new $sp.View.NotificationView(
                        { context:{ path: "/notification" }}));
            } else {
                $sp.router.navigate("signin", true);
            }
        },

        settings: function() {
            if ($sp.context.account) {
                this.loadView(new $sp.View.AccountSettingsView(
                        { context:{ path: "/settings" }}));
            } else {
                $sp.router.navigate("signin", true);
            }

        },

        howToUse: function() {
            this.loadView(new $sp.View.HowtouseView(
                { context:{ path: "/how-to-use" }}));
        },

        download: function() {
            this.loadView(new $sp.View.DownloadView(
                { context:{ path: "/download" }}));
        },

        resetPasswd: function() {
            this.loadView(new $sp.View.ResetPasswdView(
                { context: { path: "/reset-password" }}));
        },

        changePasswd: function() {
            this.loadView(new $sp.View.ChangePasswdView(
                { context: { path: "/change-password" }}));
        },

        broken: function(errorMsg) {
            this.loadView(new $sp.View.BrokenView(
                { context: { path: "/broken" }, 
                errorMsg: errorMsg || "undefined"}));
        },

        broken404: function(path) {
            if (path) {
                path = decodeURIComponent(path);
                path = "/" + path;
                if (!path.endsWith("/")) {
                    path += "/";
                }
            }
            this.loadView(new $sp.View.BrokenView({ 
                    context: { path: "/broken" }, 
                    errorTitle: $sp.string("broken_title__page_not_found"),
                    errorDesc: $sp.string("broken_desc__page_not_found", path)
                }));
        },

        broken500: function(errorMsg) {
            this.loadView(new $sp.View.BrokenView({ 
                    context: { path: "/broken" }, 
                    errorTitle: $sp.string("broken_title__internal_server_error"),
                    errorDesc: errorMsg || "undefined" 
                }));
        },

        loadView: function(view) {
            this.view && 
                (this.view.close ? this.view.close() : this.view.remove());
            this.view = view;
        }
    });


    module = module || {};
    module.router = new router();
    return module;
}($sp));


