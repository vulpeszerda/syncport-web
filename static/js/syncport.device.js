var $sp = (function(module){
    module  = module || {};

    function Device(params) {
        var device = {
                deviceId: params.uid,
                deviceType: params.type,
                deviceName: params.name,
                connectionState: undefined,
                isTurnOn: false
            };
        return device;
    }

    module.DeviceManager = (function() {
        var _account,
            _devices,
            manager;
        manager =  {
            init: function(account, callback) {
                _account = account;
                _devices = new Backbone.Collection([]);
                this.getAllDevices(callback);
            },
            getDeviceCollection: function() {
                return _devices;
            },

            getAllDevices: function(callback) {
                var device,
                    turnOffDeviceIds = [],
                    idx,
                    ret = [];

                $.when(
                    $.get($sp.Remote.api.DEVICE_ALL),
                    $.get($sp.Remote.sync.ACTIVE_DEVICES))
                    .done(function(allRes, activeRes) {
                        var allDevices = allRes[0].devices,
                            activeDevices = activeRes[0].devices;

                        $.each(allDevices, function (idx, deviceObj) {
                            var devices;
                            if (deviceObj.uid === _account.deviceId) {
                                return true;
                            }
                            devices = _devices.where({ deviceId : deviceObj.uid });
                            if (!devices.length) {
                                _devices.add(new Device(deviceObj));
                            }
                        });
                        turnOffDeviceIds.length = 0;
                        $.each(_devices.models, function(idx, model) {
                            turnOffDeviceIds.push(model.get("deviceId"));
                        });

                        $.each(activeDevices, function (idx, deviceObj) {
                            device = _devices.where({ deviceId : deviceObj.device_id});
                            device = device.length ? device[0] : null;
                            if (device) {
                                device.set({
                                    connectionState: deviceObj["mobile_state"],
                                    isTurnOn: true
                                });
                                idx = turnOffDeviceIds.indexOf(device.get("deviceId"));
                                if (idx > -1) {
                                    turnOffDeviceIds.splice(idx, 1);
                                }
                            }
                        });

                        _devices.each(function(device) {
                            if (turnOffDeviceIds.indexOf(device.get("deviceId")) > -1) {
                                device.set({
                                    connectionState: undefined,
                                    isTurnOn: false
                                });
                            }
                        });
                        callback && callback(_devices);
                    }).fail(function() {
                        $sp.notify($sp.string("failed_to_load_devices"), "error");
                        callback && callback(_devices);
                    });
            },

            getDeviceWithId: function(deviceId, callback) {
                var device = _devices.where({deviceId : deviceId});
                if (device.length) {
                    callback && callback(device[0]);
                    return;
                }
                manager.getAllDevices(function(devices) {
                    var device = devices.where({deviceId : deviceId});
                    if (device.length) {
                        callback && callback(device[0]);
                    } else {
                        console.error("failed to get device with id '" + deviceId + "'");
                    }
                });
            },

            getCachedDeviceWithId: function(deviceId) {
                var device = _devices.where({deviceId : deviceId});
                if (device.length) {
                    return device[0];
                }
            },

            pollListener: function(res) {
                if (res.device_id === _account.deviceId) {
                    return;
                }
                manager.getAllDevices();
            }
        }
        return manager;
    }());

    return module;
}($sp));
