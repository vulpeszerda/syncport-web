var $sp = (function(module){
    var _models = {};

    function injectCollectionItemInterface(prototype) {
        prototype = prototype || {};
        prototype.getParent = function() {
            return this._parent;
        };

        prototype.setParent = function(p) {
            this._parent = p;
        };

        prototype.getRoot = function() {
            var p = this.getParent();
            if (p) {
                return p.getRoot();
            }
            return this;
        };
        return prototype;
    }

    _models.VirtualFile =  function(device, name, path) {
        var hasThumbnail = false,
            thumbnailHash,
            thumbnailPath;
        this.path = path;
        this.name = name;
        this.device = device;
        this.thumbnailRequest = null;
        this._meta = {};

        this.getThumbnailHash = function() {
            var key, url;
            if (thumbnailHash) {
                return thumbnailHash;
            }
            key = this.device.get("deviceId") + ":" + path;
            thumbnailHash = md5(key);
            return thumbnailHash;
        };
        this.getThumbnailPath = function() {
            if (thumbnailPath) {
                return thumbnailPath;
            }
            //thumbnailPath = $sp.Remote.image.IMAGE_PATH +
            //                    this.getThumbnailHash() + ".jpg";
            thumbnailPath = $sp.Remote.image.IMAGE_QUERY_PATH + this.device.get("deviceId") + "/" +
                                this.getThumbnailHash() + ".jpg";
            return thumbnailPath;
        };
        this.isMediaFile = function() {
            return this.isAudioFile() || this.isVideoFile();
        };
        this.isAudioFile = function() {
            var lowerCaseName = name.toLowerCase();
            return lowerCaseName.endsWith(".wav") || lowerCaseName.endsWith(".mp3");
        };
        this.isVideoFile = function() {
            var lowerCaseName = name.toLowerCase();
            return lowerCaseName.endsWith(".mpg") || lowerCaseName.endsWith(".mpeg") ||
                lowerCaseName.endsWith(".mpe") || lowerCaseName.endsWith(".mp4") ||
                lowerCaseName.endsWith(".avi") || lowerCaseName.endsWith(".3gp");
        };
        this.isImageFile = function() {
            var lowerCaseName = name.toLowerCase();
            return lowerCaseName.endsWith(".gif") || lowerCaseName.endsWith(".jpg") ||
                lowerCaseName.endsWith(".jpeg") || lowerCaseName.endsWith(".png");
        };
        this.isDocumentFile = function() {
            var lowerCaseName = name.toLowerCase(),
                list = ["txt", "doc", "docx", "docm", "hwp", "log", "pdf",
                        "rtf", "ppt", "pptx", "key", "keynote", "odp", "otp",
                        "pez", "xls", "xlsm", "xlsx"],
                match = false;
             $.each(list, function(idx, extension) {
                 if (lowerCaseName.endsWith(extension)) {
                    match = true;
                    return false;
                 }
             });
             return match;
        };
        this.hasThumbnail = function() {
            return hasThumbnail;
        };
        this.setHasThumbnail = function(has) {
            hasThumbnail = Boolean(has);
        };
        this.getModifiedAt = function() {
            if (!this._meta["ma"]) {
                return;
            }
            return new Date(this._meta["ma"].replace(" ", "T"));
        };
        this.getImageOrientation = function() {
            if (!this._meta["or"]) {
                return 0;
            }
            return this._meta["or"];
        };
        this.getMusicArtist = function() {
            return this._meta["at"];
        };
        this.getMusicAlbum = function() {
            return this._meta["ab"];
        };
        this.getMusicTitle = function() {
            return this._meta["tt"];
        };
        this.getMusicDuration = function() {
            return this._meta["dr"];
        };
        this.getFileSize = function() {
            return this._meta["sz"];
        };
        this.setMetaInfo = function(info) {
            var that = this,
                newMeta, isChanged = false;
            if (info) {
                newMeta = $.extend({}, this._meta, info);
                $.each(newMeta, function(key, val) {
                    var old = that._meta[key];
                    if (!old || old !== val) {
                        isChanged = true;
                        return false;
                    }
                });
                if (isChanged) {
                    this._meta = newMeta;
                    this.trigger("change", this);
                }
                return isChanged;
            }
            return false;
        };
        this.clearParent = function() {
            var path = this.path,
                subPaths = path.split("/"),
                parentPath,
                p, i,
                curr;

            if (subPaths.length < 2) {
                return;
            }

            parentPath = "";
            p = null;

            for (i = 0; i < subPaths.length - 1; i++) {
                parentPath += subPaths[i] + "/";
                if (i === 0) {
                    curr = new _models.VirtualDeviceModel(this.device);
                } else {
                    curr = new _models.VirtualDirectory(
                                    this.device,
                                    subPaths[i],
                                    parentPath
                                );
                }
                if (p != null) {
                    p.addChild(curr);
                }
                p = curr;
            }
            p.addChild(this);
        };

        this.loadParentDirectory = function(callback) {
            var p = this.getParent();
            this.clearParent();
            if (p) {
                p.loadSubDirectories(1, callback);
            }
        }
    }

    _models.VirtualFile.prototype = (function(prototype){
        // extend backbone event for trigger 'change' event
        prototype = $.extend({}, Backbone.Events, prototype || {});
        prototype = injectCollectionItemInterface(prototype);

        prototype.isAdding = false;
        prototype.isDeleting = false;

        prototype.isFile = function() {
            return this instanceof _models.VirtualFile;
        };

        prototype.isDirectory = function() {
            return this instanceof _models.VirtualDirectory;
        };

        prototype.isDevice = function() {
            return this instanceof _models.VirtualDevice;
        };

        prototype.getDevice = function() {
            return this.device;
        };

        return prototype;
    }());

    _models.VirtualDirectory = function(device, name, path) {
        this.name = name;
        this.device = device;
        this.path = path;
        this.items = {};
        this.isLoaded = false;
    }

    _models.VirtualDirectory.prototype = (function(prototype) {
        var FETCH_DEPTH = 3;

        prototype = $.extend({}, prototype || {});

        prototype._untriggeredChangedItems = [];
        prototype._untriggeredAddedItems= [];
        prototype._untriggeredRemovedItems= [];

        prototype.splitSize = 40;

        prototype.getSetHeight = function(count) {
            return count * 49;
        };

        prototype._itemChangeHandler = function(item) {
            var that = this;
            this._untriggeredChangedItems.push(item);
            if (!this._changeTrigger) {
                this._changeTrigger = _.throttle(function() {
                        var items = that._untriggeredChangedItems.slice();
                        that._untriggeredChangedItems.length = 0;
                        if (items.length > 0) {
                            that.trigger("change.child", that, items);
                        }
                    }, 1000, {leading: false});
            }
            this._changeTrigger();
        };

        prototype.getItemKey = function(file) {
            return (file.isFile() ? file.name : file.name + "/").toLowerCase();
        }

        prototype.addItem = function(item, immediateTrigger) {
            var that = this;

            item.setParent(this);

            this.listenTo(item, "change", this._itemChangeHandler);
            this.listenTo(item, "remove", this.removeItem);

            this.items[this.getItemKey(item)] = item;
            if (item.isAdding) {
                this.trigger("add.temp-child", item);
            }
            this._untriggeredAddedItems.push(item);

            if (!this._addTrigger) {
                this._addTrigger = function() {
                    var items = that._untriggeredAddedItems.slice();
                    that._untriggeredAddedItems.length = 0;
                    if (items.length > 0) {
                        that.trigger("add.child", that, items);
                    }
                };
            }
            if (!this._throttledAddTrigger) {
                this._throttledAddTrigger = _.throttle(
                    this._addTrigger, 1000, { leading: false });
            }

            if (immediateTrigger) {
                this._addTrigger();
            } else {
                this._throttledAddTrigger();
            }
        };

        prototype.removeItem = function(item) {
            var that = this,
                key = this.getItemKey(item),
                removed = this.items[key];
            if (removed) {
                delete this.items[key];
                this.stopListening(removed);
                this._untriggeredRemovedItems.push(removed);
                if (!this._removeTrigger) {
                    this._removeTrigger = _.throttle(function() {
                            var items = that._untriggeredRemovedItems.slice();
                            if (items.length > 0) {
                                that.trigger("remove.child", that, items);
                            }
                        }, 1000, {leading: false});
                }
                this._removeTrigger();
                if (Object.keys(this.items).length === 0) {
                    this.trigger("remove", this);
                }
            }
        };

        prototype.remove = function() {
            var that = this;
            $.each(this.items, function(key, item) {
                that.stopListening(item);
            });
        };

        prototype.getFilesDict = function() {
            var dict = {};
            $.each(this.items, function(key, file) {
                dict[file.path] = file;
            });
            return dict;
        }

        prototype.createTempFile = function(path) {
            var file,
                name,
                p, idx;

            p = path.endsWith("/") ? path.substring(0, path.length - 1) : path;
            idx = p.lastIndexOf("/");
            if (idx > -1) {
                name = p.substring(idx + 1);
            } else {
                name = p;
            }

            if (path.endsWith("/")) {
                file = new _models.VirtualDirectory(this.device, name, path);
            } else {
                file = new _models.VirtualFile(this.device, name, path);
            }
            file.isAdding = true;
            this.addItem(file, true);
        };

        prototype.getSortedChildList = function() {
            var that = this,
                list;

            list = Object.keys(this.items).map(function(key){
                        return that.items[key]
                    });
            list = list.sort(function(a, b) {
                if (a.isDirectory()) {
                    return b.isDirectory() ?
                                a.name.localeCompare(b.name) : -1;
                }
                if (b.isDirectory()) {
                    return 1;
                }
                if (that.isImageDirectory && that.isImageDirectory()) {
                    if (a.isImageFile() || a.isMediaFile()) {
                        return b.isImageFile() || b.isMediaFile() ?
                                a.name.localeCompare(b.name) : 1;
                    }
                    if (b.isImageFile() || b.isMediaFile()) {
                        return -1;
                    }
                }
                return a.name.localeCompare(b.name);
            });
            return list;
        };

        prototype.load = function(callback) {
            var that = this;
            $.ajax({
                url:$sp.Remote.delta.PULL,
                data: {
                    type: "directory",
                    owner: this.device.get("deviceId"),
                    plain: this.path,
                    depth: FETCH_DEPTH
                },
                dataType:"json",
                contentType:"application/json"
            }).done(function(res) {
                var changedDirList  = that.buildChildren(FETCH_DEPTH, res);
                that.isLoaded = true;
                callback && callback.success.call(that, changedDirList, that);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                var msg = jqXHR.responseText;
                callback && callback.failure.call(that, msg);
            });
        };

        prototype.buildChildren = function(depth, res) {
            var needUpdateDirs = [],
                data,
                that = this,
                removeCandidates;

            removeCandidates = this.getSubFilesWithDepth(depth);

            data =  res.data;

            if (!this.isLoaded) {
                needUpdateDirs.push(this);
            }

            $.each(data, function(idx, fileObj) {
                var path = fileObj["n"],
                    meta = fileObj["m"],
                    subPaths = path.split("/"),
                    subPath,
                    key,
                    childAbsPath = that.path,
                    curr = that,
                    i,
                    idx,
                    file;

                if (path.endsWith("/")) {
                    subPaths.pop();
                }

                for (i = 0; i < subPaths.length; i++) {
                    subPath = subPaths[i];
                    key = subPath;

                    if (i < subPaths.length -1 || path.endsWith("/")) {
                        key += "/";
                    }

                    childAbsPath += key;

                    file  = curr.items[key.toLowerCase()];
                    if (!file) {
                        if (key.endsWith("/")) {
                            file = new _models.VirtualDirectory(
                                        that.device,
                                        subPath,
                                        childAbsPath
                                    );
                        } else {
                            file = new _models.VirtualFile(
                                        that.device,
                                        subPath,
                                        childAbsPath
                                    );
                        }
                        curr.addItem(file);
                        curr.isLoaded = true;
                        if (needUpdateDirs.indexOf(curr) === -1)  {
                            needUpdateDirs.push(curr);
                        }
                    } else {
                        if (file.isAdding) {
                            file.isAdding = false;
                            if (needUpdateDirs.indexOf(curr) === -1)  {
                                needUpdateDirs.push(curr);
                            }
                        }
                        idx = removeCandidates.indexOf(file);
                        if (idx > -1) {
                            removeCandidates.splice(idx, 1);
                        }
                    }
                    if (file.isFile() && file.setMetaInfo(meta)) {
                        if (needUpdateDirs.indexOf(curr) === -1)  {
                            needUpdateDirs.push(curr);
                        }
                    }
                    if (i < depth -1 && !file.isFile()) {
                        file.isLoaded = true;
                    }
                    if (i < subPaths.length - 1 && file.isFile()) {
                        throw "Invalid path & file match";
                    } else if (i < subPaths.length -1) {
                        curr = file;
                    }
                }
            });
            $.each(removeCandidates, function(idx, file) {
                var p = file.getParent();
                if (p && needUpdateDirs.indexOf(p) === -1)  {
                    needUpdateDirs.push(p);
                }
                that.removeItem(file);
            });
            return needUpdateDirs;
        };

        prototype.getSubFilesWithDepth = function(depth) {
            var files = [];
            $.each(this.items, function(key, item) {
                var childFiles;
                if (!item.isFile() && depth > 1) {
                    childFiles = item.getSubFilesWithDepth(depth -1);
                    files.concat(childFiles);
                }
                files.push(item);
            });
            return files;
        };

        return prototype;
    }(_models.VirtualFile.prototype));

    _models.VirtualDevice = function(device) {
        this.name = device.get("deviceName");
        this.device = device;
        this.path = "/";
        this.items = {};
        this.isLoaded = false;
    }

    _models.VirtualDevice.prototype = (function(prototype){
        prototype = $.extend({}, prototype || {});
        prototype.loadSyncRootPath = function(callback) {
            var that = this;
            if (this.device.get("syncRootPath")) {
                this._syncRootPath = this.device.get("syncRootPath");
                callback && callback(this);
                return;
            }
            $.ajax({
                url: $sp.Remote.delta.SYNCROOT,
                data: {
                    device_id: that.device.get("deviceId")
                },
                dataType:"text"
            }).done(function(res) {
                if (res && !res.endsWith("/")) {
                    res += "/";
                }
                that._syncRootPath = res;
                that.device.set("syncRootPath", res);
                callback && callback(that);
            }).fail(function(jqXHR) {
                if ($sp.context.locale==="ko_KR") {
                    $sp.notify(
                        "이 기기에 깔려있는 Syncport앱에 업데이트가 필요합니다.", "error");
                } else {
                    $sp.notify(
                        "Need update to Syncport client in this device.", "error");
                }
                //$sp.notify(
                //    "failed to fetch syncroot:" + jqXHR.responseText, "error");
                that._syncRootPath = "/";
                callback && callback(that);
            });
        };
        prototype.getSyncRootPath = function() {
            return this._syncRootPath;
        };
        prototype.getSyncRoot = function() {
            var root;
            if (this._syncRootPath) {
                root = this.getFile(this._syncRootPath);
            }
            return root ? root : this;
        };
        prototype.getFile = function(path) {
            var removedSlashPath = path.endsWith("/") ?
                                    path.substring(0, path.length - 1) : path,
                subPaths = removedSlashPath.split("/"),
                subPathsLen = subPaths.length,
                subPath,
                parentModel,
                currModel,
                idx;

            if (subPathsLen <= 1) {
                if (!path === "/") {
                    return;
                }
                return this;
            }

            parentModel = this;
            currModel = null;

            for (idx = 1; idx < subPathsLen; idx++) {
                subPath = subPaths[idx];
                if (idx < subPathsLen - 1 || path.endsWith("/")) {
                    subPath += "/";
                }
                if (subPath === "*/") {
                    $.each(parentModel.items, function(key, val) {
                        currModel = val;
                        if (currModel) {
                            return true;
                        }
                    });
                } else {
                    currModel = parentModel.items[subPath.toLowerCase()];
                }
                if (!currModel) {
                    return;
                }
                if (currModel.isDirectory()) {
                    parentModel = currModel;
                } else if (idx < subPathsLen - 1) {
                    console.error("sub path should be direcotry! not file");
                }
            }
            if ((path.endsWith("/") && currModel.isDirectory()) ||
                (!path.endsWith("/") && currModel.isFile())){
                return currModel;
            }
        };

        return prototype;
    }(_models.VirtualDirectory.prototype));


    _models.BaseCollectionModel = {
        extend: function(override) {
            var model =  function() {
                this.init.apply(this, arguments);
            };
            model.prototype = $.extend({}, this.prototype, override);
            model.extend = this.extend;
            return model;
        }
    };

    _models.BaseCollectionModel.prototype = (function(prototype) {
        prototype = prototype || {};
        _.extend(prototype, Backbone.Events);

        prototype.collectionType = undefined;
        prototype.isLoaded = false;

        prototype.loadSyncRootPath = function(callback) {
            var that = this;
            if (this.device.get("syncRootPath")) {
                this._syncRootPath = this.device.get("syncRootPath");
                callback && callback(this);
                return;
            }
            $.ajax({
                url: $sp.Remote.delta.SYNCROOT,
                data: {
                    device_id: that.device.get("deviceId")
                },
                dataType:"text"
            }).done(function(res) {
                if (res && !res.endsWith("/")) {
                    res += "/";
                }
                that._syncRootPath = res;
                that.device.set("syncRootPath", res);
                callback && callback(that);
            }).fail(function(jqXHR) {
                if ($sp.context.locale==="ko_KR") {
                    $sp.notify(
                        "이 기기에 깔려있는 Syncport앱에 업데이트가 필요합니다.", "error");
                } else {
                    $sp.notify(
                        "Need update to Syncport client in this device.", "error");
                }
                //$sp.notify(
                //    "failed to fetch syncroot." + jqXHR.responseText, "error");
                that._syncRootPath = "/";
                callback && callback(that);
            });
        };
        prototype.getSyncRootPath = function() {
            return this._syncRootPath;
        };
        prototype._itemChangeHandler = function(item) {
            var that = this;
            this.clearCache();
            this._untriggeredChangedItems.push(item);
            if (!this._changeTrigger) {
                this._changeTrigger = _.throttle(function() {
                        var items = that._untriggeredChangedItems.slice();
                        that._untriggeredChangedItems.length = 0;
                        if (items.length > 0) {
                            that.trigger("change.child", that, items);
                        }
                    }, 1000, {leading: false});
            }
            this._changeTrigger();
        };

        prototype.init = function(device) {
            this.device = device;
            this._untriggeredChangedItems = [];
            this._untriggeredAddedItems= [];
            this._untriggeredRemovedItems= [];

            this.items = {};
        };

        prototype.load = function(callback) {
            var that = this;
            $.ajax({
                url:$sp.Remote.delta.PULL,
                data: {
                    type: "collection",
                    owner: this.device.get("deviceId"),
                    file_type: this.collectionType
                },
                dataType:"json",
                contentType:"application/json"
            }).done(function(res) {
                var isChanged  = that.buildChildren(res);
                that.isLoaded = true;
                callback && callback.success.call(that, isChanged);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                var msg = jqXHR.responseText;
                callback && callback.failure.call(that, msg);
            });
        };

        prototype.buildChildren = function(res) {
            var data = res.data,
                that = this,
                // shallow copy existing items
                existingFiles = this.getFilesDict(),
                removeCandidateFiles = $.extend({}, existingFiles),
                isChanged = false;

            $.each(data, function(parentPath, files) {
                $.each(files, function(idx, fileObj) {
                    var name = fileObj["n"],
                        meta = fileObj["m"],
                        path,
                        file;
                    if (!name) {
                        return true;
                    }
                    path = parentPath + name;
                    file = existingFiles[path];
                    if (!file) {
                        file = new _models.VirtualFile(that.device, name, path);
                        that.addFile(file);
                        existingFiles[path] = file;
                    } else {
                        if (file.isAdding) {
                            isChanged = true;
                        }
                        if (removeCandidateFiles[path]){
                            delete removeCandidateFiles[path];
                        }
                    }
                    isChanged |= file.setMetaInfo(meta);
                });
            });
            $.each(removeCandidateFiles, function(path, file) {
                isChanged = true;
                that.removeFile(file);
            });
            return isChanged;
        };

        prototype.size = function() {
            return Object.keys(this.items).length;
        };

        prototype.addItem = function(item, immediateTrigger) {
            var that = this;
            this.clearCache();

            item.setParent && item.setParent(this);

            this.listenTo(item, "remove", this.removeItem);
            this.listenTo(item, "change", this._itemChangeHandler);
            this.items[this.getItemKey(item)] = item;
            this._untriggeredAddedItems.push(item);

            if (!this._addTrigger) {
                this._addTrigger = function() {
                    var items = that._untriggeredAddedItems.slice();
                    that._untriggeredAddedItems.length = 0;
                    if (items.length > 0) {
                        that.trigger("add.child", that, items);
                    }
                };
            }
            if (!this._throttledAddTrigger) {
                this._throttledAddTrigger = _.throttle(
                    this._addTrigger, 1000, { leading: false });
            }

            if (immediateTrigger) {
                this._addTrigger();
            } else {
                this._throttledAddTrigger();
            }
        };

        prototype.getSyncRootPath = function() {
            return this._syncRootPath;
        }

        prototype.removeItem = function(item) {
            var that = this,
                key = this.getItemKey(item),
                removed = this.items[key];
            if (removed) {
                delete this.items[key];
                this.stopListening(removed);
                this.clearCache();
                this._untriggeredRemovedItems.push(removed);
                if (!this._removeTrigger) {
                    this._removeTrigger = _.throttle(function() {
                            var items = that._untriggeredRemovedItems.slice();
                            if (items.length > 0) {
                                that.trigger("remove.child", that, items);
                            }
                        }, 1000, {leading: false});
                }
                this._removeTrigger();
                if (Object.keys(this.items).length === 0) {
                    this.trigger("remove", this);
                }
            }
        };

        prototype.clearCache = function() {

        };

        prototype.remove = function() {
            var that = this;
            $.each(this.items, function(key, item) {
                that.stopListening(item);
            });
        };

        prototype.addFile = function(file) {
            throw "Not implemented";
        };

        prototype.getFilesDict = function() {
            throw "Not implemented";
        };

        prototype.removeFile = function() {
            throw "Not implemented";
        };

        prototype.getItemKey = function(item) {
            throw "Not implemented";
        };

        prototype.createTempFile = function(path) {
            var file,
                name,
                p, idx;

            p = path.endsWith("/") ? path.substring(0, path.length - 1) : path;
            idx = p.lastIndexOf("/");
            if (idx > -1) {
                name = p.substring(idx + 1);
            } else {
                name = p;
            }

            if (path.endsWith("/")) {
                file = new _models.VirtualDirectory(this.device, name, path);
            } else {
                file = new _models.VirtualFile(this.device, name, path);
            }
            file.isAdding = true;
            this.addFile(file);
        };

        return prototype;
    } ());

    _models.FileCollectionModel = _models.BaseCollectionModel.extend({
        // [undefined | name | artist | album | time | kind | modifiedAt]
        sortType: undefined,
        // [desc | asc]
        sortDirection: "asc",
        // display set size
        splitSize: 40,
        getSetHeight: function(count) {
            throw "Not implmented";
        },
        addFile: function(file) {
            this.addItem(file);
        },
        removeFile: function(file) {
            this.removeItem(file);
        },
        getItemKey: function(item) {
            return item.path;
        },
        getFilesDict: function() {
            var dict = {};
            $.each(this.items, function(key, item) {
                dict[item.path] = item;
            });
            return dict;
        },
        getSortedItems: function() {
            var that = this,
                items = [], comparator, defaultComparator;

            $.each(this.items, function(key, item) {
                items.push(item);
            });

            defaultComparator = function(lhs, rhs) {
                if (that.sortDirection === "desc") {
                    return rhs.name.localeCompare(lhs.name);
                }
                return lhs.name.localeCompare(rhs.name);
            };

            if (this.sortType === "name") {
                comparator = defaultComparator;
            } else if (this.sortType === "kind") {
                comparator = function(lhs, rhs) {
                    var lhsKind, rhsKind, lhsName, rhsName, idx, cmp;

                    idx = lhs.name.lastIndexOf(".");
                    lhsName = idx < 0 ? lhs.name : lhs.name.substring(0, idx);
                    lhsKind = idx > -1 ?
                        lhs.name.substring(idx + 1, lhs.name.length)
                            .toUpperCase() : undefined;

                    idx = rhs.name.lastIndexOf(".");
                    rhsName = idx < 0 ? rhs.name : rhs.name.substring(0, idx);
                    rhsKind = idx > -1 ?
                        rhs.name.substring(idx + 1, rhs.name.length)
                            .toUpperCase() : undefined;

                    if (lhsKind) {
                        if (!rhsKind) {
                            return -1;
                        }
                        if (that.sortDirection === "desc") {
                            cmp = rhsKind.localeCompare(lhsKind);
                        } else {
                            cmp = lhsKind.localeCompare(rhsKind);
                        }
                        return cmp !== 0 ? cmp : defaultComparator(lhs, rhs);
                    }
                    if (!rhsKind) {
                        return defaultComparator(lhs, rhs);
                    }
                    return 1;
                };
            } else if (this.sortType === "modifiedAt") {
                comparator = function(lhs, rhs) {
                    var lhsModifiedAt, rhsModifiedAt, cmp;

                    lhsModifiedAt = lhs.getModifiedAt() || undefined;
                    rhsModifiedAt = rhs.getModifiedAt() || undefined;

                    if (lhsModifiedAt) {
                        if (!rhsModifiedAt) {
                            return -1;
                        }
                        if (that.sortDirection === "desc") {
                            cmp = rhsModifiedAt - lhsModifiedAt;
                        } else {
                            cmp = lhsModifiedAt - rhsModifiedAt;
                        }
                        return cmp !== 0 ? cmp : defaultComparator(lhs, rhs);
                    }
                    if (!rhsModifiedAt) {
                        return defaultComparator(lhs, rhs);
                    }
                    return 1;
                };
            }
            if (comparator) {
                items.sort(comparator);
            }
            return items;
        }
    });

    _models.AlbumCollectionModel = _models.BaseCollectionModel.extend({
        collectionType: "image",
        _itemChangeHandler: function(album, file) {
            if (album.size() === 0) {
                return;
            }
            _models.BaseCollectionModel.prototype
                ._itemChangeHandler.apply(this, arguments);
        },
        getAlbum: function(file) {
            var MAX_DEPTH = 3,
                rootPath,
                subPaths,
                idx,
                syncRootDepth,
                albumPath;

            rootPath = this.getSyncRootPath();
            syncRootDepth = rootPath.split("/").length;

            subPaths = file.path.split("/");
            subPaths = subPaths.splice(0, subPaths.length - 1);

            subPaths.push("");
            idx = Math.min(syncRootDepth + MAX_DEPTH, subPaths.length);
            albumPath = subPaths.splice(0, idx - 1).join("/") + "/";

            return this.items[albumPath];
        },
        addFile: function(file) {
            var MAX_DEPTH = 3,
                rootPath,
                subPaths,
                idx,
                syncRootDepth,
                albumPath,
                album;

            rootPath = this.getSyncRootPath();
            syncRootDepth = rootPath.split("/").length;

            subPaths = file.path.split("/");
            subPaths = subPaths.splice(0, subPaths.length - 1);

            subPaths.push("");
            idx = Math.min(syncRootDepth + MAX_DEPTH, subPaths.length);
            albumPath = subPaths.splice(0, idx - 1).join("/") + "/";

            album = this.items[albumPath];
            if (!album) {
                album = new _models.AlbumModel(this.device, albumPath);
                this.addItem(album);
            }
            album.addFile(file);
        },
        removeFile: function(file) {
            var album = this.getAlbum(file);
            if (album) {
                album.removeFile(file);
            }
        },
        addItem: function(item) {
            _models.BaseCollectionModel.prototype.addItem.call(this, item);
            this.listenTo(item, "add.child remove.child change.child",
                this._itemChangeHandler);
        },
        getSortedItems: function() {
            var albums = [];
            $.each(this.items, function(key, album) {
                albums.push(album);
            });
            albums.sort(function(l, r) {
                return r.size() - l.size();
            });
            return albums;
        },
        getItemKey: function(item) {
            return item.path;
        },
        getFilesDict: function() {
            var dict = {};
            $.each(this.items, function(key, album) {
                $.extend(dict, album.getFilesDict());
            });
            return dict;
        }
    });

    _models.AlbumModel = _models.FileCollectionModel.extend({
        collectionType: "image",
        splitSize: 40,
        init: function(device, path) {
            var name,
                subPaths;

            _models.FileCollectionModel.prototype.init.call(this, device);

            if (!path.endsWith("/")) {
                path += "/";
            }
            subPaths = path.split("/");
            name = subPaths[subPaths.length - 2];

            this.path = path;
            this.name = name;
            _.bindAll(this, "getThumbnailFile");
        },
        getSetHeight: function(count) {
            var rowCount = Math.ceil(count / 5);
            return rowCount * 161;
        },
        addFile: function(file) {
            if (!file.path.startsWith(this.path)) {
                return;
            }
            _models.FileCollectionModel.prototype.addFile.call(this, file);
        },
        isRecentlyModified: function() {
            var latestModified,
                timeDiff;
            $.each(this.items, function(key, file) {
                if (file.getModifiedAt() &&
                        (!latestModified ||
                            latestModified < file.getModifiedAt())) {
                    latestModified = file.getModifiedAt();
                }
            });
            if (latestModified) {
                timeDiff = (new Date()).getTime() - latestModified.getTime();
                if (timeDiff / (60 * 60 * 1000 * 24) < 1) {
                    return true;
                }
            }
            return false;
        },
        getThumbnailFile: function() {
            var recentFile;
            if (!this._thumbnailFileCache) {
                $.each(this.items, function(key, file) {
                    if (!recentFile) {
                        recentFile = file;
                        return true;
                    }
                    if (file.getModifiedAt() &&
                            (!recentFile.getModifiedAt() ||
                            file.getModifiedAt() > recentFile.getModifiedAt())) {
                        recentFile = file;
                    }
                });
                this._thumbnailFileCache = recentFile;
            }
            return this._thumbnailFileCache;
        },
        clearCache: function() {
            _models.FileCollectionModel.prototype
                .clearCache.apply(this, arguments);
            this._thumbnailFileCache = null;
            this._photoGroupsCache = null;
        },
        getPhotoGroups: function() {
            var groups = {},
                results = [];

            if (this._photoGroupsCache) {
                return this._photoGroupsCache;
            }

            $.each(this.items, function(path, file) {
                var meta, date, key;
                if (file.getModifiedAt()) {
                    date = file.getModifiedAt();
                    if ($sp.context.locale === "ko_KR") {
                        key = moment(date).format("YYYY년 M월");
                    } else {
                        key = moment(date).format("MMM, YYYY");
                    }
                } else {
                    key = "unknown";
                }
                if (!groups[key]) {
                    groups[key] = [];
                }
                groups[key].push(file);
            });

            $.each(groups, function(key, group) {
                var sortedGroup,
                    photoGroup,
                    indexFile;

                sortedGroup = group.sort(function(lhs, rhs)  {
                    var lhsM, rhsM;
                    lhsM = lhs.getModifiedAt();
                    rhsM = rhs.getModifiedAt();
                    if (lhsM && !rhsM) {
                        return -1;
                    }
                    if (!lhsM && rhsM) {
                        return 1;
                    }
                    if (lhsM && rhsM) {
                        return lhsM > rhsM ? -1 :
                                    lhsM === rhsM ? 0 : 1;
                    }
                    return lhs.name.localeCompare(rhs.name);
                });

                if (sortedGroup.length > 0) {
                    indexFile = sortedGroup[0];
                    photoGroup = {
                        readableModifiedAt: key,
                        photos: sortedGroup
                    };
                    if (indexFile.getModifiedAt()) {
                        photoGroup["modifiedAt"] = indexFile.getModifiedAt();
                    }
                    results.push(photoGroup);
                }
            });
            results.sort(function(lhs, rhs)  {
                var lhsDate = lhs.modifiedAt,
                    rhsDate = rhs.modifiedAt,
                    lhsYear, lhsMonth,
                    rhsYear, rhsMonth,
                    els;

                if (!lhsDate) {
                    return 1;
                }
                if (!rhsDate) {
                    return -1;
                }
                return rhsDate - lhsDate;
            });
            this._photoGroupsCache = results;
            return results;
        }
    });

    _models.AlbumModel.prototype = (function(prototype) {
        return injectCollectionItemInterface(prototype);
    }(_models.AlbumModel.prototype));

    _models.MusicListModel = _models.FileCollectionModel.extend({
        collectionType: "music",
        getSetHeight: function(count) {
            return count * 45;
        },
        getSortedItems: function() {
            var that = this,
                items = [], comparator, defaultComparator;

            $.each(this.items, function(key, item) {
                items.push(item);
            });

            defaultComparator = function(lhs, rhs) {
                if (that.sortDirection === "desc") {
                    return rhs.name.localeCompare(lhs.name);
                }
                return lhs.name.localeCompare(rhs.name);
            };

            if (this.sortType === "name") {
                comparator = defaultComparator;
            } else if (this.sortType === "artist") {
                comparator = function(lhs, rhs) {
                    var lhsArtist, rhsArtist, cmp;

                    lhsArtist = lhs.getMusicArtist() || undefined;
                    rhsArtist = rhs.getMusicArtist() || undefined;

                    if (lhsArtist) {
                        if (!rhsArtist) {
                            return -1;
                        }
                        if (that.sortDirection === "desc") {
                            cmp = rhsArtist.localeCompare(lhsArtist);
                        } else {
                            cmp = lhsArtist.localeCompare(rhsArtist);
                        }
                        return cmp !== 0 ? cmp : defaultComparator(lhs, rhs);
                    }
                    if (!rhsArtist) {
                        return defaultComparator(lhs, rhs);
                    }
                    return 1;
                };
            } else if (this.sortType === "album") {
                comparator = function(lhs, rhs) {
                    var lhsAlbum, rhsAlbum, cmp;

                    lhsAlbum = lhs.getMusicAlbum() || undefined;
                    rhsAlbum = rhs.getMusicAlbum() || undefined;

                    if (lhsAlbum) {
                        if (!rhsAlbum) {
                            return -1;
                        }
                        if (that.sortDirection === "desc") {
                            cmp = rhsAlbum.localeCompare(lhsAlbum);
                        } else {
                            cmp = lhsAlbum.localeCompare(rhsAlbum);
                        }
                        return cmp !== 0 ? cmp : defaultComparator(lhs, rhs);
                    }
                    if (!rhsAlbum) {
                        return defaultComparator(lhs, rhs);
                    }
                    return 1;
                };
            } else if (this.sortType === "time") {
                comparator = function(lhs, rhs) {
                    var lhsDuration, rhsDuration, cmp;

                    lhsDuration = lhs.getMusicDuration() || undefined;
                    rhsDuration = rhs.getMusicDuration() || undefined;

                    if (lhsDuration) {
                        if (!rhsDuration) {
                            return -1;
                        }
                        if (that.sortDirection === "desc") {
                            cmp = rhsDuration - lhsDuration;
                        } else {
                            cmp = lhsDuration - rhsDuration;
                        }
                        return cmp !== 0 ? cmp : defaultComparator(lhs, rhs);
                    }
                    if (!rhsDuration) {
                        return defaultComparator(lhs, rhs);
                    }
                    return 1;
                };
            }
            if (comparator) {
                items.sort(comparator);
            }
            return items;
        }
    });

    _models.VideoListModel = _models.FileCollectionModel.extend({
            collectionType: "video",
            getSetHeight: function(count) {
                return count * 85;
            }
        });
    _models.DocumentListModel = _models.FileCollectionModel.extend({
            collectionType: "document",
            sortType: "modifiedAt",
            sortDirection: "desc",
            getSetHeight: function(count) {
                return count * 45;
            }
        });

    module = module || {};
    module.Model = $.extend(_models, module.Model || {});
    return module;
}($sp));
