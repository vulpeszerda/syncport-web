var $sp = (function(module) {
    var utils = {};

    utils.downloadFile = (function(){
        var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1,
            isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1,
            obj;

        obj = function (sUrl) {

            //iOS devices do not support downloading. We have to inform user about this.
            if (/(iP)/g.test(navigator.userAgent)) {
                alert('Your device does not support files downloading. Please try again in desktop browser.');
                return false;
            }

            //If in Chrome or Safari - download via virtual link click
            if (isChrome || isSafari) {
                //Creating new link node.
                var link = document.createElement('a');
                link.href = sUrl;

                if (link.download) {
                    //Set HTML5 download attribute. This will prevent file from opening if supported.
                    var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
                    link.download = fileName;
                }

                //Dispatching click event.
                if (document.createEvent) {
                    var e = document.createEvent('MouseEvents');
                    e.initEvent('click', true, true);
                    link.dispatchEvent(e);
                    return true;
                }
            }

            window.open(sUrl, '_self');
            return true;
        }
        return obj;
    }());

    utils.isVis = function(){
        var stateKey, eventKey, keys = {
            hidden: "visibilitychange",
            webkitHidden: "webkitvisibilitychange",
            mozHidden: "mozvisibilitychange",
            msHidden: "msvisibilitychange"
        };
        for (stateKey in keys) {
            if (stateKey in document) {
                eventKey = keys[stateKey];
                break;
            }
        }
        return!document[stateKey];
    };
    utils.listenVisChange = function(func) {
        var stateKey, eventKey, keys = {
            hidden: "visibilitychange",
            webkitHidden: "webkitvisibilitychange",
            mozHidden: "mozvisibilitychange",
            msHidden: "msvisibilitychange"
        };
        for (stateKey in keys) {
            if (stateKey in document) {
                eventKey = keys[stateKey];
                break;
            }
        }
        if (document.addEventListener) {
            document.addEventListener(eventKey, func);
        } else if (document.attachEvent){
            document.attachEvent(eventKey, func);
        } else {
            throw "Not supported browser";
        }
    }

    utils.stopListenVisChange = function(func) {
        var stateKey, eventKey, keys = {
            hidden: "visibilitychange",
            webkitHidden: "webkitvisibilitychange",
            mozHidden: "mozvisibilitychange",
            msHidden: "msvisibilitychange"
        };
        for (stateKey in keys) {
            if (stateKey in document) {
                eventKey = keys[stateKey];
                break;
            }
        }
        document.removeEventListener(eventKey, func);
    }

    utils.debugLog = function(msg) {
        $sp.context.debug && console.log(msg);
    };

    utils.browserVersion = (function(){
        var ua= navigator.userAgent, tem,
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\bOPR\/(\d+)/)
            if(tem) return 'Opera '+tem[1];
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))) {
            M.splice(1, 1, tem[1]);
        }
        return M.join(' ');
    })();

    utils.isMobile = (function() {
        return (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
    })();

    utils.os = (function() {
        var OSName="Unknown OS";
        if (navigator.appVersion.indexOf("Win")!==-1) {
            OSName="Windows";
        }
        if (navigator.appVersion.indexOf("Mac")!==-1) {
            OSName="MacOS";
        }
        if (navigator.appVersion.indexOf("X11")!==-1) {
            OSName="UNIX";
        }
        if (navigator.appVersion.indexOf("Linux")!==-1) {
            OSName="Linux";
        }
        return OSName;
    }());

    utils.getSyncportTabCount = function(callback) {
        if (!utils._requestHiThere) {
            utils._requestHiThere = _.throttle(function() {
                currentTabCount = 0;
                isSyncportTabSelected = false;
                module.intercom.emit("hiThere", {});
            }, 500, {trailing: false});
        }
        utils._requestHiThere();
        setTimeout(function() {
            callback && callback(currentTabCount, isSyncportTabSelected);
        }, 500);
    };

    utils.ListenableArray = function() {};
    utils.ListenableArray.extend = function(override) {
        return function() {
            var array = Object.create(utils.ListenableArray.prototype);
            array = utils.ListenableArray.apply(array, arguments) || array;
            $.each(override, function(key, val) {
                array[key] = val;
            });
            return array;
        };
    };
    utils.ListenableArray.prototype = (function() {
        var newProto = new Array;
        $.extend(newProto, Backbone.Events);

        newProto.push = function() {
            var val = Array.prototype.push.apply(this, arguments),
                args = Array.prototype.slice.call(arguments);
            args.unshift("push");
            this.trigger.apply(this, args);
            return val;
        };
        newProto.pop = function() {
            var val = Array.prototype.pop.apply(this, arguments),
                args = Array.prototype.slice.call(arguments);
            args.unshift("pop");
            this.trigger.apply(this, args);
            return val;
        };
        newProto.reverse = function() {
            var val = Array.prototype.reverse.apply(this, arguments),
                args = Array.prototype.slice.call(arguments);
            args.unshift("reverse");
            this.trigger.apply(this, args);
            return val;
        };
        newProto.shift = function() {
            var val = Array.prototype.shift.apply(this, arguments),
                args = Array.prototype.slice.call(arguments);
            args.unshift("shift");
            this.trigger.apply(this, args);
            return val;
        };
        newProto.unshift = function() {
            var val = Array.prototype.unshift.apply(this, arguments),
                args = Array.prototype.slice.call(arguments);
            args.unshift("unshift");
            this.trigger.apply(this, args);
            return val;
        };
        newProto.splice = function() {
            var val = Array.prototype.splice.apply(this, arguments),
                args = Array.prototype.slice.call(arguments);
            args.unshift("splice");
            this.trigger.apply(this, args);
            return val;
        };
        newProto.sort = function() {
            var val = Array.prototype.sort.apply(this, arguments),
                args = Array.prototype.slice.call(arguments);
            args.unshift("sort");
            this.trigger.apply(this, args);
            return val;
        };
        newProto.clear = function() {
            this.length = 0;
            this.trigger.call(this, "clear");
        };
        return newProto;
    }());

    utils.encodeDeviceId = function(deviceId) {
        var first = deviceId.substring(0, 16),
            second = deviceId.substring(16),
            idx = 0,
            result = "";
        for (idx = 0; idx < 16; idx++) {
            result += first.charAt(idx);
            result += second.charAt(idx);
        }
        return result;
    };

    utils.decodeDeviceId = function(code) {
        var first = "",
            second = "",
            idx = 0,
            result = "";
        for (idx = 0; idx < 16; idx++) {
            first += code.charAt(idx * 2);
            second += code.charAt(idx * 2 + 1);
        }
        return first + second;
    };


    module = module || {};
    module.Utils = utils;
    var currentTabCount = 0,
        isSyncportTabSelected = false;

    module.intercom = module.intercom || Intercom.getInstance();
    module.intercom.on("hiThere", function() {
        module.intercom.emit("imhere", {
                id: parseInt(Math.random() * 1000000),
                isVis: utils.isVis()
            });
    });
    module.intercom.on("imhere", function(data) {
        currentTabCount += 1;
        isSyncportTabSelected |= data.isVis;
    });

    module.debugLog = utils.debugLog;

    return module;
} ($sp));



