var PROTOCOL_HEADER_SIZE = 40,
    CHUNK_MAX_SIZE = 4 * 1000 * 1000 + PROTOCOL_HEADER_SIZE,
    pushUrl,
    file,
    transaction,
    sendableData,
    lastChunkOffset = 0,
    workerFailed = false,
    readyToFinish;

function init() {
    var reader,
        buffer;
    reader = new FileReaderSync();

    lastChunkOffset = 0;
    if (file.size > CHUNK_MAX_SIZE - PROTOCOL_HEADER_SIZE) {
        chunk = file.slice(0, CHUNK_MAX_SIZE - PROTOCOL_HEADER_SIZE);
    } else {
        chunk = file;
    }
    buffer = reader.readAsArrayBuffer(chunk);
    sendableData = buildSendData(new Uint8Array(buffer));
    postInitialized();
}

function start() {
    sendableData = sendableData(transaction.id);
    sendChunk(sendableData);
}

function buildSendData(loadedFile, transId) {
    var loadedFileLen = loadedFile.length,
        headerLen = 36,
        fileLenByteLen = 4,
        bytes = new Array(3),
        byteStr = "",
        i,
        data,
        bufferIdx,
        header,
        x = file.size;

    bytes[3] = x & 255;
    x = x >> 8;
    bytes[2] = x & 255;
    x = x >> 8;
    bytes[1] = x & 255;
    x = x >> 8;
    bytes[0] = x & 255;

    header = transId ? "send" + transId : "send";
    data = new Uint8Array(loadedFileLen + headerLen + fileLenByteLen);
    for (i = 0; i < header.length; i++) {
        data[i] = header.charCodeAt(i) & 0xff;
    }
    for (i = 0; i < 4; i++) {
        data[headerLen + i] = bytes[i];
    }
    data.set(loadedFile, headerLen + fileLenByteLen);

    function fillTransId(transId) {
        for (i = 0; i < transId.length; i++) {
            data[4 + i] = transId.charCodeAt(i) & 0xff;
        }
        return data;
    }

    return transId ? data : fillTransId;
}

function sendChunk(data) {
    var chunkOffset = lastChunkOffset,
        xhr,
        reader,
        nextChunkOffset,
        nextChunk,
        buffer;

    console.log("chunk length = " + data.length);

    if (workerFailed) {
        return;
    }

    xhr = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", function(e) {
        if (e.lengthComputable) {
            if (e.loaded > PROTOCOL_HEADER_SIZE) {
                var percentage;
                percentage = Math.round(((chunkOffset + e.loaded - PROTOCOL_HEADER_SIZE) * 100) / file.size);
                postProgress(percentage);
                console.log("file '" + file.name + "' sending.. " + percentage);
            }
        }
    }, false);

    xhr.upload.addEventListener("load", function(e){
        if (file.size == chunkOffset + e.loaded - PROTOCOL_HEADER_SIZE) {
            this.readyToFinish = true;
        }
        lastChunkOffset += data.length - PROTOCOL_HEADER_SIZE;
        data = null;

        if (!workerFailed && file.size > lastChunkOffset) {
            nextChunkOffset = file.size;
            if (nextChunkOffset > lastChunkOffset + CHUNK_MAX_SIZE - PROTOCOL_HEADER_SIZE) {
                nextChunkOffset = lastChunkOffset + CHUNK_MAX_SIZE - PROTOCOL_HEADER_SIZE;
            }
            nextChunk = file.slice(lastChunkOffset, nextChunkOffset);
            reader = new FileReaderSync();
            buffer = reader.readAsArrayBuffer(nextChunk);
            data = buildSendData(new Uint8Array(buffer), transaction.id);
            sendChunk(data);
        }
    }, false);

    xhr.open("POST", pushUrl);
    xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
    xhr.send(data.buffer);
    xhr.onreadystatechange = function(e) {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                if (this.upload.readyToFinish) {
                    postFinished();
                }
            } else if (xhr.status != 200) {
                postFailed("failed to send");
                workerFailed = true;
            }
        }
    };


}


self.onmessage = function(e) {
    var obj = e.data,
        data = obj.data;

    if (obj.action == "init") {
        file = data.file;
        pushUrl = data.pushUrl;
        init();
    } else if (obj.action == "start") {
        transaction = data.transaction;
        start();
    }
};

function _post(eventName, data) {
    self.postMessage({ eventName: eventName, data: data});
}

function postFailed(msg) {
    _post("onFailed", { msg: msg});
}

function postFinished() {
    _post("onFinished", {});
}

function postProgress(percentage) {
    _post("onProgress", { percentage: percentage });
}

function postInitialized() {
    _post("onInitialized", {});
}
