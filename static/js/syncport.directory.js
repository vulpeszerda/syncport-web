var $sp = (function(module) {
    var _commits = {},
        _directories = {};

    function SyncportFile(path, name, parentDir) {
        var hasThumbnail = false,
            thumbnailHash,
            thumbnailPath,
            deviceId = parentDir.getDevice().get("deviceId");
        this.path = path;
        this.name = name;
        this.parentDir = parentDir;
        this.thumbnailRequest = null;
        this.meta = {};

        this.getThumbnailHash = function() {
            var key, hash, url;
            if (thumbnailHash) {
                return thumbnailHash;
            }
            key = deviceId + ":" + path;
            thumbnailHash = md5(key);
            return thumbnailHash;
        };
        this.getThumbnailPath = function() {
            if (thumbnailPath) {
                return thumbnailPath;
            }
            //thumbnailPath = $sp.Remote.image.IMAGE_PATH +
            //                    this.getThumbnailHash() + ".jpg";
            thumbnailPath = $sp.Remote.image.IMAGE_QUERY_PATH +
                                this.getThumbnailHash() + ".jpg";
            return thumbnailPath;
        };
        this.isMediaFile = function() {
            return this.isAudioFile() || this.isVideoFile();
        };
        this.isAudioFile = function() {
            var lowerCaseName = name.toLowerCase();
            return lowerCaseName.endsWith(".wav") || lowerCaseName.endsWith(".mp3");
        };
        this.isVideoFile = function() {
            var lowerCaseName = name.toLowerCase();
            return lowerCaseName.endsWith(".mpg") || lowerCaseName.endsWith(".mpeg") || 
                lowerCaseName.endsWith(".mpe") || lowerCaseName.endsWith(".mp4") || 
                lowerCaseName.endsWith(".avi") || lowerCaseName.endsWith(".3gp");
        };
        this.isImageFile = function() {
            var lowerCaseName = name.toLowerCase();
            return lowerCaseName.endsWith(".gif") || lowerCaseName.endsWith(".jpg") ||
                lowerCaseName.endsWith(".jpeg") || lowerCaseName.endsWith(".png");
        };
        this.isDocumentFile = function() {
            var lowerCaseName = name.toLowerCase(),
                list = ["txt", "doc", "docx", "docm", "hwp", "log", "pdf", 
                        "rtf", "ppt", "pptx", "key", "keynote", "odp", "otp", 
                        "pez", "xls", "xlsm", "xlsx"],
                match = false;
             $.each(list, function(idx, extension) {
                 if (lowerCaseName.endsWith(extension)) {
                    match = true;
                    return false;
                 }
             });
             return match;
        };
        this.hasThumbnail = function() {
            return hasThumbnail;
        };
        this.setHasThumbnail = function(has) {
            hasThumbnail = Boolean(has);
        };
        this.setMetaInfo = function(info) {
            var prevInfo = this.meta,
                modifiedAt = new Date(info.m.replace(" ", "T")),
                meta = {};
            meta['modifiedAt'] = modifiedAt;
            if (info.artist) {
                meta['artist'] = info.artist;
            }
            if (info.album) {
                meta['album'] = info.album;
            }
            if (info.duration) {
                meta['duration'] = parseInt(info.duration);
            }
            this.meta = $.extend({}, meta);
            if (prevInfo !== this.meta) {
                this.trigger("meta", this);
            }
        };
        this.hasMetaInfo = function() {
            return Boolean(this.meta) && !$.isEmptyObject(this.meta);
        };
    }

    SyncportFile.prototype = (function(prototype){
        // extend backbone event for trigger 'change' event
        prototype = $.extend({}, Backbone.Events, prototype || {});

        prototype.isAdding = false;
        prototype.isDeleting = false;

        prototype.isFile = function() {
            return this instanceof SyncportFile;
        };

        prototype.isDirectory = function() {
            return this instanceof SyncportDirectory;
        };

        prototype.isDevice = function() {
            return this instanceof SyncportDevice;
        };

        prototype.getDevice = function() {
            if (this.isDevice()) {
                return this.device;
            }
            if (this.parentDir) {
                return this.parentDir.getDevice();
            }
            return;
        };

        prototype.getRoot = function() {
            if (this.parentDir) {
                return this.parentDir.getRoot();
            }
            return this;
        };

        return prototype;
    }());

    function SyncportDirectory(path, name, parentDir) {
        this.name = name;
        this.parentDir = parentDir;
        this.path = path;
        this.children = {};
    }

    SyncportDirectory.prototype = (function(prototype) {
        prototype = $.extend({}, prototype || {});

        prototype.addChild = function(file) {
            var filename = file.isFile() ? file.name : file.name + "/",
                that = this;
            this.children[filename.toLowerCase()] = file;
            this.trigger("add.child", this, file);
            if (!this._onMetaListener) {
                this._onMetaListener = $.proxy(function(file) {
                    this.trigger("change.child", this, file);
                }, this);
            }
            file.on("meta", this._onMetaListener);
        };

        prototype.removeChild = function(key) {
            var removed,
                stack = [],
                item,
                root;
            key = key.toLowerCase();
            removed = this.children[key];
            delete this.children[key];
            if (removed) {
                if (this._onMetaListener) {
                    removed.off("meta", this._onMetaListener); 
                }
                this.trigger("remove.child", this, removed);

                // trigger remove bubbling down
                stack.push(removed);
                while(stack.length > 0) {
                    item = stack.pop();
                    item.trigger("remove", item, removed);
                    if (item.isDirectory()) {
                        $.each(item.children, function(key, val) {
                            stack.push(val);
                        });
                    }
                }
                if (removed.isFile()) {
                    root = removed.getRoot();
                    if (removed.isImageFile()) {
                        delete root.imageFiles[removed.path];
                    } else if (removed.isAudioFile()) {
                        delete root.musicFiles[removed.path];
                    } else if (removed.isVideoFile()) {
                        delete root.videoFiles[removed.path];
                    } else if (removed.isDocumentFile()) {
                        delete root.docFiles[removed.path];
                    }
                }
            }
            return removed;
        };

        prototype.getSortedChildList = function() {
            var that = this,
                list;

            list = Object.keys(this.children).map(function(key){return that.children[key]});
            list = list.sort(function(a, b) {
                if (a.isDirectory()) {
                    return b.isDirectory() ? 
                                a.name.localeCompare(b.name) : -1;
                }
                if (b.isDirectory()) {
                    return 1;
                }
                if (that.isImageDirectory && that.isImageDirectory()) {
                    if (a.isImageFile() || a.isMediaFile()) {
                        return b.isImageFile() || b.isMediaFile() ?
                                a.name.localeCompare(b.name) : 1;
                    }
                    if (b.isImageFile() || b.isMediaFile()) {
                        return -1;
                    }
                }
                return a.name.localeCompare(b.name);
            });
            return list;
        };

        prototype.requestMetaInfos = function(callback, recursive) {
            var that = this,
                parentPaths = [],
                stack = [],
                dir;
            if (!this.getDevice().get("isTurnOn")) {
                callback && callback({success: false});
                return;
            }
            
            stack.push(this);
            while (stack.length) {
                dir = stack.pop();
                if (parentPaths.indexOf(dir.path) < 0) {
                    parentPaths.push(dir.path);
                }
                if (recursive) {
                    $.each(dir.children, function(key, file) {
                        if (!file.isFile()) {
                            stack.push(file);
                        }
                    });
                }
            }

            if (this._metaInfoReq) {
                this._metaInfoReq.abort();
            }
            this._metaInfoReq = $sp.Utils.reqMetaInfo({
                    deviceId: that.getDevice().get("deviceId"),
                    ext: ["*"],
                    paths: parentPaths,
                    onSuccess: function(meta) {
                        callback && callback({
                            success: true, meta: meta});
                    },
                    onFailure: function(cause) {
                        callback && callback({success: false});
                    }
                });

        };
        return prototype;
    }(SyncportFile.prototype));

    function SyncportDevice(deviceModel, loadFailed) {
        var _onMetaInfoReceived;

        this.device = deviceModel;
        this.parentDir = undefined;
        this.name = deviceModel.get("deviceName");
        this.children = {};
        this.path = "/";
        this.isLoaded = !loadFailed;

        // pre aggregated set
        this.imageFiles = {};
        this.videoFiles = {};
        this.musicFiles = {};
        this.docFiles = {};

        _onMetaInfoReceived = $.proxy(function(e, sender, meta) {
            var that = this;
            if (sender !== this.device.get("deviceId")) {
                return;
            }
            $.each(meta, function(key, infos) {
                var parentDir = that.getFile(key);
                if (!parentDir) {
                    return true;
                }
                $.each(infos, function(idx, info) {
                    var file = parentDir.children[info.n.toLowerCase()];
                    if (!file || !file.isFile()) {
                        return true;
                    }
                    file.setMetaInfo(info);
                });
            });
        }, this);
        $($sp.Polling).on("dirMetaRes", _onMetaInfoReceived);
    }

    SyncportDevice.prototype = (function(prototype){
        prototype = $.extend({}, prototype || {});
        prototype.getSyncRootFile = function() {
            var DEFAULT_PATH_CANDIDATES = [
                    "/mnt/sdcard/",
                    "/storage/emulated/0/",
                    "/storage/sdcard0/",
                    "/Users/*/"
                ],
                found,
                that = this;
            if (!this.isLoaded) {
                return;
            }
            if (!this._syncRootFile) {
                $.each(DEFAULT_PATH_CANDIDATES, function(idx, path) {
                    found = that.getFile(path);
                    if (found) {
                        return false;
                    }
                });
                this._syncRootFile = found ? found : this;
            }
            return this._syncRootFile;
        };

        prototype.getSyncRootPath = function() {
            var path = this.getSyncRootFile().path;
            if (!path.endsWith("/")) {
                path += "/";
            }
            return path;
        };

        prototype.getFile = function(path) {
            var removedSlashPath = path.endsWith("/") ?
                                    path.substring(0, path.length - 1) : path,
                subPaths = removedSlashPath.split("/"),
                subPathsLen = subPaths.length,
                subPath,
                parentModel,
                currModel,
                idx;

            if (subPathsLen <= 1) {
                if (!path === "/") {
                    return;
                }
                return this;
            }

            parentModel = this;
            currModel = null;

            for (idx = 1; idx < subPathsLen; idx++) {
                subPath = subPaths[idx];
                if (idx < subPathsLen - 1 || path.endsWith("/")) {
                    subPath += "/";
                }
                if (subPath === "*/") {
                    $.each(parentModel.children, function(key, val) {
                        currModel = parentModel.children[key]
                        if (currModel) {
                            return true;
                        }
                    });
                } else {
                    currModel = parentModel.children[subPath.toLowerCase()];
                }
                if (!currModel) {
                    return;
                }
                if (currModel.isDirectory()) {
                    parentModel = currModel;
                } else if (idx < subPathsLen - 1) {
                    console.error("sub path should be direcotry! not file");
                }
            }
            if ((path.endsWith("/") && currModel.isDirectory()) ||
                (!path.endsWith("/") && currModel.isFile())){
                return currModel;
            }
        };

        prototype.createFile = function(path, isAdding) {
            var removedSlashPath = path.endsWith("/") ? 
                                    path.substring(0, path.length - 1) : path,
                subPaths = removedSlashPath.split("/"),
                subPathsLen = subPaths.length,
                subPath,
                parentModel = this,
                currModel,
                currPath,
                idx;

            if (subPathsLen <= 1) {
                console.error("cannot create '/' path");
                return;
            }

            currModel = null;
            currPath = "/";

            for (idx = 1; idx < subPathsLen; idx++) {
                subPath = subPaths[idx];
                if (idx < subPathsLen - 1 || path.endsWith("/")) {
                    subPath += "/";
                }
                currModel = parentModel.children[subPath.toLowerCase()];
                currPath += subPaths[idx];
                if (!currModel) {
                    if (idx === subPathsLen - 1 && !path.endsWith("/")) {
                        currModel = new SyncportFile(currPath, subPaths[idx], parentModel);
                    } else {
                        currPath += "/";
                        currModel = new SyncportDirectory(currPath, subPaths[idx], parentModel);
                    }
                    // trigger removed path broadcast
                    if (Boolean(isAdding)) {
                        currModel.isAdding = true;
                    }
                    this.trigger("create", this, currModel);
                    parentModel.addChild(currModel);
                } else if (currModel.isDirectory()) {
                    currPath += "/";
                }
                if (currModel.isDirectory()) {
                    parentModel = currModel;
                } else if (idx < subPathsLen - 1) {
                    console.error("sub path should be direcotry! not file");
                    return;
                }
            }
            if (currModel.isAdding !== Boolean(isAdding)) {
                currModel.isAdding = Boolean(isAdding);
                currModel.parentDir.trigger("change.child", 
                    currModel.parentDir, currModel);
            }

            if (currModel.isFile()) {
                if (currModel.isImageFile()) {
                    this.imageFiles[currModel.path] = currModel;
                } else if (currModel.isAudioFile()) {
                    this.musicFiles[currModel.path] = currModel;
                } else if (currModel.isVideoFile()) {
                    this.videoFiles[currModel.path] = currModel;
                } else if (currModel.isDocumentFile()) {
                    this.docFiles[currModel.path] = currModel;
                }
            }
            return currModel;
        };

        prototype.deleteFile = function(path) {
            var file = this.getFile(path),
                parentModel,
                key;
            if (!file) {
                return;
            }
            parentModel = file.parentDir;
            key = file.isFile() ? file.name : file.name + "/"
            key = key.toLowerCase();
            return parentModel.removeChild(key);
        };

        return prototype;
    }(SyncportDirectory.prototype));

    function applyPatch(deviceModel, patch) {
        $.each(patch.addition, function(idx, path) {
            deviceModel.createFile(path);
        });

        $.each(patch.deletion, function(idx, path) {
            deviceModel.deleteFile(path);
        });
    }

    function pull(deviceId, commit, callback) {
        var params = {
                type: "directory",
                owner: deviceId,
                commit: commit ? commit : "-1"
            },
            device = _directories[deviceId],
            url;

            url = $sp.Remote.delta.PULL + "?" + $.param(params);
        $.get(url).done(function(res) {
            var additions,
                deletions,
                commit,
                patch;

            if (!res) {
                if (!device) {
                    $sp.DeviceManager.getDeviceWithId(deviceId, function(deviceModel) {
                        device = new SyncportDevice(deviceModel, true);
                        _directories[deviceId] = device;
                        callback && callback(device);
                    });
                } else {
                    callback && callback(device);
                }
                return;
            }
            commit = res.commit;
            additions = res.raw.addition;
            deletions = res.raw.deletion;

            patch = {
                    deviceId: deviceId,
                    addition: additions,
                    deletion: deletions
                };

            if (!device) {
                $sp.DeviceManager.getDeviceWithId(deviceId, function(deviceModel) {
                    device = new SyncportDevice(deviceModel);
                    _directories[deviceId] = device;
                    afterFetchCommit(callback);
                });
            } else {
                afterFetchCommit(callback);
            }

            function afterFetchCommit(callback) {
                var triggerLoad = false;
                if (!device.isLoaded) {
                    device.isLoaded = true;
                    triggerLoad = true;
                }
                applyPatch(device, patch);
                _commits[deviceId] = commit;
                callback && callback(device);
                if (triggerLoad) {
                    device.trigger("load", device);
                }
            }
        }).fail(function(res) {
            if (res.status === 400) {
                if (!device) {
                    $sp.DeviceManager.getDeviceWithId(deviceId, function(deviceModel) {
                        device = new SyncportDevice(deviceModel, true);
                        _directories[deviceId] = device;
                        callback && callback(device);
                    });
                } else {
                    callback && callback(device);
                }
            }
        });
    }

    function listener(res) {
        var owner = res.owner,
            commit = res.commit,
            lastReceivedCommit = _commits[owner];
        if (_directories[owner]) {
            pull(owner, lastReceivedCommit);
        }
    }

    function load(deviceId, callback) {
        var rootDir = _directories[deviceId];
        if (rootDir && rootDir.isLoaded) {
            callback && callback(_directories[deviceId]);
        } else {
            pull(deviceId, _commits[deviceId], callback);
        }
    }

    function refresh() {
        var deviceId;
        for (deviceId in _commits) {
            if (_directories[deviceId]) {
                pull(deviceId, _commits[deviceId]);
            }
        }
    }

    function init() {
        _directories = {};
        _commits = {};
    }

    module = module || {};
    module.Directory = {
        init: init,
        load: load,
        refresh: refresh,
        listener: listener,
        getDirectoryWithDeviceId: function(deviceId) {
            return _directories[deviceId];
        }
    }

    return module;
}($sp));
