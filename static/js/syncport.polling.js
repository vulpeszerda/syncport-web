var $sp = (function(module){
    var pollingClientId = parseInt(Math.random() * 1000000),
        lastPollResponseId;

    module  = module || {};
    module.Polling = module.Polling || {};

    module.intercom = module.intercom || Intercom.getInstance();
    module.Polling.client = (function() {
        var pollingAjaxRequest,
            isRunning = false,
            isTabChangeListenerAttached = false,
            visHandler;

        function onOtherPollStart(data) {
            if (data.sender !== pollingClientId && isRunning) {
                stop();
            }
        }

        function _init() {
            module.intercom.off("newPollStart", onOtherPollStart);
            module.intercom.emit("newPollStart", {sender: pollingClientId});
            module.intercom.on("newPollStart", onOtherPollStart);
            module.intercom.on("pollEnd", function(data) {
                module.intercom.once("key", function() {
                    if (!isRunning) {
                        start();
                    }
                }, 2);
            });
            module.intercom.on("pollResponse", function(data) {
                var type = data.response.type,
                    responseId = data.id,
                    handler = $sp.Polling.listener[type];
                if (!lastPollResponseId || responseId !== lastPollResponseId) {
                    lastPollResponseId = responseId;
                    if (handler) {
                        handler(data.response);
                    }
                }
            });
            if (!isTabChangeListenerAttached) {
                isTabChangeListenerAttached  = true;
                visHandler = function() {
                    var isVisible;
                    if (!$sp.context.account) {
                        return;
                    }
                    isVisible = $sp.Utils.isVis();
                    if (isVisible && !isRunning) {
                        start(function() {
                            $sp.DeviceManager.getAllDevices();
                            $sp.Directory.refresh();
                        });
                    }
                };
                $sp.Utils.listenVisChange(visHandler);
            }
            return true;
        }

        function start(callback, callConnect) {
            if (typeof callback === "boolean") {
                callConnect = callback;
                callback = null;
            }
            if (!_init()) {
                return;
            }
            isRunning = true;

            if (!callConnect) {
                $sp.AccountManager.getAccount(function(account) {
                    if (account) {
                        $sp.debugLog("connected");
                        doPolling();
                        callback && callback();
                    }
                });
                return;
            }
            $sp.AccountManager.getAccount(function(account) {
                if (account) {
                    $.ajax({
                        url: $sp.Remote.sync.CONNECT,
                        data: JSON.stringify({
                            device_type: account.deviceType,
                            device_name: account.deviceName
                        }),
                        contentType: "application/json",
                        dataType: "json",
                        type: "POST"
                    }).fail(function(res){
                        isRunning = false;
                        console.error(res);
                    }).done(function(res){
                        if (isRunning) {
                            $sp.debugLog("connected");
                            doPolling();
                            callback && callback();
                        }
                    });
                }
            })
        }

        function doPolling() {

            $sp.debugLog("wait..");
            pollingAjaxRequest = $.ajax({
                url: $sp.Remote.sync.POLLING,
                timeout: 30 * 1000,
                cache: false,
                dataType: "text"
            }).done(function(res) {
                var response;
                try {
                    response = $.parseJSON(res);
                    $sp.intercom.emit("pollResponse", {
                        response: response,
                        id: parseInt(Math.random() * 1000000)
                    });
                } catch (err) {
                    return;
                }
            }).always(function(data, textStatus, xhr) {
                var resObj;
                if (textStatus === "timeout" || textStatus === "success") {
                    if (textStatus === "success") {
                        try {
                            resObj = $.parseJSON(data);
                        } catch (err) {
                            if ($("#notify-polling-failure").size() === 0) {
                                $sp.notify({
                                    html: "<span id='notify-polling-failure'>" +
                                        $sp.string("failed_to_polling") + "</span>"
                                }, "error", {
                                    clickToHide: false,
                                    autoHide: false
                                });
                            }
                            return;
                        }
                    }
                    doPolling();
                } else if (textStatus !== "abort") {
                    if (data.status === 401) {
                        return;
                    }
                    if ($("#notify-polling-failure").size() === 0) {
                        $sp.notify({
                            html: "<span id='notify-polling-failure'>" +
                                $sp.string("failed_to_polling") + "</span>"
                        }, "error", {
                            clickToHide: false,
                            autoHide: false
                        });
                    }
                }
            });
        }

        function stop(callback, callDisconnect) {
            if (pollingAjaxRequest) {
                pollingAjaxRequest.abort();
            }
            isRunning = false;
            if (!callDisconnect) {
                callback && callback();
                return;
            }
            $sp.AccountManager.getAccount(function(account) {
                if (!account) {
                    callback && callback();
                    return;
                }
                $.ajax({
                    url: $sp.Remote.sync.DISCONNECT,
                    data: {
                        device_type: account.deviceType,
                        device_name: account.deviceName
                    },
                    dataType: "json",
                    type: "POST"
                }).fail(function(res){
                    console.error(res);
                }).done(function(res){
                    $sp.debugLog("disconnected");
                }).always(function(res) {
                    callback && callback();
                });
            });
        }

        function getIsRunning() {
            return isRunning;
        }
        return {
            start: start,
            stop: stop,
            isRunning: getIsRunning
        };
    }());

    module.Polling.listener = {
        device: function(res) {
            $sp.DeviceManager.pollListener(res);
        },
        directory: function(res) {
            $(module.Polling).trigger("dirChanged", res.owner);
        },
        collection: function(res) {
            $(module.Polling).trigger("colChanged", res.owner);
        },
        thumbnail: function(res) {
            $(module.Polling).trigger("thumbnailUploaded", res.hashes);
        },
        negotiation: function(res) {
            $sp.FileTransfer.webP2pNegotiationHandler(res);
        },
        address: function(res) {
            $sp.FileTransfer.webP2pAddressHandler(res);
        },
        notify_fail: function(res) {
            $sp.FileTransfer.notifyFailureHandler(res);
        },
        taping_progress: function(res) {
            $(module.Polling).trigger("tapingProgress." + res.id, {
                path: res.path,
                progress: res.progress
            });
        },
    };

    return module;
}($sp));
