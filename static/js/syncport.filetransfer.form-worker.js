var pushUrl,
    file,
    transaction,
    readyToFinish,
    formData;

function init() {
    postInitialized();
}

function start() {
    var xhr;
    formData = new FormData()
    formData.append("key", "send" + transaction.id);
    formData.append("filesize", file.size);
    formData.append("file", file);

    xhr = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", function(e) {
        if (e.lengthComputable) {
            var percentage;
            percentage = Math.round((e.loaded * 100) / file.size);
            postProgress(percentage);
        }
    }, false);

    xhr.upload.addEventListener("load", function(e){
    }, false);

    xhr.open("POST", pushUrl);
    xhr.send(formData);
    xhr.onreadystatechange = function(e) {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                postFinished();
            } else if (xhr.status != 200) {
                postFailed("failed to send");
                workerFailed = true;
            }
        }
    };


}


self.onmessage = function(e) {
    var obj = e.data,
        data = obj.data;

    if (obj.action == "init") {
        file = data.file;
        pushUrl = data.pushUrl;
        init();
    } else if (obj.action == "start") {
        transaction = data.transaction;
        start();
    }
};

function _post(eventName, data) {
    self.postMessage({ eventName: eventName, data: data});
}

function postFailed(msg) {
    _post("onFailed", { msg: msg});
}

function postFinished() {
    _post("onFinished", {});
}

function postProgress(percentage) {
    _post("onProgress", { percentage: percentage });
}

function postInitialized() {
    _post("onInitialized", {});
}
