var $sp = (function(module) {
    var proxyPath = "/proxy/",
        api,
        delta,
        sync,
        domain,
        image;

    domain = (function() {
        var domainEndIdx,
            domainStartIdx,
            host,
            tmp;
        host = window.location.hostname;
        domainEndIdx = host.lastIndexOf(".");
        tmp = host.substring(0, domainEndIdx);
        domainStartIdx = tmp.lastIndexOf(".");
        if (domainStartIdx > -1) {
            return host.substring(domainStartIdx + 1);
        }
        return host;
    }());

    module = module || {};
    module.Remote = module.Remote || {};

    function getProxyUrl(type) {
        return proxyPath + type + "/";
    }

    api = (function() {
        var VERSION = "v1";

        function endpoint(name) {
            return getProxyUrl("api") + "api/" + VERSION + "/" + name + "/";
        };

        return {
            FINDPWD: endpoint("users/findpwd"),
            SIGNIN: endpoint("users/signin"),
            SIGNUP: endpoint("users/signup"),
            SIGNOUT: (function() {
                    var scheme;
                    if (window.location.host.startsWith("dev.")) {
                        scheme = "http";
                    } else {
                        scheme = "https";
                    }
                    return scheme + "://" + window.location.host + "/signout";
                }()),
            USER_SELF: endpoint("users/self"),
            DEVICE_ALL: endpoint("devices/all"),
            DEVICE_SELF: endpoint("devices/self"),
            INQUIRY: endpoint("async/inquiry_mail"),
            REGISTER_TESTER: "/register_tester",
            FORGOT: endpoint("users/forgot"),
            CPR: endpoint("devices/cpr"),
            RCPR: endpoint("devices/rcpr")
        };
    }());

    delta = (function() {
        var VERSION = "0";

        function endpoint(name) {
            return getProxyUrl("delta") + VERSION + "/" + name + "/";
        };

        return {
            PULL: endpoint("delta_pull"),
            PUSH: endpoint("delta_push"),
            SYNCROOT: endpoint("syncroot"),
            START_FILE_TRANSACTION: endpoint("start_file_transaction"),
            END_FILE_TRANSACTION: endpoint("end_file_transaction")
        }
    }());

    sync = (function() {
        var VERSION = "0";

        function endpoint(name) {
            return getProxyUrl("sync") + VERSION + "/" + name + "/";
        };

        return {
            CONNECT: endpoint("connect"),
            DISCONNECT: endpoint("disconnect"),
            POLLING: endpoint("long_poll"),
            ACTIVE_DEVICES: endpoint("devices")
        }
    }());


    image = {
        IMAGE_PATH: "http://temp-thumbnail." + domain + "/",
        IMAGE_QUERY_PATH: "http://temp-thumbnail." + domain + "/q/"
        //IMAGE_QUERY_PATH: getProxyUrl("temp-thumbnail")
    };

    module.Remote.api = api;
    module.Remote.delta = delta;
    module.Remote.sync = sync;
    module.Remote.image = image;

    return module;
}($sp));
