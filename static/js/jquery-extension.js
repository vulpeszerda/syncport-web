$.fn.dndhover = function(options) {

    return this.each(function() {

        var self = $(this);
        var collection = $();

        self.on('dragenter', function(event) {
            if (collection.size() === 0) {
                self.trigger('dndHoverStart');
            }
            collection = collection.add(event.target);
        });

        self.on('dragleave', function(event) {
            /*
             * Firefox 3.6 fires the dragleave event on the previous element
             * before firing dragenter on the next one so we introduce a delay
             */
            setTimeout(function() {
                collection = collection.not(event.target);
                if (collection.size() === 0) {
                    self.trigger('dndHoverEnd');
                }
            }, 1);
        });
    });
};

/**
 * Get text width of 'sometext' or contents in DOM element 'this'
 * This only for SINGLELINE text contents element.
 * XXX: Because this function use $.textWidth function internally,
 *      this function has dependancy in HTML5 canvas API
 */
$.fn.textWidth = function(text) {
    /**
     * Uses canvas.measureText to compute and return the width of the given 
     * text of given font in pixels.
     *
     * @param {String} text The text to be rendered.
     * @param {String} font The css font descriptor that text is to be 
     *                  rendered with (e.g. "bold 14px verdana").
     *
     * @see http://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
     */
    function getTextWidth(text, font) {
        // re-use canvas object for better performance
        var canvas,
            context,
            metrics,
        canvas = getTextWidth.canvas || 
                    (getTextWidth.canvas = document.createElement("canvas"));
        context = canvas.getContext("2d");
        context.font = font;
        metrics = context.measureText(text);
        return metrics.width;
    };

    var font = this.css("font");
    text = text || this.text().trim();
   return getTextWidth(text, font);
};

/**
 * Make string truncate or ellipsis to fit container $el.
 * This only for SINGLELINE text contents element.
 * XXX: 1) Because this function use $.textWidth function internally,
 *          this function has dependancy in HTML5 canvas API
 *      2) This function should be called on fixed width $el because 
 *          this calculates proper max width with $el.width().
 * 
 * @param {Object} options in form 
 *        { separator : '...', ellipsis: 'start|end|middle', width: '[0-9]+'}
 *        default is { separator: '...', ellipsis: middle }
 */
$.fn.ellipsis = function(options) {
    var text = this.text().trim(),
        oriText = text,
        length = text.length,
        textWidth,
        containerWidth = options.width || this.width(),
        divider = parseInt(length / 2),
        ellipsis,
        separator,
        ELLIPSIS = {
            start: function (text, length, separator) {
                var l;
                l = length - separator.length;
                return separator + text.substr(text.length - l, l);
            },
            middle: function(text, length, separator) {
                var l, 
                    lhsLen, rhsLen, 
                    startStr, endStr;

                l = length - separator.length;
                lhsLen = Math.ceil(l / 2);
                rhsLen = Math.floor(l / 2);
                startStr = text.substr(0, lhsLen);
                endStr = text.substr(text.length - rhsLen, rhsLen);
                return startStr + separator + endStr;
            },
            end: function(text, length, separator) {
                var l;
                l = length - separator.length;
                return text.substr(0, l) + separator;
            }
        };

    ellipsis = ELLIPSIS[options.ellipsis || "middle"];
    separator = options.separator || "...";

    if (!containerWidth) {
        return;
    }

    textWidth = this.textWidth(text);
    if (textWidth < containerWidth) {
        return;
    }

    length = length + separator.length;
    text = ellipsis(oriText, length, separator);

    while(true) {
        textWidth = this.textWidth(text);
        if (textWidth < containerWidth) {
            if (divider <= 1) {
                break;
            }
            divider = parseInt(divider / 2);
            length += divider;
        } else {
            if (divider <= 1) {
                divider = 1;
            } else {
                divider = parseInt(divider / 2);
            }
            length -= divider;
        }
        text = ellipsis(oriText, length, separator);
    }
    this.text(text);
    this.attr("title", oriText);
};
