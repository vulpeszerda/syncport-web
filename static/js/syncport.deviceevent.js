var $sp = (function(module){
    var Event,
        EventManager = {},
        _deviceEvents = {};

    Event = function(type, params) {
        this.type = type;
        return $.extend(this, params);
    }
    
    EventManager.getEvents = function(deviceId) {
        if (!_deviceEvents[deviceId]) {
            _deviceEvents[deviceId] = [];
        }
        return _deviceEvents[deviceId];
    };

    EventManager.pushSendEvent = function(deviceId, path) {
        var events = EventManager.getEvents(deviceId),
            idx, filename, parentPath;

        idx = path.lastIndexOf("/");
        parentPath = path.substring(0, idx);
        filename = path.substring(idx + 1);

        events.push(new Event("send", {
            file: filename,
            parentPath: parentPath
        }));
    };

    EventManager.pushDownloadEvent = function(deviceId, path) {
        var events = EventManager.getEvents(deviceId),
            idx, filename, parentPath;

        idx = path.lastIndexOf("/");
        parentPath = path.substring(0, idx);
        filename = path.substring(idx + 1);

        events.push(new Event("download", {
            file: filename,
            parentPath: parentPath,
            requestDownload: function() {
                $sp.FileTransfer.registerReceiveFileWorker(deviceId, path);
            }
        }));
    };

    EventManager.pushCompressRequestEvent = function(deviceId, paths) {
        var events = EventManager.getEvents(deviceId);

        events.push(new Event("compressRequest", {
            paths: paths
        }));
    };

    EventManager.pushCompressCompleteEvent = function(deviceId, paths, path) {
        var events = EventManager.getEvents(deviceId);
        events.push(new Event("compressComplete", {
            paths: paths,
            completePath: path,
            requestDownload: function() {
                $sp.FileTransfer.registerReceiveFileWorker(deviceId, path);
            }
        }));
    };

    module  = module || {};
    module.DeviceEventManager = EventManager;
    return module;
}($sp));
