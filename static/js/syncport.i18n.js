var $sp = (function(module) {
    // Library origin
    // https://github.com/gammasoft/browser-i18n
    var I18n = function(options){
        for (var prop in options) {
            this[prop] = options[prop];
        };

        this.setLocale(this.locale);
    };

    I18n.localeCache = {};

    I18n.prototype = {
        defaultLocale: "en",
        directory: "/static/locales",
        extension: ".json",

        getLocale: function(){
            return this.locale;
        },

        setLocale: function(locale){
            if(!locale) {
                locale = $("html").attr("lang");
            }

            if(!locale) {
                locale = this.defaultLocale;
            }

            this.locale = locale;

            if(!(locale in I18n.localeCache)) {
                this.getLocaleFileFromServer();
            }
        },

        getLocaleFileFromServer: function(){
            var localeFile;
            localeFile = null;

            $.ajax({
                url: this.directory + "/" + this.locale + this.extension,
                async: false,
                dataType: 'json',
                success: function(data){
                    localeFile = data;
                }, 
                error: function(res) {
                    console.error(res.responseText);
                }
            });

            I18n.localeCache[this.locale] = localeFile;
        },

        __: function(){
            var msg = I18n.localeCache[this.locale][arguments[0]];

            if (arguments.length > 1) {
                msg = vsprintf(msg, Array.prototype.slice.call(arguments, 1));
            }
            if (!msg) {
                console.error(arguments[0] + " is undefined");
                return "undefined";
            }
            if (msg.indexOf("\n") > -1) {
                msg = msg.replace(/\n/g, "<br/>");
            }
            return msg;
        },

        __n: function(singular, count){
            var msg = I18n.localeCache[this.locale][singular];

            count = parseInt(count, 10);
            if(count === 0)
                msg = msg.zero;
            else
                msg = count > 1 ? msg.other : msg.one;

            msg = vsprintf(msg, [count]);
            if (arguments.length > 2)
                msg = vsprintf(msg, Array.prototype.slice.call(arguments, 2));

            return msg;
        }
    };

    module = module || {};
    module.I18n = I18n;

    // Add shortcut

    // init i18n
    var locale = $sp.context ? $sp.context.locale || "en_US" : "en_US";

    module.i18n = new $sp.I18n({locale: locale});

    module.string = function(args) {
        return module.i18n.__.apply($sp.i18n, arguments);
    };

    module.string_n = function(args) {
        return module.i18n.__n.apply($sp.i18n, arguments);
    };

    return module;
}($sp));
