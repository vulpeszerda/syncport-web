var $sp = (function(module){
    var pageFrame = "body #page",
        views = {},
        // General views
        SyncportView,
        SyncportPageView,
        SyncportAuthPageView,

        MinimizableModalView,

        // Base abstract view
        BaseDevicePageView,
        CollectionPageView,

        BaseDeviceContentView,
        BaseCollectionContentView,
        BaseFileCollectionContentView,

        BaseContextMenuView,


        // Device page components
        DeviceView,
        DeviceLoadFailureView,
        DeviceLoadingView,
        DeviceDropzoneView,
        DeviceConnectionCheckView,
        FileOpenView,

        FileContextMenuView,
        AlbumContextMenuView,

        DirListView,
        AlbumListView,
        AlbumView,
        MusicListView,
        VideoListView,
        DocumentListView,
        DeviceSettingsContentView,
        DeviceEventsContentView;


    SyncportView = Backbone.View.extend({
        initialize: function(options) {
            var context,
                parentView;
            options = options || {};

            context = options.context || {};
            context = $.extend({}, $sp.context, context);
            this._context = { _context: context };

            parentView = options.parentView;
            if (parentView) {
                this.parentView = parentView;
            }

            if (this.template) {
                this._template = _.template($(this.template).html());
            }
        },
        events: {
            "click .page-inner-link": "pageInnerLink"
        },
        pageInnerLink: function(e) {
            var $el = $(e.currentTarget),
                targetId = $el.data("target"),
                $target = $("#" + targetId),
                scrollTop;
            if (!$target.size()) {
                return;
            }
            scrollTop = $target.offset().top;
            $(window).scrollTop(scrollTop);
            e.stopPropagation();
        }
    });

    SyncportPageView = SyncportView.extend({
        inquiryModalTmpl : "#tmpl-modal-inquiry",
        initialize: function(options) {
            var that = this,
                modalTarget;
            SyncportView.prototype.initialize.call(this, options);

            modalTarget = "#modal-inquiry .btn-send-inquiry";
            $("#modal").on("click", modalTarget, function(e) {
                that.sendInquiry.call(that, e);
            });
        },
        events: function() {
            var events = SyncportView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend({}, events, {
                "click .link-home": "navHome",
                "click .link-how-to-use": "navHowToUse",
                "click .link-download": "navDownload",
                "click .link-signin": "navSignin"
            });
        },
        navHome: function() {
            $sp.router.navigate('', { trigger: true });
        },
        navHowToUse: function() {
            $sp.router.navigate("how-to-use", { trigger: true });
        },
        navDownload: function() {
            $sp.router.navigate("download", { trigger: true });
        },
        navSignin: function() {
            $sp.router.navigate("signin", { trigger: true });
        },
        sendInquiry: function(e) {
            var that = this,
                $modal = $("#modal-inquiry"),
                $title = $(".input-inquiry-subject", $modal),
                $email = $(".input-email", $modal),
                $content = $(".input-inquiry", $modal),
                title = $title.val(),
                email = $email.val(),
                content = $content.val(),
                $target = $(e.currentTarget),
                errors = [];

            if ($target.attr("disabled")) {
                return;
            }

            if (email === "") {
                errors.push({
                    msg: $sp.string("need_feedback_sender_email"),
                    $target: $title
                });
            }
            if (title === "") {
                errors.push({
                    msg: $sp.string("need_feedback_title"),
                    $target: $title
                });
            }
            if (content === "") {
                errors.push({
                    msg: $sp.string("need_feedback_contents"),
                    $target: $content
                });
            }
            if (errors.length) {
                showError(errors);
                return;
            }

            $title.attr("readonly", "readonly");
            $email.attr("readonly", "readonly");
            $content.attr("readonly", "readonly");

            $target.attr("disabled", "true");
            $target.addClass("progress");
            $(".text", $target).html($sp.string("sending_feedback"));

            $.ajax({
                url: $sp.Remote.api.INQUIRY,
                data: JSON.stringify({
                    sender: email,
                    subject: title,
                    content: content
                }),
                contentType: "application/json",
                dataType: "json",
                type: "POST"
            }).done(function(res) {
                $sp.notify($sp.string("feedback_registered"), "success");
                that.hideInquiryBox();
            }).fail(function(res, textStatus) {
                if (textStatus !== "abort") {
                    showError(res);
                }
            }).always(function(res) {
                $target.removeAttr("disabled");
                $target.removeClass("progress");
                $(".text", $target).html($sp.string("send_feedback"));

                $title.removeAttr("readonly");
                $email.removeAttr("readonly");
                $content.removeAttr("readonly");
            });

            function showError(errors) {
                var $errorPrompt = $(".error-prompt", $modal);
                if (!errors instanceof Array) {
                    errors = [{ msg: errors}];
                }
                $errorPrompt.html("");
                $.each(errors, function(idx, el) {
                    $errorPrompt.append(el.msg + "<br/>");
                    if (el.$target) {
                        el.$target.addClass("error");
                    }
                });
                if (!$errorPrompt.is(":visible")) {
                    $errorPrompt.slideDown();
                }
            }
        },
        showInquiryBox: function() {
            var tmpl;
            if (!this._inquiryModalTmpl) {
                tmpl = $(this.inquiryModalTmpl).html();
                this._inquiryModalTmpl = _.template(tmpl);
            }
            $("#modal").append(this._inquiryModalTmpl(this._context));
            $("#modal-inquiry").modal("show")
                .on("hidden.bs.modal", function() {
                    $(this).remove();
                });

        },
        hideInquiryBox: function() {
            $("#modal-inquiry").modal("hide");
        }
    });

    SyncportAuthPageView = SyncportPageView.extend({
        accountDropdownTmpl: "#tmpl-dropdown-account",
        accountDropdownTarget: "header .account-menu .account",

        initialize: function(options) {
            var that = this;

            SyncportPageView.prototype.initialize.call(this, options);

            $("#dropdown").on("click", "#dropdown-account .menu", function(e) {
                var $target = $(e.currentTarget);
                if ($target.hasClass("menu-account-settings")) {
                    that.navAccountSettings.apply(that, arguments);
                } else if ($target.hasClass("menu-signout")){
                    that.doSignout.apply(that, arguments);
                } else if ($target.hasClass("menu-inquiry")) {
                    that.showInquiryBox();
                }
                that.hideAccountMenu();
            });
        },
        events: function() {
            var events = SyncportPageView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend ({}, events, {
                "click .btn-signout": "doSignout",
                "click header .account": "showAccountMenu"
            });
        },
        navAccountSettings: function() {
            $sp.router.navigate('settings', { trigger: true });
        },
        doSignout: function() {
            $sp.AccountManager.signout(function() {
                window.location.href = "/";
            })
        },
        showAccountMenu: function() {
            var that = this,
                tmpl,
                $target = $(this.accountDropdownTarget),
                $dropdown;
            if (!this._accountDropdownTmpl) {
                tmpl = $(this.accountDropdownTmpl).html();
                this._accountDropdownTmpl = _.template(tmpl);
            }
            $("#dropdown").append(this._accountDropdownTmpl(this._context));
            $dropdown = $("#dropdown-account");
            $dropdown.css({
                top: $target.offset().top + $target.height() -
                    $(window).scrollTop(),
                left: $target.offset().left - ($dropdown.width() / 2) +
                    ($target.width() / 2)
            });
            $("#filter").show().one("click", function() {
                that.hideAccountMenu();
            });
        },
        hideAccountMenu: function() {
            $("#dropdown #dropdown-account").remove();
            $("#filter").click();
            $("#filter").hide();
        }
    });

    BaseDevicePageView = SyncportAuthPageView.extend({
        template: "#tmpl-page-device",
        tagName: "div",
        className: "page-device",
        menuType: "undefined",
        id: function() {
            return "page-" + this.menuType;
        },
        initialize: function(options) {
            SyncportAuthPageView.prototype.initialize.call(this, options);
            _.bindAll(this, "startLoadDevice", "onThumbnailUploaded");

            this._options = options;
            $(pageFrame).html(this.el);
            this.render();

            $($sp.Polling).on("thumbnailUploaded", this.onThumbnailUploaded);

        },
        events: function() {
            var events = SyncportAuthPageView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend ({}, events, {
                "click .device-menu:not(.selected)": "navDeviceMenu"
            });
        },
        navDeviceMenu: function(e) {
            var $target = $(e.currentTarget),
                navMenu = $target.data("target"),
                device = this.subViews.selectedDeviceView.getSelectedDevice(),
                encodedDeviceId = $sp.Utils.encodeDeviceId(device.get("deviceId"));
            $sp.router.navigate(
                "/device/" + encodedDeviceId + "/" + navMenu, { trigger: true });
        },

        getUploadDir: function() {
            var contentView = this.subViews.contentView;
            return contentView.getUploadDir.call(contentView);
        },
        remove: function() {
            if (this.subViews) {
                $.each(this.subViews, function(key, view) {
                    view.remove && view.remove();
                });
            }
            $($sp.Polling).off("thumbnailUploaded", this.onThumbnailUploaded);
            SyncportAuthPageView.prototype.remove.call(this);
        },
        startLoadDevice: function(device, path) {
            var that = this;
            this.showLoading();
            this.loadDevice(device, path, function(success) {
                that.hideLoading(device, path, success);
            });
        },
        loadDevice: function(device, path, callback) {
            throw "Not implemented";
        },
        render: function() {
            this.$el.html(this._template(this._context));
            this.renderSubViews();
        },
        renderSubViews: function() {
            this.subViews = this.subViews || {};
            this.subViews.emptyView = (function() {
                var $view = $(".empty-view", this.$el),
                    $deviceView = $("header .device-select-menu", this.$el),
                    $deviceSubView = $("#device-subviews", this.$el),
                    view = {};
                view.isVisible = function() {
                    return $view.is(":visible");
                };
                view.hide = function() {
                    $deviceView.show();
                    $deviceSubView.show();
                    $view.hide();
                };
                view.show = function() {
                    $deviceView.hide();
                    $deviceSubView.hide();
                    $view.show();
                };
                return view;
            }());

            this.subViews.loadingView = new DeviceLoadingView(
                $.extend({}, this._options, {
                    el: "#loading-indicator",
                    parentView: this
                }));

            this.subViews.loadFailureView = new DeviceLoadFailureView(
                $.extend({}, this._options, {
                    el: "#load-failure",
                    parentView: this
                }));

            this.subViews.selectedDeviceView = new DeviceView(
                $.extend({}, this._options, {
                    el: "header .device-select-menu",
                    parentView: this
                }));

            this.subViews.connectionCheckView = new DeviceConnectionCheckView(
                $.extend({}, this._options, {
                    elHolder: "#connection-check-indicator-holder",
                    parentView: this
                }));
        },
        showLoading: function() {
            this.subViews.loadFailureView.hide();
            $("#device-content-wrapper").hide();
            this.subViews.loadingView.show();
        },
        hideLoading: function(device, path, success) {
            this.subViews.loadingView.hide();
            if (success) {
                $("#device-content-wrapper").show();
            } else {
                this.subViews.loadFailureView.show(device, path);
            }
        },
        onThumbnailUploaded: function() {
            var $statusText = $("header .app-status-text"),
                statusText,
                hashes = Array.prototype.slice.call(arguments),
                e = hashes.splice(0, 1);

            // display thumbnail loading status
            statusText = $sp.string("loading_thumbnails");
            statusText += "<img src=\"/static/images/loader.gif\"/>";
            $statusText.html(statusText);
            if (this._thumbnailUploadStatusTimeout) {
                clearTimeout(this._thumbnailUploadStatusTimeout);
            }
            this._thumbnailUploadStatusTimeout = setTimeout(function() {
                var $el = $("header .app-status-text");
                if ($el.size() > 0) {
                    $el.html("");
                }
            }, 5000);
        }
    });

    CollectionPageView = BaseDevicePageView.extend({
        menuType: undefined,
        getContentView: function(options) {
            throw "Not implemented";
        },
        getContextMenuView: function(options) {
            return new FileContextMenuView(options);
        },
        renderSubViews: function() {
            this.subViews = this.subViews || {};
            this.subViews.contentView = this.getContentView(
                $.extend({
                    el: "#device-content",
                    parentView: this
                }, this._options));

            this.subViews.dropzoneView = new DeviceDropzoneView(
                $.extend({}, this._options, {
                    parentView: this,
                    sendFile: this.subViews.contentView.sendFile
                }));

            this.subViews.contextMenuView = this.getContextMenuView(
                $.extend({}, this._options, {
                    elHolder: "#context-menu-holder",
                    parentView: this
                }));

            BaseDevicePageView.prototype.renderSubViews.call(this);
        },
        loadDevice: function(device, path, callback) {
            this.subViews.contentView.loadDevice(device, path, callback);
        }
    });

    DeviceLoadingView = SyncportView.extend({
        template: "#tmpl-dir-list-progress",
        initialize: function(options) {
            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "hide", "show");
            this.$el.html(this._template(this._context));
        },
        show: function() {
            this.$el.show();
        },
        hide: function() {
            this.$el.hide();
        }
    });

    BaseDeviceContentView = SyncportView.extend({
        uploadModalTmpl: "#tmpl-modal-upload",
        createFolderModalTmpl: "#tmpl-modal-create-folder",
        initialize: function(options) {
            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "remove", "loadDevice", "sendFile",
                "getUploadDir", "adjustContentFrame", "sendFormFiles", "showUploader",
                "createFolder", "sendRemoteAddDel", "showCreateFolderModal");

            $(window).on("resize", this.adjustContentFrame);
            $("#modal").on("click", "#modal-upload .btn-send", this.sendFormFiles);
        },
        events: function() {
            var events = SyncportView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend ({}, events, {
                "click .action-show-uploader" : "showUploader",
                "click .action-create-folder" : "showCreateFolderModal",
            });
        },
        onDeviceStateChanged: function(device) {
            if (device.get("isTurnOn")) {
                $(".action-show-uploader", this.$el).removeAttr("disabled");
                $(".action-create-folder", this.$el).removeAttr("disabled");
            } else {
                $(".action-show-uploader", this.$el).attr("disabled", "true");
                $(".action-create-folder", this.$el).attr("disabled", "true");
                $("#modal-upload").modal("hide")
                    .on("hidden.bs.modal", function() {
                        $(this).remove();
                    });
            }
        },
        sendFormFiles: function(e) {
            var that = this,
                target = $("#modal-upload input[type='file']")[0],
                doUpload,
                device = this.getUploadDir().getDevice();
            if (!target.files || target.files.length === 0) {
                $sp.notify($sp_stirng("nothing_to_send"), "success");
                return;
            }
            doUpload = function() {
                $.each(target.files, function(idx, file) {
                    that.sendFile(file);
                });
                that.hideUploader();
                that.hideCreateFolderModal();
            };
            if (device.get("connectionState") === "charged") {
                $sp.confirm({
                        title: $sp.string(
                            "warning_data_charge__device_charged_connected__title"),
                        text: $sp.string(
                            "warning_data_charge__device_charged_connected__text"),
                        confirmText: $sp.string("proceed_send"),
                        closeText: $sp.string("cancel")
                    }, function(confirmed) {
                        if (confirmed) {
                            doUpload();
                        }
                    });
            } else {
                doUpload();
            }
        },
        showUploader: function(e) {
            var tmpl,
                context,
                $target = $(".action-show-uploader", this.$el);

            if ($target.attr("disabled")) {
                return;
            }

            if (!this._uploadModalTmpl) {
                tmpl = $(this.uploadModalTmpl).html();
                this._uploadModalTmpl = _.template(tmpl);
            }
            context = $.extend({}, this._context, {
                targetDirModel: this.getUploadDir()
            });

            $("#modal").append(this._uploadModalTmpl(context));
            $("#modal-upload").modal("show").on("hidden.bs.modal", function() {
                $(this).remove();
            });
        },
        hideUploader: function(e) {
            $("#modal-upload").modal("hide");
        },
        showCreateFolderModal: function(e) {
            var tmpl,
                context,
                $target = $(".action-create-folder", this.$el),
                that = this;

            if ($target.attr("disabled")) {
                return;
            }

            if (!this._createFolderModalTmpl) {
                tmpl = $(this.createFolderModalTmpl).html();
                this._createFolderModalTmpl = _.template(tmpl);
            }
            context = $.extend({}, this._context, {
                targetDirModel: this.getUploadDir()
            });

            $("#modal").append(this._createFolderModalTmpl(context));
            $("#modal-create-folder").modal("show")
                .on("hidden.bs.modal", function() {
                    $(this).remove();
                })
                .on("click", ".btn-apply", this.createFolder);
        },
        hideCreateFolderModal: function(e) {
            $("#modal-create-folder").modal("hide");
        },
        createFolder: function(e) {
            var hasDupplicatedName = false,
                text, path, request,
                $modal = $("#modal-create-folder"),
                $errorPrompt = $(".error-prompt", $modal),
                newFolderName = $("#input-new-folder-name", $modal).val(),
                $target = $(e.currentTarget);

            if ($target.attr("disabled")) {
                return;
            }
            if (newFolderName === "") {
                $errorPrompt.html($sp.string("required_field"));
                if ($errorPrompt.is(":hidden")) {
                    $errorPrompt.slideDown();
                }
                return;
            }

            hasDupplicatedName = Boolean(
                this.getUploadDir().items[newFolderName.toLowerCase() + "/"]);
            if (hasDupplicatedName) {
                text = $sp.string("already_has_%s_folder_name", newFolderName);
                text += $sp.string("enter_new_folder_name");
                $errorPrompt.html(text);
                if ($errorPrompt.is(":hidden")) {
                    $errorPrompt.slideDown();
                }
                return;
            }

            $target.attr("disabled", true)
                .addClass("progress")
                .find(".text").html($sp.string("sending_request"));
            $errorPrompt.html("").slideUp();

            path = this.getUploadDir().path + newFolderName + "/";
            request = this.sendRemoteAddDel(this.getCollection(), [path]);
            request.done(function(res) {
                request.done && request.done.apply(this, arguments);
                $modal.modal("hide");
            }).fail(function(jqXHR, textStatus, errorThrown) {
                request.fail && request.fail.apply(this, arguments);
                $target.removeAttr("disabled")
                    .removeClass("progress");
                $(".text", $target).html($sp.string("apply_create_folder"));
            });
        },
        sendRemoteAddDel: function(collection, additions, deletions, callback) {
            var that = this;
            deletions = deletions || [];
            additions = additions || [];
            return $.ajax({
                url: $sp.Remote.delta.PUSH,
                data: JSON.stringify({
                    delta: {
                        type: "remote_adddel",
                        receiver: collection.device.get("deviceId"),
                        addition: additions,
                        deletion: deletions
                    }
                }),
                dataType:"json",
                type:"POST",
                contentType:"application/json"
            }).done(function(res) {
                var filesDict = collection.getFilesDict();

                $.each(additions, function(idx, addition) {
                    collection.createTempFile(addition, true);
                });

                $.each(deletions, function(idx, deletion) {
                    var fileModel = filesDict[deletion],
                        $fileEl;
                    if (!fileModel) {
                        return true;
                    }
                    fileModel.isDeleting = true;
                    $fileEl = that.getItemEl(fileModel);
                    if ($fileEl) {
                        $fileEl.addClass("nonselectable").removeClass("selected");
                    }
                });
            }).always(function() {
                callback && callback.apply(that, arguments);
            });
        },
        sendFile: function(file, localPath) {
            var uploadDir,
                device,
                pushPath,
                idx,
                toParentPath,
                fromParentPath,
                registerSuccess = false,
                reader;

            uploadDir = this.getUploadDir();
            device = uploadDir.getDevice();

            localPath = localPath || "/" + file.name;

            if ($sp.context.browser.currentBrowser[0] !== "Chrome" &&
                $sp.context.browser.currentBrowser[0] !== "IE" &&
                !file.type && file.size % 4096 === 0 && file.size <= 102400) {
                try {
                    reader = new FileReaderSync();
                    reader.readAsBinaryString(file);
                } catch (NS_ERROR_FILE_ACCESS_DENIED) {
                    $sp.alert(
                        $sp.string("failed_to_send_directory%s",
                            $sp.context.browser.currentBrowser[0]));
                    return false;
                }
            }

            pushPath = uploadDir.path + localPath.substring(1);

            if (!device.get("isTurnOn") ||
                $sp.FileTransfer.hasSendFileWorker(device.get("deviceId"),
                    pushPath)) {
                return false;
            }

            idx = pushPath.lastIndexOf(file.name);
            toParentPath = pushPath.substring(0, idx);

            if (toParentPath === "//") { toParentPath = "/" };
            idx = localPath.lastIndexOf(file.name);
            fromParentPath = localPath.substring(0, idx);

            return $sp.view.sendFilesView.addJob({
                    file: file,
                    path: pushPath,
                    peerId: device.get("deviceId"),
                    toParentPath: toParentPath,
                    fromParentPath: fromParentPath
                });
        },
        remove: function() {
            $("#modal").off("click", "#modal-upload");
            $(window).off("resize", this.adjustContentFrame);
            this.unbindVisibilityTracker();
            SyncportView.prototype.remove.call(this);
        },
        adjustContentFrame: function() {
            var $fixedContainer= $(".fixed-container", this.$el),
                $placeholder = $fixedContainer.next(),
                height = $(window).height(),
                dirmodelY;

            if ($fixedContainer.size() === 0) {
                return;
            }
            if ($fixedContainer.innerHeight() === 0) {
                setTimeout(this.adjustContentFrame, 100);
                return;
            }

            dirmodelY = $fixedContainer.offset().top - $(window).scrollTop() +
                    $fixedContainer.outerHeight();

            // add placeholder because fixed container has no height block
            if (!$placeholder || !$placeholder.hasClass("fixed-container-placeholder")) {
                $placeholder = $("<div class='fixed-container-placeholder'></div>");
                $fixedContainer.after($placeholder);
            }
            $placeholder.css({
                height: $fixedContainer.outerHeight()
            });
            $placeholder.next().css({
                minHeight: height - dirmodelY,
                display:"block"
            });
        },
        loadDevice: function(device, path, callback) {
            if (this._device) {
                this.stopListening(device);
            }
            this._device = device;
            this.listenTo(device, "change", this.onDeviceStateChanged);

            // need more implementation on child
        },
        getDevice: function() {
            return this._device;
        },
        getUploadDir: function() {
            throw "Not implemented";
        },
        refreshThumbnail: function($icon, path) {
            var $img;
            $img = $("<img style='display:none' src='" + path + "?v=" + (new Date()).getTime()+ "'>");
            $icon.html($img);
            $img.fadeIn(500);
        },
        unbindVisibilityTracker: function() {
            if (this._visibilityTracker) {
                $(window).off("resize scroll", this._visibilityTracker);
                delete this._visibilityTracker;
            }
        },
        bindVisibilityTracker: function(options) {
            // options should be in form
            // { candidateSetEls: [..], visibleEls: [..], selectableEls:[..]}
            var that = this,
                lastVisibleCheckX = -1,
                lastVisibleCheckY = -1,
                lastVisibleCheckIdx = 0,
                VISIBLE_CHECK_INTERVAL = 60, // check every 60px scroll
                candidateSetEls,
                visibleSetEls,
                visibleEls,
                selectableEls;

            this.unbindVisibilityTracker();

            candidateSetEls = options.candidateSetEls;
            if (!candidateSetEls.length) {
                return;
            }
            visibleSetEls = [];
            visibleEls = options.visibleEls || [];
            visibleEls.length = 0;
            selectableEls = options.selectableEls || [];
            selectableEls.length = 0;

            this._visibilityTracker = function() {
                var hDiff,
                    wDiff,
                    isScrollDown,
                    $el, y, height,
                    length,
                    idx,
                    tmp,
                    visibleYFrom = $(window).scrollTop(),
                    visibleYTo = visibleYFrom + $(window).height(),
                    scrollHeight = $("#page").height();

                if (visibleYTo + VISIBLE_CHECK_INTERVAL > scrollHeight) {
                    visibleYTo = scrollHeight;
                }
                hDiff = Math.abs(lastVisibleCheckY - visibleYFrom);
                wDiff = Math.abs(lastVisibleCheckX - $(window).width());
                isScrollDown = lastVisibleCheckY <= visibleYFrom;
                if ((lastVisibleCheckY >= 0 && hDiff < VISIBLE_CHECK_INTERVAL) &&
                    (lastVisibleCheckX >= 0 && wDiff < VISIBLE_CHECK_INTERVAL )) {
                    return;
                }
                lastVisibleCheckY = visibleYFrom;
                lastVisibleCheckX = $(window).width();

                // remove visible class from previous visible items
                length = visibleSetEls.length;
                tmp = [];
                for (idx = 0; idx < length; idx++) {
                    $el = visibleSetEls[isScrollDown ? idx : length - idx - 1];
                    // check $el exit in dom
                    if (!$.contains(document, $el[0])) {
                        tmp.push($el);
                        continue;
                    }
                    height = $el.height();
                    y = $el.offset().top;

                    if (y + height > visibleYFrom && y <= visibleYTo) {
                        break;
                    } else {
                        tmp.push($el);
                        $el.removeClass("visible");
                    }
                }
                $.each(tmp, function(idx, $el) {
                    var i = visibleSetEls.indexOf($el);
                    if (i > -1) {
                        visibleSetEls.splice(i, 1);
                    }
                });



                // Find visible element
                length = candidateSetEls.length;
                for (idx = lastVisibleCheckIdx; idx < length && idx >= 0;
                        isScrollDown ? idx++ : idx--) {
                    $set = candidateSetEls[idx];
                    // check $set exit in dom
                    if (!$.contains(document, $set[0])) {
                        continue;
                    }
                    height = $set.height();
                    y = $set.offset().top;

                    if (y + height > visibleYFrom && y <= visibleYTo) {
                        lastVisibleCheckIdx = idx;
                        if (!$set.hasClass("visible")) {
                            $set.addClass("visible");
                            if (isScrollDown) {
                                visibleSetEls.push($set);
                            } else {
                                visibleSetEls.unshift($set);
                            }
                        }
                    } else if ((isScrollDown && y <= visibleYFrom) ||
                        (!isScrollDown && y > visibleYTo)) {
                        //$el.trigger("visible");
                    }
                }

                if (!that._selectStarted) {
                    $.each(selectableEls, function(idx, $el) {
                        $el.removeClass("selectable");
                    });
                    selectableEls.length = 0;
                }
                visibleEls.length = 0;

                $.each(visibleSetEls, function(idx, $set) {
                    $(that.itemSelector, $set).each(function(idx, el) {
                        var $el = $(el);
                        visibleEls.push($el);
                        $el.trigger("visible");
                        if (!$el.hasClass("selectable")) {
                            $el.addClass("selectable");
                            selectableEls.push($el);
                        }
                    });
                });

            };
            $(window).on("resize scroll", this._visibilityTracker)
            setTimeout(this._visibilityTracker, 0);
        }
    });

    DeviceView = SyncportView.extend({
        template: "#tmpl-selected-device",

        deviceDropdownTmpl: "#tmpl-dropdown-device",
        deviceDropdownTarget: "header .device-select-menu",

        initialize: function(options) {
            var defaultDevice,
                nonWebDevices,
                that = this,
                deviceId;

            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "renderSelectedDevice", "getSelectedDevice");

            this.collection = $sp.DeviceManager.getDeviceCollection();
            if (this.collection) {
                this.listenTo(this.collection, "change", this.onDevicesChanged);
                this.listenTo(this.collection, "add", this.onDeviceAdded);
            }

            if (options.deviceId) {
                deviceId = $sp.Utils.decodeDeviceId(options.deviceId);
                defaultDevice = this.collection.where({
                    deviceId: deviceId});
                if (defaultDevice.length) {
                    defaultDevice = defaultDevice[0];
                    this.loadDevice(defaultDevice, options.path);
                }
            }
            if (!this._selectedDevice) {
                nonWebDevices = this.collection.filter(function(device) {
                    return device.get("deviceType") !== "WEB";
                });
                if (nonWebDevices.length > 0) {
                    defaultDevice = nonWebDevices[0];
                    this.loadDevice(defaultDevice);
                } else {
                    $sp.router.navigate(
                        "/device", { trigger: false , replace: true});
                    this.parentView.subViews.emptyView.show();
                    return;
                }
            }
            $("#dropdown")
                .off("click", "#dropdown-device .device")
                .on("click", "#dropdown-device .device", function() {
                        that.onDeviceClicked.apply(that, arguments);
                    })
                .off("click", "#dropdown-device .link-download")
                .on("click", "#dropdown-device .link-download", function() {
                        $sp.router.navigate('download', { trigger: true });
                        that.hideDeviceDropdown();
                    });
            this.renderSelectedDevice();
        },
        onDeviceAdded: function(model) {
            if (!this._selectedDevice) {
                this.loadDevice(model);
                this.renderSelectedDevice();
            }
            if ($("#dropdown-device").size()) {
                this.hideDeviceDropdown();
                this.showDeviceDropdown();
            }
        },
        onDevicesChanged: function() {
            if ($("#dropdown-device").size()) {
                this.hideDeviceDropdown();
                this.showDeviceDropdown();
            }
        },
        events: function() {
            var events = SyncportView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend({}. events, {
                "click .selected-device": "showDeviceDropdown"
            });
        },
        loadDevice: function(device, path) {
            var that = this,
                parentView = this.parentView,
                menuUri,
                deviceId = device.get("deviceId"),
                encodedDeviceId = $sp.Utils.encodeDeviceId(deviceId);

            // update url
            menuUri = "/device/" + encodedDeviceId + "/" + parentView.menuType;
            if (path) {
                if (path.startsWith("/")) {
                    menuUri += path;
                } else {
                    menuUri += "/" + path;
                }
            }
            $sp.router.navigate(menuUri, { trigger: false, replace: true });
            this._selectedDevice = device;
            this.listenTo(device, "change", this.onDeviceStateChanged);
            parentView.startLoadDevice(device, path);
        },

        onDeviceClicked: function(e) {
            var $target = $(e.currentTarget),
                device = this.collection.get($target.data("cid"));
            this.hideDeviceDropdown();
            this.loadDevice(device);
            this.renderSelectedDevice();
        },

        onDeviceStateChanged: function(device) {
            this.renderSelectedDevice();
        },

        renderSelectedDevice: function() {
            var context,
                currModel;

            if (!this._selectedDevice) {
                this.$el.hide();
                return;
            }

            currModel = this._selectedDevice.toJSON();
            currModel.cid = this._selectedDevice.cid;

            context = $.extend({}, this._context, {
                currModel: currModel
            });

            this.$el.html(this._template(context));
            this.$el.show();

            if (this.parentView.subViews.emptyView.isVisible()) {
                this.parentView.subViews.emptyView.hide();
            }
        },

        showDeviceDropdown: function() {
            var that = this,
                devices,
                models,
                activeModels,
                deactiveModels,
                $target,
                context,
                tmpl;

            devices = this.collection.filter(function(device) {
                return device.get("deviceType") !== "WEB";
            });
            if (devices.length === 0) {
                return;
            }

            models = $.map(devices, function(device) {
                var json = device.toJSON();
                json.cid = device.cid;
                if (that._selectedDevice.cid === device.cid) {
                    json.selected = true;
                }
                return json;
            });

            activeModels = $.map(models, function(model) {
                if (model.isTurnOn) {
                    return model;
                }
            });

            deactiveModels = $.map(models, function(model) {
                if (!model.isTurnOn) {
                    return model;
                }

            });

            if (!this._deviceDropdownTmpl) {
                tmpl = $(this.deviceDropdownTmpl).html();
                this._deviceDropdownTmpl = _.template(tmpl);
            }

            context = $.extend({}, this._context, {
                activeModels: activeModels,
                deactiveModels: deactiveModels
            });

            $target = $(this.deviceDropdownTarget);
            $("#dropdown").html(this._deviceDropdownTmpl(context));
            $("#dropdown-device").css({
                top: $target.offset().top + $target.height() -
                        $(window).scrollTop(),
                left: $target.offset().left
            });
            $("#filter").show().one("click", function() {
                that.hideDeviceDropdown();
            });
        },

        hideDeviceDropdown: function() {
            $("#dropdown-device").remove();
            $("#filter").click();
            $("#filter").hide();
        },

        remove: function() {
            SyncportView.prototype.remove.call(this);
            return this;
        },

        getSelectedDevice: function() {
            return this._selectedDevice;
        }
    });

    DeviceConnectionCheckView = SyncportView.extend({
        template: "#tmpl-modal-connection-check",
        initialize: function(options) {
            var that = this;

            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "show", "hide");
            this._headSendListener = {
                onStart: function(deviceId) {
                    that.show.call(that, deviceId);
                },
                onFinished: function(deviceId) {
                    that.hide.call(that, deviceId);
                }
            };
            $sp.FileTransfer
                .addHeadSendListener(this._headSendListener);
        },
        remove: function() {
            $sp.FileTransfer
                .removeHeadSendListener(this._headSendListener);
            SyncportView.prototype.remove.call(this);
            $("#modal-connection-check").remove();
            return this;
        },
        show: function(deviceId) {
            var that = this;
            this.render(deviceId);
            $("#modal-connection-check").modal({ show: true, backdrop:"static"})
                .on("hidden.bs.modal", function() {
                    $(this).remove();
                });
            setTimeout(function() {
                if (that._hidable) {
                    that.hide();
                } else {
                    that._hidable = true;
                }
            }, 1000);
        },
        hide: function() {
            if (this._hidable) {
                $("#modal-connection-check").modal("hide");
            } else {
                this._hidable = true;
            }
        },
        render: function(deviceId) {
            var context,
                html;
            context = $.extend({}, this._context, {
                device: $sp.DeviceManager.getCachedDeviceWithId(deviceId)
            });
            if (!this._template) {
                this._template = _.template($(template).html());
            }
            html = this._template(context);
            $("#modal").append(html);
        }
    });

    DeviceLoadFailureView = SyncportView.extend({
        template: "#tmpl-dir-loadfailure",
        initialize: function(options) {
            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "show", "hide");
        },
        show: function(device, path) {
            this.render(device);
            this.$el.show();

            this._onCollectionChanged = $.proxy(function() {
                this.parentView.startLoadDevice(device, path);
            }, this);
            $($sp.Polling).on("colChanged dirChanged", this._onCollectionChanged);
        },
        hide: function() {
            this.$el.hide();
            if (this._onCollectionChanged) {
                $($sp.Polling).off("colChanged dirChanged", this._onCollectionChanged);
            }
        },
        render: function(device) {
            var html,
                path,
                encodedDeviceId = $sp.Utils.encodeDeviceId(device.get("deviceId"));
            path = "device/" + encodedDeviceId;
            html = this._template(
                $.extend({}, this._context, { model: device }));
            this.$el.html(html);
            $sp.router.navigate(path, { trigger: false });
        }
    });

    DeviceDropzoneView = SyncportView.extend({
        dropzoneTmpl: "#tmpl-dropzone",
        dropzoneOffTmpl: "#tmpl-dropzone-off",
        initialize: function(options) {
            var that = this;

            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "initDropzone", "remove")
            if (options.sendFile) {
                this._sendFile = $.proxy(options.sendFile, that.parentView);
            }

            this._dropzoneTmpl = _.template($(this.dropzoneTmpl).html());
            this._dropzoneOffTmpl = _.template($(this.dropzoneOffTmpl).html());
            this._initialized = false;
            this._confirmDataUsage = false;
            this._sendableFiles = [];
        },
        remove: function() {
            var $dropzone = $("body");
            $dropzone.off({
                dragover: this._dragover,
                dragleave: this._dragleave,
                dragenter: this._dragenter,
                drop: this._drop,
                dndHoverStart: this._dndHoverStart,
                dndHoverEnd: this._dndHoverEnd
            });
            SyncportView.prototype.remove.call(this);
            this._initialized = false;
        },
        initDropzone: function() {
            var that = this,
                _dragging = 0,
                $dropzone = $("body");

            if (this._initialized) {
                return;
            }
            this._initialized = true;
            this._dragover = function(e) {
                $(this).addClass("dragenter");
                e.stopPropagation();
                e.preventDefault();
                return false;
            };
            this._dragleave = function(e) {
                _dragging--;
                if (_dragging === 0) {
                    $(this).removeClass("dragenter");
                    that.hide();
                }
                e.stopPropagation();
                e.preventDefault();
                return false;
            };
            this._dragenter = function(e) {
                _dragging++;
                if (!$(this).hasClass("dragenter")) {
                    $(this).addClass("dragenter");
                    that.show();
                }
                e.stopPropagation();
                e.preventDefault();
                return false;
            };
            this._drop = function(e) {
                var dirModel = that.parentView.getUploadDir(),
                    doUpload,
                    device;
                e.stopPropagation();
                e.preventDefault();
                $(this).removeClass("dragenter");
                that.hide();

                if ($("#modal-upload").size()) {
                    $("#modal-upload").modal("hide");
                }

                device = dirModel.getDevice();
                if (device.get("isTurnOn")) {
                    if (device.get("connectionState") === "charged") {
                        that._confirmDataUsage = false;
                        $sp.confirm({
                                title: $sp.string(
                                    "warning_data_charge__device_charged_connected__title"),
                                text: $sp.string(
                                    "warning_data_charge__device_charged_connected__text"),
                                confirmText: $sp.string("proceed_send"),
                                closeText: $sp.string("cancel")
                            }, function(confirmed) {
                                if (confirmed) {
                                    that._confirmDataUsage = true;
                                    that.flushSendableFiles();
                                } else {
                                    that.clearSendableFiles();
                                }
                            });
                    } else {
                        that._confirmDataUsage = true;
                    }
                    that.onFileDragDrop.call(that, e);
                }
                return false;
            };
            this._dndHoverStart = function(e) {
                if (!$(this).hasClass("dragenter")) {
                    $(this).addClass("dragenter");
                    that.show();
                }
                e.stopPropagation();
                e.preventDefault();
                return false;
            };
            this._dndHoverEnd = function(e) {
                $dropzone.removeClass("dragenter");
                that.hide();
                e.stopPropagation();
                e.preventDefault();
                return false;
            }
            $dropzone.dndhover().on({
                dragover: this._dragover,
                dragleave: this._dragleave,
                dragenter: this._dragenter,
                drop: this._drop,
                dndHoverStart: this._dndHoverStart,
                dndHoverEnd: this._dndHoverEnd
            });
        },
        sendFile: function(file) {
            if (this._confirmDataUsage) {
                this._sendFile && this._sendFile(file);
                return;
            }
            this._sendableFiles.push(file);
        },
        flushSendableFiles: function() {
            var that = this;
            $.each(this._sendableFiles, function(idx, file) {
                that._sendFile && that._sendFile(file);
            });
            this.clearSendableFiles();
        },
        clearSendableFiles: function() {
            this._sendableFiles.length = 0;
        },
        show: function() {
            var context,
                $indicator = $("#dropzone-indicator"),
                html,
                dirModel = this.parentView.getUploadDir();

            context = { context: this._context, model: dirModel};
            $("body").addClass("droppable");

            if (dirModel.getDevice().get("isTurnOn")) {
                $("body").removeClass("droppable-off");
                html = this._dropzoneTmpl(context);
            } else {
                $("body").addClass("droppable-off");
                html = this._dropzoneOffTmpl(context);
            }
            $indicator.html(html);
            $indicator.fadeIn();

            if ($("#modal-upload").size()) {
                $("#modal-upload .droppable-off").fadeOut(200, function() {
                    $("#modal-upload .droppable").fadeIn(200);
                });
            }
        },
        hide: function() {
            $("body").removeClass("droppable");
            $("#dropzone-indicator").hide();

            if ($("#modal-upload").size()) {
                $("#modal-upload .droppable").fadeOut(200, function() {
                    $("#modal-upload .droppable-off").fadeIn(200);
                });
            }
        },
        onFileDragDrop: function(e) {
            var items,
                item,
                i,
                entry,
                that = this,
                dataTransfer = e.originalEvent.dataTransfer,
                dirTester,
                errCnt;

            if(dataTransfer) {
                if (dataTransfer.items){
                    items = dataTransfer.items;

                    for(i = 0; i < items.length; i++){
                        item = items[i]
                        if(item.getAsEntry){
                            //Standard HTML5 API
                            entry = item.getAsEntry();
                        }else if(item.webkitGetAsEntry){
                            //WebKit implementation of HTML5 API.
                            entry = item.webkitGetAsEntry();
                        }
                        if(entry.isFile){
                            //Handle FileEntry
                            this.sendFile(item.getAsFile());
                        }else if(entry.isDirectory){
                            //Handle DirectoryEntry
                            readFileTree(entry, this.sendFile);
                        }
                    }
                } else if (dataTransfer.files) {
                    // IE 10, 11 drag and drop containing directory always has
                    // filelist with length 0.
                    if (!dataTransfer.files.length) {
                        $sp.alert(
                            $sp.string(
                                "failed_to_send_directory%s",
                                $sp.context.browser.currentBrowser[0]
                            ));
                        return;
                    }
                    // Check whether directory is dropped or not in IE, Firefox, Safari
                    // Transfer should not be started when dropped item contains directory.
                    errCnt = 0;
                    for(i = 0; i< dataTransfer.files.length; i++) {
                        dirTester = new FileReader();
                        dirTester.file = dataTransfer.files[i];
                        dirTester.onerror = function(e) {
                            if (errCnt === 0) {
                                $sp.alert(
                                    $sp.string(
                                        "failed_to_send_directory%s",
                                        $sp.context.browser.currentBrowser[0]));
                            }
                            errCnt += 1;
                        }
                        dirTester.onload = function(b) {
                            that.sendFile(this.file);
                        }
                        dirTester.readAsText(dataTransfer.files[i].slice(0, 1));
                        dirTester.idx = i;
                    }
                }
            }

            function displayFileReadError(e, entry) {
                var filename = entry ? entry.name || "files" : "files";
                $sp.alert($sp.string("failed_to_send_unicode_files", filename));
                console.error(e);
            }

            function readFile(entry, callback) {
                entry.file(function(callback, path, file){
                    callback && callback.apply(this, [file, path]);
                }.bind(that, callback, entry.fullPath),
                function(e) {
                    displayFileReadError(e, entry);
                });
            }

            function readFileTree(entry, callback){
                if(entry.isFile){
                    readFile(entry, callback);
                }else if(entry.isDirectory){
                    var dirReader = entry.createReader();
                    dirReader.readEntries(function(entries){
                        var idx = entries.length;
                        while(idx--){
                            readFileTree(entries[idx], callback);
                        }
                    }, function(e){
                        displayFileReadError(e, entry);
                    });
                }
            }
        }
    });

    DeviceSettingsContentView = SyncportView.extend({
        template: "#tmpl-device-settings",
        initialize: function(options) {
            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "loadDevice");
        },
        events: function() {
            var events = SyncportView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend({}, events, {
                "click .btn-discard-changes": "render",
                "click .btn-save-changes": "saveChanges",
                "click .btn-delete-device": "deleteDevice",
                "submit #device-settings-form": "saveChanges"
            });
        },
        loadDevice: function(device, path, callback) {
            this._device = device;
            this.render();
            callback && callback(true);
        },
        deleteDevice: function(e) {
            var $target = $(".btn-delete-device", this.$el),
                that = this;

            if ($target.attr("disabled")) {
                return;
            }
            $sp.confirm({
                    title: $sp.string(
                        "are_you_sure"),
                    text: $sp.string(
                        "are_you_sure_delete_device%s", this._device.get("deviceName")),
                    confirmText: $sp.string("proceed_delete"),
                    closeText: $sp.string("cancel")
                }, function(confirmed) {
                    if (confirmed) {
                        doDelete();
                    }
                });

            function doDelete() {
                $target.attr("disabled", true);
                $target.addClass("progress");
                $target.find(".text").html($sp.string("deleting_device"));
                $.ajax({
                    url: $sp.Remote.DEVICE_SELF,
                    type: "DELETE"
                }).done(function(res) {
                    $sp.router.navigate("/", { trigger: true });
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    $sp.alert(jqXHR.responseJSON.error || errorThrown);
                }).always(function() {
                    $target.removeAttr("disabled").removeClass("progress");
                    $target.find(".text").html($sp.string("delete_device"));
                });
            }
        },
        saveChanges: function(e) {
            var that = this,
                $target = $(".btn-save-changes", this.$el),
                $deviceName = $(".input-device-name", this.$el),
                notifyOptions = {
                        showDuration: 0,
                        hideDuration: 0,
                        elementPosition: "right middle",
                        autoHide: false
                    },
                valid = true;

            if ($target.attr("disabled")) {
                return;
            }
            if ($deviceName.val() === "") {
                valid = false;
                $deviceName.notify({
                    msg: $sp.string("required_field")
                }, notifyOptions, "error");
            }
            if (!valid) {
                return;
            }
            $target.addClass("progress");
            $deviceName.attr("disabled", true);
            $target.find(".text").html($sp.string("saving_changes"));
            $.ajax({
                url:$sp.Remote.api.DEVICE_ALL,
                data: JSON.stringify({
                    "changes": [{
                        "device_id": this._device.get("deviceId"),
                        "name": $deviceName.val()
                    }]
                }),
                type:"POST",
                dataType: "json",
                contentType: "application/json",
            }).done(function(res) {
                $sp.alert(
                    "<i class='glyphicons glyphicons-ok-2'></i>" + $sp.string("applied"),
                    function() {
                        that._device.set("deviceName", $deviceName.val());
                        that.render();
                    });
            }).fail(function(jqXHR, textStatus, errorThrown) {
                $sp.alert(jqXHR.responseJSON.error || errorThrown);
            }).always(function() {
                $target.removeAttr("disabled").removeClass("progress");
                $deviceName.removeAttr("disabled");
                $target.find(".text").html($sp.string("save_changes"));
            });
        },
        render: function() {
            var context = $.extend({}, this._context, {
                device: this._device
            });
            this.$el.html(this._template(context));
        }
    });

    DeviceEventsContentView = SyncportView.extend({
        template: "#tmpl-device-events",
        initialize: function(options) {
            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "loadDevice");
        },
        loadDevice: function(device, path, callback) {
            this._device = device;
            this.render();
            callback && callback(true);
        },
        render: function() {
            var context = $.extend({}, this._context, {
                device: this._device,
                eventModels: $sp.DeviceEventManager.getEvents(this._device.get("deviceId"))
            });
            this.$el.html(this._template(context));
        }
    });

    BaseCollectionContentView = BaseDeviceContentView.extend({
        template: undefined,
        itemSelector: ".item",
        itemThumbSelector: ".icon",
        bgContextMenuRegionSelector: undefined,
        uploadModalTmpl: "#tmpl-modal-upload",
        selectedItemMenusTmpl: "#tmpl-selected-item-menus",
        useItemSelectorUI: true,
        compressFilter: undefined,
        initialize: function(options) {
            var that = this,
                ListenableArray;

            this._visibleItemEls = [];
            this._selectableItemEls = [];

            if (this.useItemSelectorUI) {
                ListenableArray = new $sp.Utils.ListenableArray.extend({
                    push: function() {
                        var args = Array.prototype.slice.call(arguments),
                            item = args[0],
                            array = this;
                        if (!this._onItemRemoveHandler) {
                            this._onItemRemoveHandler = function() {
                                var idx = array.indexOf(this),
                                    $el;
                                if (idx > -1) {
                                    array.splice(idx, 1);
                                }
                                if (($el = that.getItemEl(this))) {
                                    $el.removeClass("selected");
                                }
                            };
                        }
                        item.on("remove", this._onItemRemoveHandler);
                        $sp.Utils.ListenableArray.prototype.push.apply(this, arguments);
                    },
                    splice: function() {
                        var array = this,
                            items = $sp.Utils.ListenableArray.prototype.splice
                                        .apply(this, arguments);
                        $.each(items, function(idx, item) {
                            if (array._onItemRemoveHandler) {
                                item.off("remove", array._onItemRemoveHandler);
                            }
                        });
                    },
                    clear: function() {
                        var array = this;
                        $.each(this, function(idx, item) {
                            if (array._onItemRemoveHandler) {
                                item.off("remove", array._onItemRemoveHandler);
                            }
                        });
                        return $sp.Utils.ListenableArray.prototype.clear
                            .apply(this, arguments)
                    }
                });

                this._selectedItems = new ListenableArray();

                this.listenTo(this._selectedItems, "push splice clear", function() {
                    that.onItemSelectionChanged.call(that, that._selectedItems);
                });

                // bind drag move event
                this.initSelector();
            }

            BaseDeviceContentView.prototype.initialize.call(this, options);
            _.bindAll(this, "onThumbnailUploaded", "contextmenuListener", "openItem",
                "downloadItem", "onSelectedItemMenuClicked", "onCollectionChanged");

            $($sp.Polling).on("thumbnailUploaded", this.onThumbnailUploaded);
            $($sp.Polling).on("colChanged dirChanged", this.onCollectionChanged);
        },
        events: function() {
            var events = BaseDeviceContentView.prototype.events,
                extendEvents = {},
                thumbSelector,
                openlinkSelector;

            if (_.isFunction(events)) {
                events = events.call(this);
            }

            thumbSelector = this.itemSelector + " " + this.itemThumbSelector;
            openlinkSelector = this.itemSelector + " .open-link";
            extendEvents["click " + [thumbSelector, openlinkSelector].join(",")] = "openItem";
            if (this.useItemSelectorUI) {
                extendEvents["click " + this.itemSelector] = "selectItem";
            }
            return $.extend({}, events, extendEvents, {
                        "click .menu": "onSelectedItemMenuClicked"
                    });
        },
        onSelectedItemMenuClicked: function(e) {
            var that = this,
                $target = $(e.currentTarget),
                action = $target.data("action"),
                $itemEl,
                $tmpItems,
                paths,
                doDownload,
                device;

            if (action === "download") {
                $tmpItems = $(this.itemSelector + ".temp-selected");
                if ($tmpItems.size() > 0) {
                    $itemEl = $tmpItems;
                } else if (this.useItemSelectorUI) {
                    $itemEl = this.getItemEl(this._selectedItems[0]);
                    this._selectedItems.clear();
                    $(this.itemSelector + ".selected", this.$el).removeClass("selected");
                } else {
                    return;
                }
                if ($itemEl) {
                    this.downloadItem($itemEl);
                }
            } else if (action === "open") {
                $tmpItems = $(this.itemSelector + ".temp-selected");
                if ($tmpItems.size() > 0) {
                    $itemEl = $tmpItems;
                } else if (this.useItemSelectorUI) {
                    $itemEl = this.getItemEl(this._selectedItems[0]);
                    this._selectedItems.clear();
                    $(this.itemSelector + ".selected", this.$el).removeClass("selected");
                } else {
                    return;
                }
                if ($itemEl) {
                    this.openItem($itemEl);
                }
            } else if (action === "send_file") {
                this.showUploader();
            } else if (action === "create_folder") {
                this.showCreateFolderModal();
            } else if (action === "delete") {
                paths = []
                $tmpItems = $(this.itemSelector + ".temp-selected");
                if ($tmpItems.size() > 0) {
                    $tmpItems.each(function(idx, el) {
                        var item = that.getItemFromEl($(el));
                        if (item) {
                            paths.push(item.path);
                        }
                    });
                } else if (this.useItemSelectorUI) {
                    $.each(this._selectedItems, function(idx, item) {
                        paths.push(item.path);
                    });
                } else {
                    return;
                }
                $sp.confirm({
                        title: $sp.string("are_you_sure"),
                        text: "<span>" +
                            $sp.string("are_you_sure_delete%s", paths.length) +
                            "</span><br/><strong>" +
                            $sp.string("warning_file_delete") +
                            "</strong>",
                        confirmText: $sp.string("proceed_delete"),
                        closeText: $sp.string("cancel")
                    },
                    function(confirmed) {
                        if (confirmed) {
                            if (that.useItemSelectorUI) {
                                that._selectedItems.clear();
                                $(that.itemSelector + ".selected", that.$el)
                                    .removeClass("selected");
                            }
                            that.sendRemoteAddDel(
                                that.getCollection(), [], paths);
                        }
                    });
                return;
            } else if (action === "download-multiple") {
                paths = [];
                $tmpItems = $(this.itemSelector + ".temp-selected");
                if ($tmpItems.size() > 0) {
                    $tmpItems.each(function(idx, el) {
                        var item = that.getItemFromEl($(el));
                        if (item) {
                            paths.push(item.path);
                        }
                    });
                } else if (this.useItemSelectorUI) {
                    $.each(this._selectedItems, function(idx, item) {
                        paths.push(item.path);
                    });
                    this._selectedItems.clear();
                    $(this.itemSelector + ".selected", this.$el).removeClass("selected");
                } else {
                    return;
                }
                device = this.getDevice();
                doDownload = function() {
                    $sp.view.compressingProgressView.addJob({
                        device: device,
                        paths: paths,
                        filter: that.compressFilter
                    });
                };
                if (device.get("connectionState") === "charged") {
                    $sp.confirm({
                            title: $sp.string(
                                "warning_data_charge__device_charged_connected__title"),
                            text: $sp.string(
                                "warning_data_charge__device_charged_connected__text"),
                            confirmText: $sp.string("proceed_download"),
                            closeText: $sp.string("cancel")
                        }, function(confirmed) {
                            if (confirmed) {
                                doDownload();
                            }
                        });
                } else {
                    doDownload();
                }
            } else {
                return;
            }
        },
        onDeviceStateChanged: function(device) {
            BaseDeviceContentView.prototype.onDeviceStateChanged
                .apply(this, arguments);
            if (this._collection && this._collection.isLoaded) {
                this.render();
            }
        },
        openItem: function(e) {
            throw "Not implemented";
        },
        downloadItem: function(e) {
            throw "Not implemented";
        },
        getCollection: function() {
            return this._collection;
        },
        loadDevice: function(device, path, callback) {
            var that = this,
                selectors = [
                        this.$el.selector + " " + this.itemSelector,
                        "#filter"
                    ];

            if (this.bgContextMenuSelector) {
                selectors.push(
                    this.$el.selector + " " + this.bgContextMenuSelector);
            }
            if (this.useItemSelectorUI) {
                this._selectedItems.clear();
                $(this.itemSelector + ".selected", this.$el)
                    .removeClass("selected");
            }

            BaseDeviceContentView.prototype.loadDevice.call(
                this, device, path, callback);

            if (this._collection) {
                this.stopListening(this._collection);
            }

            this.createCollection(device, path, $.proxy(function(collection) {
                this._collection = collection;
                this._collection.load({
                    success: function() {
                        var uploadPath,
                            name,
                            idx,
                            subPaths;
                        if (this.device !== device) {
                            return;
                        }

                        uploadPath = that.getUploadDirPath(this.getSyncRootPath());
                        if (!uploadPath.endsWith("/")) {
                            uploadPath += "/";
                        }
                        subPaths = uploadPath.split("/");
                        name = subPaths[subPaths.length -2];
                        that._uploadDir = new $sp.Model.VirtualDirectory(
                                                this.device,
                                                name,
                                                uploadPath);
                        $(document).on("contextmenu", selectors.join(","),
                            that.contextmenuListener);
                        that.parentView.subViews.dropzoneView.initDropzone();

                        that.render(true);

                        callback && callback(true);
                        this.isInitialized = true;
                    },
                    failure: function(msg) {
                        callback && callback(false, msg);
                    }
                });
            }, this));
        },
        selectItem: function(e, isSelectionMode) {
            var that = this,
                $target = $(e.currentTarget),
                item = this.getItemFromEl($target),
                idx,
                isSelectionMode = false || isSelectionMode,
                isShiftSelectionMode = false,
                targetIdx,
                firstSelectedItemIdx,
                startIdx,
                endIdx,
                $selectedItems,
                selector;

            if (!item || $target.hasClass("nonselectable")) {
                return;
            }
            if ($sp.context.os === "MacOS") {
                isSelectionMode |= e.metaKey;
            } else {
                isSelectionMode |= e.ctrlKey;
            }
            if (isSelectionMode) {
                if ($target.hasClass("selected")) {
                    $target.removeClass("selected");
                    idx = this._selectedItems.indexOf(item);
                    if (idx > -1) {
                        this._selectedItems.splice(idx, 1);
                    }
                } else {
                    $target.addClass("selected");
                    idx = this._selectedItems.indexOf(item);
                    if (idx === -1) {
                        this._selectedItems.push(item);
                    }
                }
            } else if (e.shiftKey) {
                targetIdx = $target.index(this.itemSelector);
                firstSelectedItemIdx = $(this.itemSelector + ".selected").first()
                    .index(this.itemSelector);
                startIdx = Math.min(targetIdx, firstSelectedItemIdx);
                endIdx = Math.max(targetIdx, firstSelectedItemIdx);

                this._selectedItems.clear();
                $(this.itemSelector + ".selected", this.$el)
                    .removeClass("selected");

                selector = this.itemSelector;
                if (startIdx > 0) {
                    selector += ":gt(" + (startIdx -1) + ")";
                }
                selector += ":lt(" + (endIdx - startIdx + 1) + ")";

                $selectedItems = $(selector);
                $selectedItems.addClass("selected");
                $selectedItems.each(function(idx, el) {
                    var item = that.getItemFromEl($(el));
                    if (item) {
                        that._selectedItems.push(item);
                    }
                });
            } else {
                this._selectedItems.clear();
                $(this.itemSelector + ".selected", this.$el).removeClass("selected");

                $target.addClass("selected");
                this._selectedItems.push(item);
            }
        },
        onItemSelectionChanged: function(selectedItems) {
            throw "Not implemented";
        },
        getUploadDir: function() {
            return this._uploadDir;
        },
        remove: function() {
            $(document).off("contextmenu", this.contextmenuListener);
            this.$el.off("mouseenter");
            $($sp.Polling).off("thumbnailUploaded", this.onThumbnailUploaded);
            $($sp.Polling).off("dirChanged colChanged", this.onCollectionChanged);
            if (this.getCollection()) {
                this.getCollection().remove();
            }
            BaseDeviceContentView.prototype.remove.call(this);
        },
        onCollectionChanged: function(e, ownerId) {
            var that = this,
                device = this.getDevice();
            if (device && device.get("deviceId") === ownerId &&
                    this._collection.isLoaded) {

                this._collection.load({
                    success: function(isChanged) {
                        if (isChanged) {
                            that.render();
                        }
                    },
                    failure: function(msg) {
                        $sp.notify(msg, "error");
                    }
                });
            }
        },
        render: function(forceRefresh) {
            if (!this._render) {
                this._render = $.proxy(function() {
                    var that = this,
                        context,
                        callback,
                        collection = this.getCollection();

                    if (!collection) {
                        return;
                    }
                    if (this._tutorialStarted) {
                        return;
                    }

                    context = $.extend({}, this._context, {
                        collection: this.getCollection(),
                        model: this.getUploadDir(),
                        deviceName: this.getUploadDir().getDevice().get("deviceName"),
                        selectedItems: this._selectedItems || [],
                        tempSelectedItem: this.getItemFromEl(
                            $(this.itemSelector + ".temp-selected")) || null
                    });

                    this.$el.html(this._template(context));
                    this.onItemSelectionChanged(this._selectedItems || []);
                    setTimeout(this.adjustContentFrame, 0);

                    this._candidateItemSetEls =
                            $.map($(".item-set", this.$el), function(el) { return $(el); });
                    this._visibleItemEls = [];
                    this._selectableItemEls = [];
                    this.bindVisibilityTracker({
                        candidateSetEls: this._candidateItemSetEls,
                        visibleEls: this._visibleItemEls,
                        selectableEls: this._selectableItemEls
                    });

                    if (!this._tutorialStarted &&
                            !$.cookie("didTutorial.general")) {
                        this._tutorialStarted = true;
                        hopscotch.startTour($sp.Tour.getDeviceTour());
                        callback = $.proxy(function() {
                            $.cookie("didTutorial.general", true);
                            this._tutorialStarted = false;
                            this.render();
                        }, this);
                        hopscotch.listen("end", callback);
                        hopscotch.listen("close", callback);
                    }
                }, this);
                this._throttledRender = _.throttle(this._render, 3000);
            }
            if (forceRefresh) {
                this._render();
            } else {
                this._throttledRender();
            }
        },
        getItemKey: function(item) {
            // need implement
            return item.path;
        },
        getItemEl:function(item) {
            var key, $el;
            key = this.getItemKey(item);
            $el = $(this.itemSelector + "[data-key='" + escape(key) + "']", this.$el);
            return $el.length > 0 ? $el : null;
        },
        getItemFromEl: function($el) {
            var that = this,
                key;

            if ($el.size() === 0) {
                return;
            }
            key = unescape($el.data("key"))
            return this.getCollection().items[key];
        },
        initSelector: function() {
            var that = this,
                $pageEl = $(".page-device"),
                dirSelectorOnMousemove,
                lastSelectApplyX = -1,
                lastSelectApplyY = -1,
                SELECTION_APPLY_MIN_DIFF = 20,
                $target;

            $pageEl.off("mousedown").on("mousedown", function(e) {
                var $selector = $("#directory-selector", $pageEl),
                    originX,
                    originY;

                originX = e.pageX;
                originY = e.pageY;

                $pageEl.off("mousemove", dirSelectorOnMousemove);
                $selector.remove();

                that._selectStarted = false;
                if (e.button === 2) {
                    return;
                }

                // when already selected item exist
                if (that._selectedItems.length > 0) {
                    $target = $(e.target).closest(that.itemSelector);

                    // when current drag started from item
                    if ($target.length) {
                        // and drag started item is selected
                        if ($target.hasClass("selected")) {
                            // will be handled on this.selectItem method
                            return;
                        }
                    } else {
                        if ($(e.target).closest(".selected-item-menus-wrapper").length) {
                            return;
                        }
                        // remove all previouse selected file el
                        that._selectedItems.clear();
                        $(that.itemSelector + ".selected", that.$el).removeClass("selected");
                    }
                }

                if ($selector.size() > 0) {
                    $selector.remove();
                }
                $selector = $("<div id='directory-selector'><div class='inner'></div></div>");
                $selector.css({
                    'left': originX,
                    'top': originY,
                    'width': 0,
                    'height': 0
                });
                dirSelectorOnMousemove = function(e) {
                    var moveX = e.pageX,
                        moveY = e.pageY,
                        width = Math.abs(moveX - originX),
                        height = Math.abs(moveY - originY),
                        newX = moveX < originX ? moveX : originX,
                        newY = moveY < originY ? moveY : originY,
                        isSelectionMode = false;

                    if (width > 10 || height > 10 &&
                            $("#directory-selector", $pageEl).size() === 0) {
                        $pageEl.append($selector);
                        that._selectStarted = true;
                    }
                    $selector.css({
                        'left': newX,
                        'top': newY,
                        'width': width,
                        'height': height
                    });
                    if ((lastSelectApplyX > -1 &&
                        Math.abs(lastSelectApplyX - moveX) < SELECTION_APPLY_MIN_DIFF) &&
                        (lastSelectApplyY > -1 &&
                        Math.abs(lastSelectApplyY - moveY) < SELECTION_APPLY_MIN_DIFF)) {
                        return;
                    }

                    // remove all previouse selected file el
                    // except when alt / shift key pressed
                    if ($sp.context.os === "MacOS") {
                        isSelectionMode |= e.metaKey;
                    } else {
                        isSelectionMode |= e.ctrlKey;
                    }
                    if (!isSelectionMode) {
                        that._selectedItems.clear();
                        $(that.itemSelector + ".selected", that.$el).removeClass("selected");
                    }

                    lastSelectApplyX = moveX;
                    lastSelectApplyY = moveY;

                    $.each(that._selectableItemEls, function(idx, $el) {
                        var offset, elX, elY, elWidth, elHeight, item, i;
                        item = that.getItemFromEl($el);
                        offset = $el.offset();
                        elX = offset.left;
                        elY = offset.top;
                        elWidth = $el.width();
                        elHeight = $el.height();

                        if (!$el || $el.hasClass("nonselectable") || !item) {
                            return true;
                        }
                        if (newY < elY + elHeight &&
                            newY + height > elY &&
                            newX < elX + elWidth &&
                            newX  + width > elX) {
                            $el.addClass("selected");
                            if (that._selectedItems.indexOf(item) < 0) {
                                that._selectedItems.push(item);
                            }
                            return true;
                        }
                        if (!isSelectionMode) {
                            $el.removeClass("selected");
                            i = that._selectedItems.indexOf(item);
                            if (i > -1) {
                                that._selectedItems.splice(i, 1);
                            }
                        }
                    });
                };
                $pageEl.on("mousemove", dirSelectorOnMousemove);
            }).off("mouseup").on("mouseup", function(e) {
                var $selector = $("#page #directory-selector");
                $pageEl.off("mousemove", dirSelectorOnMousemove);

                $selector.remove();
                that._selectStarted = false;
            });

        },
        contextmenuListener: function(e) {
            var $target = $(e.currentTarget),
                contextMenuView = this.parentView.subViews.contextMenuView,
                contextableTop,
                contextableLeft,
                contextableWidth,
                offset,
                x,
                y,
                $hoverTarget;

            if ($target.is("#filter")) {
                if (contextMenuView.isVisible()) {
                    contextMenuView.hide();
                }
                x = e.offsetX;
                y = e.offsetY;

                if (this.bgContextMenuRegionSelector) {
                    offset = $(this.bgContextMenuRegionSelector).offset();
                    contextableTop = offset.top;
                    contextableLeft = offset.left;
                    contextableWidth = $(this.bgContextMenuRegionSelector).outerWidth();

                    if (x < contextableLeft ||
                        x > contextableLeft + contextableWidth ||
                        y < contextableTop ) {
                        return;
                    }
                }

                $.each(this._visibleItemEls, function(idx, $el) {
                    var offset = $el.offset(),
                        elMinX = offset.left,
                        elMaxX = elMinX + $el.outerWidth(),
                        elMinY = offset.top - $(window).scrollTop(),
                        elMaxY = elMinY + $el.outerHeight();
                    if (x >= elMinX && x < elMaxX && y >= elMinY && y < elMaxY) {
                        $hoverTarget = $el;
                        return false;
                    }
                });

                if (!this.bgContextMenuRegionSelector && !$hoverTarget) {
                    return;
                }
            }

            this.showContextMenu(e, $hoverTarget);
            e.stopPropagation();
            e.preventDefault();
        },
        showContextMenu: function(e, $target) {
            var subViews = this.parentView.subViews,
                contextMenuView = subViews.contextMenuView,
                $target = $target || $(e.currentTarget),
                items = [],
                item;
            if (e.button !== 2) {
                contextMenuView.hide();
                return;
            }
            if (!$target.is(this.itemSelector)) {
                return contextMenuView.show(e, this.getUploadDir(), items);
            }
            if ($target.hasClass("nonselectable")) {
                e.stopPropagation();
                return false;
            }
            if ($target.hasClass("selected") && this.useItemSelectorUI) {
                $.merge(items, this._selectedItems);
            } else {
                item = subViews.contentView.getItemFromEl($target);
                if (item) {
                    items.push(item);
                }
            }
            return contextMenuView.show(e, this.getUploadDir(), items, $target);
        },
        onThumbnailUploaded: function() {
            throw "Not implemented";
        },
        createCollection: function(device, path, callback) {
            // need override
            throw "Not implemented";
        },
        getUploadDirPath: function(rootPath) {
            // need override
            throw "Not implemented";
        }
    });

    AlbumListView = BaseCollectionContentView.extend({
        template: "#tmpl-photo-albums",
        menuType: "photos",
        itemSelector: ".album",
        itemThumbSelector: ".thumb",
        useItemSelectorUI: false,
        compressFilter: "image",
        getUploadDirPath: function(rootPath) {
            return rootPath + "Syncport/images/";
        },
        createCollection: function(device, path, callback) {
            collection = new $sp.Model.AlbumCollectionModel(device);
            collection.loadSyncRootPath(callback);
        },
        getItemKey: function(item) {
            return item.path;
        },
        openItem: function(e) {
            var $target = e instanceof $ ?
                    e : $(e.currentTarget).closest(this.itemSelector),
                targetItem = this.getItemFromEl($target),
                path,
                encodedDeviceId;
            if ($target.hasClass("nonselectable")) {
                return;
            }
            encodedDeviceId =
                $sp.Utils.encodeDeviceId(targetItem.device.get("deviceId"))
            path = "device/" + encodedDeviceId +
                "/photo-album" + targetItem.path;
            $sp.router.navigate(path, { trigger: true });
        },
        downloadItem: function(e) {
            var $target = e instanceof $ ?
                    e : $(e.currentTarget).closest(this.itemSelector),
                targetFile = this.getItemFromEl($target),
                device,
                path,
                doDownload;

            if ($target.hasClass("nonselectable")) {
                return;
            }
            device = targetFile.getDevice();
            if (device.get("isTurnOn")) {
                path = targetFile.path;
                doDownload = function() {
                    $sp.FileTransfer
                        .registerReceiveFileWorker({
                                deviceId: device.get("deviceId"),
                                path: path
                            });
                    // push event
                    $sp.DeviceEventManager.pushDownloadEvent(
                        device.get("deviceId"), path);
                };
                if (device.get("connectionState") === "charged") {
                    $sp.confirm({
                            title: $sp.string(
                                "warning_data_charge__device_charged_connected__title"),
                            text: $sp.string(
                                "warning_data_charge__device_charged_connected__text"),
                            confirmText: $sp.string("proceed_download"),
                            closeText: $sp.string("cancel")
                        }, function(confirmed) {
                            if (confirmed) {
                                doDownload();
                            }
                        });
                } else {
                    doDownload();
                }
                return;
            }
            $sp.notify($sp.string("turnedoff_device_action_error",
                device.get("deviceName")), "error");
        },
        onSelectedItemMenuClicked: function(e) {
            var that = this,
                $target = $(e.currentTarget),
                action = $target.data("action"),
                album,
                $itemEl,
                $tmpItems,
                paths,
                device,
                doDownload;

            if (action === "delete") {
                paths = []
                $tmpItems = $(this.itemSelector + ".temp-selected");
                if ($tmpItems.size() === 0) {
                    return;
                }
                album = this.getItemFromEl($tmpItems);
                $.each(album.items, function(key, file) {
                    paths.push(file.path);
                });
                $sp.confirm({
                        title: $sp.string("are_you_sure"),
                        text: "<span>" +
                            $sp.string("are_you_sure_delete%s", paths.length) +
                            "</span><br/><strong>" +
                            $sp.string("warning_file_delete") +
                            "</strong>",
                        confirmText: $sp.string("proceed_delete"),
                        closeText: $sp.string("cancel")
                    }, function(confirmed) {
                        if (!confirmed) {
                            return;
                        }
                        that.sendRemoteAddDel(
                            that.getCollection(),
                            [],
                            paths,
                            function() {
                                album.isDeleting = true;
                                $tmpItems.addClass("nonselectable");
                            });
                    });
                return;
            } else if (action === "download-multiple") {
                paths = [];
                $tmpItems = $(this.itemSelector + ".temp-selected");
                if ($tmpItems.size() === 0) {
                    return;
                }
                album = this.getItemFromEl($tmpItems);
                paths.push(album.path);

                device = this.getCollection().device;
                doDownload = function() {
                    $sp.view.compressingProgressView.addJob({
                        device: device,
                        paths: paths,
                        filter: that.compressFilter
                    });
                };
                if (device.get("connectionState") === "charged") {
                    $sp.confirm({
                            title: $sp.string(
                                "warning_data_charge__device_charged_connected__title"),
                            text: $sp.string(
                                "warning_data_charge__device_charged_connected__text"),
                            confirmText: $sp.string("proceed_download"),
                            closeText: $sp.string("cancel")
                        }, function(confirmed) {
                            if (confirmed) {
                                doDownload();
                            }
                        });
                } else {
                    doDownload();
                }
            } else {
                return;
            }
        },
        onThumbnailUploaded: function() {
            var that = this,
                hashes = Array.prototype.slice.call(arguments),
                e = hashes.splice(0, 1),
                collection = this.getCollection();
            if (!collection || !Object.keys(collection.items).length) {
                return;
            }
            $.each(collection.items, function(key, album) {
                var file = album.getThumbnailFile();
                if (hashes.indexOf(file.getThumbnailHash()) > -1) {
                    file.thumbnailRequest && file.thumbnailRequest.abort();
                    file.thumbnailRequest = undefined;
                    file.setHasThumbnail(true);
                    that.refreshThumbnail($(that.itemThumbSelector, that.getItemEl(album)),
                        file.getThumbnailPath());
                }
            });
        },
        onItemSelectionChanged: function(selectedItems) {
        }
    });

    BaseFileCollectionContentView = BaseCollectionContentView.extend({
        events: function() {
            var events = BaseCollectionContentView.prototype.events,
                extendEvents = {},
                thumbSelector,
                openlinkSelector;

            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend({}, events, {
                        "click .orderable": "changeSortType"
                    });
        },
        openItem: function(e) {
            var $target = e instanceof $ ?
                    e : $(e.currentTarget).closest(this.itemSelector),
                targetFile = this.getItemFromEl($target),
                deviceName,
                device,
                path;
            if ($target.hasClass("nonselectable")) {
                return;
            }
            device = targetFile.getDevice();
            if (device.get("isTurnOn")) {
                this.parentView.subViews.fileOpenView = new FileOpenView(
                    $.extend({
                        parentView: this.parentView
                    }, this.parentView._options));

                this.parentView.subViews.fileOpenView.show(targetFile);
                return;
            }
            deviceName = device.get("deviceName");
            $sp.notify(
                $sp.string("turnedoff_device_action_error", deviceName), "error");
        },
        downloadItem: function(e) {
            var $target = e instanceof $ ?
                    e : $(e.currentTarget).closest(this.itemSelector),
                targetFile = this.getItemFromEl($target),
                device,
                path,
                doDownload;

            if ($target.hasClass("nonselectable")) {
                return;
            }
            device = targetFile.getDevice();
            if (device.get("isTurnOn")) {
                path = targetFile.path;
                doDownload = function() {
                    $sp.FileTransfer
                        .registerReceiveFileWorker({
                                deviceId: device.get("deviceId"),
                                path: path
                            });
                    // push event
                    $sp.DeviceEventManager.pushDownloadEvent(
                        device.get("deviceId"), path);
                };
                if (device.get("connectionState") === "charged") {
                    $sp.confirm({
                            title: $sp.string(
                                "warning_data_charge__device_charged_connected__title"),
                            text: $sp.string(
                                "warning_data_charge__device_charged_connected__text"),
                            confirmText: $sp.string("proceed_download"),
                            closeText: $sp.string("cancel")
                        }, function(confirmed) {
                            if (confirmed) {
                                doDownload();
                            }
                        });
                } else {
                    doDownload();
                }
                return;
            }
            deviceName = device.get("deviceName");
            $sp.notify(
                $sp.string("turnedoff_device_action_error", deviceName), "error");
        },
        onItemSelectionChanged: function(selectedItems) {
            var $menuEl = $(".selected-item-menus-wrapper", this.$el),
                $sortMenuEl = $(".collection-columns", this.$el),
                context,
                files = [],
                folders = [];

            if (!$menuEl) {
                return;
            }
            if (selectedItems.length > 0) {
                if (!this._selectedItemMenusTmpl) {
                    this._selectedItemMenusTmpl = _.template($(this.selectedItemMenusTmpl).html());
                }
                $.each(selectedItems, function(idx, item) {
                    if (item.isDirectory()) {
                        folders.push(item);
                    } else if (item.isFile()) {
                        files.push(item);
                    }
                });

                context = $.extend({}, this._context, {
                    files: files,
                    folders: folders,
                    device: this.getDevice()
                });
                $menuEl.html(this._selectedItemMenusTmpl(context));
                $(".filename", $menuEl).ellipsis({ width:240, ellipsis: "middle" });

                $sortMenuEl.hide();
                $menuEl.show();
            } else {
                $sortMenuEl.show();
                $menuEl.hide();
            }
        },
        onThumbnailUploaded: function() {
            var that = this,
                hashes = Array.prototype.slice.call(arguments),
                e = hashes.splice(0, 1),
                collection = this.getCollection();
            if (!collection || !Object.keys(collection.items).length) {
                return;
            }
            $.each(collection.items, function(key, file) {
                if (hashes.indexOf(file.getThumbnailHash()) > -1) {
                    file.thumbnailRequest && file.thumbnailRequest.abort();
                    file.thumbnailRequest = undefined;
                    file.setHasThumbnail(true);
                    that.refreshThumbnail($(that.itemThumbSelector, that.getItemEl(file)),
                        file.getThumbnailPath());
                }
            });
        },
        changeSortType: function(e) {
            var $target = $(e.currentTarget),
                prevOrderBy = $target.data("orderby"),
                collection = this.getCollection();

            if (!prevOrderBy) {
                collection.sortType = $target.data("orderType");
                collection.sortDirection = "asc";
            } else if (prevOrderBy === "desc") {
                collection.sortDirection = "asc";
            } else {
                collection.sortDirection = "desc";
            }
            this.render(true);
        }
    });

    AlbumView = BaseFileCollectionContentView.extend({
        template: "#tmpl-photo-album",
        itemSelector: ".photo",
        itemThumbSelector: ".thumb",
        menuType: "photo-album",
        compressFilter: "image",
        initialize: function(options) {
            _.bindAll(this, "showPhotoNavigator");
            BaseFileCollectionContentView.prototype.initialize.call(this, options);
        },
        getUploadDirPath: function(rootPath) {
            return this.getCollection().path;
        },
        createCollection: function(device, path, callback) {
            collection = new $sp.Model.AlbumModel(device, path);
            collection.loadSyncRootPath(callback);
        },
        events: function() {
            var events = BaseFileCollectionContentView.prototype.events,
                openItemEventKeys = [];
            if (_.isFunction(events)) {
                events = events.call(this);
            }

            // replace extended openItem event to dbclclick
            $.each(events, function(key, handler) {
                if (handler === "openItem") {
                    openItemEventKeys.push(key);
                }
            });
            $.each(openItemEventKeys, function(idx, key) {
                delete events[key];
            });
            events["dblclick " + this.itemSelector] = "openItem";

            return $.extend ({}, events, {
                "mouseenter .photo": "showPhotoDate",
                "mouseleave .photo": "hidePhotoDate",
                "mouseenter #nav-photo-groups-placeholder": "showPhotoNavigator",
                "mouseenter .nav-photo-group-wrapper": "showPhotoNavigator",
                "mouseleave #nav-photo-groups-placeholder": "hidePhotoNavigator"
            });
        },
        loadDevice: function(device, path, callback) {
            var that = this;

            BaseFileCollectionContentView.prototype.loadDevice.call(
                this, device, path, function(success) {
                    if (success) {
                        $(window).on("scroll", that.showPhotoNavigator);
                    }
                    callback && callback.apply(that, arguments);
                });
        },
        selectItem: function(e) {
            BaseFileCollectionContentView.prototype.selectItem.call(this, e, true);
        },
        showPhotoNavigator: function(e) {
            if (!this._showPhotoNavigator) {
                this._showPhotoNavigator = _.throttle($.proxy(function() {
                    var $nav = $("#nav-photo-groups", this.$el),
                        $placeholder = $("#nav-photo-groups-placeholder", this.$el);

                    if (this._hidePhotoNavigatorTimer) {
                        clearTimeout(this._hidePhotoNavigatorTimer);
                        delete this._hidePhotoNavigatorTimer;
                    }
                    $nav.css("display", "block");
                    $nav.stop(true).fadeTo(300, 1);

                    if (e.type === "scroll" &&
                            ($placeholder.size() && !$placeholder.is(":hover"))) {
                        this._hidePhotoNavigatorTimer = setTimeout(
                            $.proxy(this.hidePhotoNavigator, this), 3000);
                    }
                }, this), 300, { trailing: false });
            }
            this._showPhotoNavigator(e);
        },
        hidePhotoNavigator: function(e) {
            var $nav = $("#nav-photo-groups", this.$el);

            if (this._hidePhotoNavigatorTimer) {
                clearTimeout(this._hidePhotoNavigatorTimer);
                delete this._hidePhotoNavigatorTimer;
            }
            $nav.stop(true).fadeTo(500, 0, function() {
                $nav.css("display", "none");
            });
        },
        showPhotoDate: function(e) {
            var $target = $(e.currentTarget),
                $label = $(".label", $target),
                $name = $(".name", $label);
            $label.show();
            if($name[0].offsetWidth < $name[0].scrollWidth &&
                    !$name.attr('title')){
                $name.attr('title', $name.text());
            }
        },
        hidePhotoDate: function(e) {
            var $target = $(e.currentTarget),
                $label = $(".label", $target);
            $label.hide();
        },
        pageInnerLink: function(e) {
            var $el = $(e.currentTarget),
                targetId = $el.data("target"),
                $target = $("#" + targetId),
                $fixedContainer= $(".fixed-container", this.$el),
                scrollTop;
            if (!$target.size()) {
                return;
            }
            scrollTop = $target.offset().top + $(window).scrollTop() -
                ($fixedContainer.offset().top + $fixedContainer.outerHeight());
            $(window).scrollTop(scrollTop);
            e.stopPropagation();
        },
        remove: function() {
            $(window).off("scroll", this.showPhotoNavigator);
            BaseFileCollectionContentView.prototype.remove.apply(this, arguments);
        }
    });

    MusicListView = BaseFileCollectionContentView.extend({
        template: "#tmpl-music-list",
        compressFilter: "music",
        getUploadDirPath: function(rootPath) {
            return rootPath + "Syncport/music/";
        },
        createCollection: function(device, path, callback) {
            collection = new $sp.Model.MusicListModel(device);
            collection.loadSyncRootPath(callback);
        }
    })

    VideoListView = BaseFileCollectionContentView.extend({
        template: "#tmpl-video-list",
        compressFilter: "video",
        getUploadDirPath: function(rootPath) {
            return rootPath + "Syncport/video/";
        },
        createCollection: function(device, path, callback) {
            collection = new $sp.Model.VideoListModel(device);
            collection.loadSyncRootPath(callback);
        }
    })

    DocumentListView = BaseFileCollectionContentView.extend({
        template: "#tmpl-document-list",
        compressFilter: "document",
        getUploadDirPath: function(rootPath) {
            return rootPath + "Syncport/documents/";
        },
        createCollection: function(device, path, callback) {
            collection = new $sp.Model.DocumentListModel(device);
            collection.loadSyncRootPath(callback);
        },
        onThumbnailUploaded: function(hashes) {
            // do nothing
        }
    })

    DirListView = BaseFileCollectionContentView.extend({
        VIEW_TYPES: {
            ICON: "display-as-icon",
            LIST: "display-as-list"
        },
        template: "#tmpl-dir-list-section",
        dirListTmpl: "#tmpl-dir-list",
        bgContextMenuRegionSelector: "#dirmodels",
        initialize: function(options) {
            var that = this;

            this._viewType = this.VIEW_TYPES.LIST;
            this._childImageFiles = [];

            BaseFileCollectionContentView.prototype.initialize.call(this, options);
            _.bindAll(this, "moveDirectory");

            this._dirListTmpl = _.template($(this.dirListTmpl).html());
            this.$el.on("mouseenter", this.itemSelector, function() {
                var $name = $(".name", $(this));
                if($name[0].offsetWidth < $name[0].scrollWidth &&
                        !$name.attr('title')){
                    $(this).attr('title', $name.text());
                }
            });
        },
        events: function() {
            var events = BaseFileCollectionContentView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }

            return $.extend ({}, events, {
                "click .path .device": "navRoot",
                "click .path .subpath": "navPath",
                "click .view-type-menus .menu" : "setViewType"
            });
        },
        onDeviceStateChanged: function(device) {
            var that = this;

            // XXX: extend grandparent method
            BaseDeviceContentView.prototype.onDeviceStateChanged
                .apply(this, arguments);

            if (this._collection && this._collection.isLoaded) {
                this.render();
                this.renderDirList();
            }
        },
        loadDevice: function(device, path, callback) {
            var that = this,
                selectors = [
                        this.$el.selector + " " + this.itemSelector,
                        "#filter"
                    ];


            if (this.bgContextMenuSelector) {
                selectors.push(
                    this.$el.selector + " " + this.bgContextMenuSelector);
            }

            BaseDeviceContentView.prototype.loadDevice.call(
                this, device, path, callback);

            if (this._collection) {
                this.stopListening(this._collection);
            }


            this.createCollection(device, path, $.proxy(function(collection) {
                this._collection = collection;
                this.moveDirectory(this._collection, {
                    success: function() {

                        if (this.device !== device) {
                            return;
                        }

                        $(document).on("contextmenu", selectors.join(","),
                            that.contextmenuListener);
                        that.parentView.subViews.dropzoneView.initDropzone();

                        callback && callback(true);
                    },
                    failure: function(msg) {
                        callback && callback(false, msg);
                    }
                });
            }, this));
        },
        createCollection: function(device, path, callback) {
            var name, dir, subPaths, i, root, p;

            if (!path) {
                path = "/";
            }

            if (!path.endsWith("/")) {
                path += "/";
            }

            subPaths = path.split("/");

            root = new $sp.Model.VirtualDevice(device);
            path = "/";

            p = root;

            for (i = 1; i < subPaths.length - 1; i++) {
                name = subPaths[i];
                path += name + "/";
                dir = new $sp.Model.VirtualDirectory(device, name, path);
                p.addItem(dir);
                p = dir;
            }
            root.loadSyncRootPath($.proxy(function() {
                callback && callback(p);
            }, this));
        },
        getUploadDir: function() {
            return this.getCollection();
        },
        navRoot: function() {
            var root = this.getCollection().getRoot(),
                syncRoot = root.getSyncRoot();
            this.moveDirectory(syncRoot);
        },
        navPath: function(e) {
            var $target = $(e.currentTarget),
                path = $target.data("path"),
                root = this.getCollection().getRoot(),
                pathFile = root.getFile(path);

            if (!pathFile || pathFile.isFile()) {
                $sp.notify("path is not directory", "error");
                return;
            }
            this.moveDirectory(pathFile);
        },
        openItem: function(e) {
            var $target = e instanceof $ ?
                    e : $(e.currentTarget).closest(this.itemSelector),
                type = $target.data("type"),
                curr;
            if (this._viewType === this.VIEW_TYPES.ICON && e instanceof $.Event) {
                if (!this._lastOpenFileClicked) {
                    this._lastOpenFileClicked = (new Date()).getTime();
                    return;
                }
                curr = (new Date()).getTime();
                if (curr - this._lastOpenFileClicked > 300) {
                    this._lastOpenFileClicked = curr;
                    return;
                }
            }
            if (type === "file") {
                BaseFileCollectionContentView.prototype.openItem
                    .apply(this, arguments);
                return;
            } else if (type === "directory") {
                this.openDirectory(e);
                return;
            }
        },
        openDirectory: function(e) {
            var $target = $(e.currentTarget).closest(this.itemSelector),
                key = unescape($target.data("key")),
                targetDir = this.getCollection().items[key];
            if ($target.hasClass("nonselectable")) {
                return;
            }
            if (!targetDir) {
                $sp.notify("failed to find proper directory", "error");
                return;
            }
            this.moveDirectory(targetDir);
        },
        openParent: function() {
            var targetDir = this.getCollection().getParent();
            this.moveDirectory(targetDir);
        },
        setViewType: function(e) {
            var $target = $(e.currentTarget),
                viewType;
            if (!$target.hasClass("selected")) {
                viewType = $target.data("target");
                this._viewType = this.VIEW_TYPES[viewType.toUpperCase()];
                this.render(true);
                if (this._collection.isLoaded) {
                    this.renderDirList(true);
                }
            }
        },
        onAddTempItem: function(item) {
            this.render(true);
            this.renderDirList(true);
        },
        moveDirectory: function(dirModel, callback) {
            var that = this,
                prevDirModel,
                path,
                device,
                encodedDeviceId;

            prevDirModel = this.getCollection();
            if (prevDirModel && prevDirModel !== dirModel) {
                this.stopListening(prevDirModel);
            }

            if (this.useItemSelectorUI) {
                this._selectedItems.clear();
                $(this.itemSelector + ".selected", this.$el)
                    .removeClass("selected");
            }

            this._collection = dirModel;
            this.listenTo(this._collection, "add.temp-child", this.onAddTempItem);

            device = this._collection.getDevice();
            encodedDeviceId = $sp.Utils.encodeDeviceId(device.get("deviceId"));
            path = "device/" + encodedDeviceId + "/directory";
            path += this._collection.path;

            $sp.router.navigate(path, { trigger: false });

            this._collection.load({
                success: function(changedDirs) {
                    var root = this.getRoot(),
                        syncRoot = root.getSyncRoot(),
                        uri,
                        device,
                        encodedDeviceId;
                    if (this.device !== that.getDevice()) {
                        return;
                    }
                    if (this.path.length < syncRoot.path.length) {
                        device = this.getDevice();
                        encodedDeviceId = $sp.Utils.encodeDeviceId(device.get("deviceId"));
                        uri = "device/" + encodedDeviceId + "/directory";
                        uri += syncRoot.path;
                        $sp.router.navigate(uri, { trigger: false, replace: true});
                        that.moveDirectory(syncRoot, callback);
                        return;
                    }

                    if (changedDirs.indexOf(this) > -1) {
                        that.render();
                        that.renderDirList();
                    }
                    callback && callback.success.apply(this, arguments);
                },
                failure: function(msg) {
                    callback && callback.failure.apply(this, arguments);
                }
            });

            this.render(true);
            if (this._collection.isLoaded) {
                this.renderDirList(true);
            }
        },
        onCollectionChanged: function(e, ownerId) {
            var that = this,
                device = this.getDevice();
            if (device && device.get("deviceId") === ownerId &&
                    this._collection.isLoaded) {
                this._collection.load({
                    success: function(changedDirs) {
                        if (changedDirs.indexOf(this) > -1) {
                            that.render();
                            that.renderDirList();
                        }
                    },
                    failure: function(msg) {
                        $sp.notify(msg, "error");
                    }
                });
            }
        },
        render: function(forceRefresh) {
            if (!this._render) {
                this._render = $.proxy(function() {
                    var context,
                        html;
                    context = $.extend({}, this._context, {
                            model: this.getCollection(),
                            viewType: this._viewType,
                            VIEW_TYPES: this.VIEW_TYPES
                        });
                    html = this._template(context);
                    this.$el.html(html);
                    this.$sendFormFileInput = $("send-form input[type='file']");
                    this.$dirModels = $("#dirmodels", this.$el);

                    // run adjust right after rendering DOM element
                    setTimeout(this.adjustContentFrame, 0);
                    $(".tooltip").tooltipster({
                        delay: 0,
                        speed: 0
                    });
                }, this);
            }
            if (!this._throttledRender) {
                this._throttledRender = _.throttle(this._render, 3000);
            }
            if (forceRefresh) {
                this._render();
            } else {
                this._throttledRender();
            }
        },
        renderDirList: function(forceRefresh) {
            if (!this._renderDirList) {
                this._renderDirList = $.proxy(function() {
                    var that = this,
                        context,
                        html,
                        childImageFiles = [];

                    context = $.extend({}, this._context, {
                            model: this.getCollection(),
                            viewType: this._viewType,
                            VIEW_TYPES: this.VIEW_TYPES,
                            selectedItems: this._selectedItems,
                            tempSelectedItem: this.getItemFromEl(
                                $(this.itemSelector + ".temp-selected")) || null
                        });
                    html = this._dirListTmpl(context);
                    this.$dirModels.html(html);
                    this.onItemSelectionChanged(this._selectedItems);

                    $.each(this.getCollection().getSortedChildList(),
                        function(idx, childFile) {
                            if (childFile.isFile() &&
                                (childFile.isImageFile() || childFile.isMediaFile())) {
                                childImageFiles.push(childFile);
                            }
                        });
                    this._childImageFiles = childImageFiles;
                    this._candidateItemSetEls = $.map($(".item-set", this.$dirModels),
                        function(el) { return $(el); });
                    this._visibleItemEls = [];
                    this._selectableItemEls = [];

                    this.bindVisibilityTracker({
                        candidateSetEls: this._candidateItemSetEls,
                        visibleEls: this._visibleItemEls,
                        selectableEls: this._selectableItemEls
                    });

                    // run adjust right after rendering DOM element
                    setTimeout(this.adjustContentFrame, 0);
                }, this);
            }
            if (!this._throttledRenderDirList) {
                this._throttledRenderDirList = _.throttle(this._renderDirList, 3000);
            }
            if (forceRefresh) {
                this._renderDirList();
            } else {
                this._throttledRenderDirList();
            }
        },
        onThumbnailUploaded: function() {
            var that = this,
                hashes = Array.prototype.slice.call(arguments),
                e = hashes.splice(0, 1);

            if (!this._childImageFiles) {
                return;
            }
            $.each(this._childImageFiles, function(idx, file) {
                if (hashes.indexOf(file.getThumbnailHash()) > -1) {
                    file.thumbnailRequest && file.thumbnailRequest.abort();
                    file.thumbnailRequest = undefined;
                    file.setHasThumbnail(true);
                    that.refreshThumbnail($(that.itemThumbSelector, that.getItemEl(file)),
                        file.getThumbnailPath());
                }
            });
        },
        getItemEl: function(file) {
            var type, key, $el, selector;
            if (!file) {
                return null;
            }
            type = file.isFile() ? "file" : "directory";
            key = file.isDirectory() ? file.name + "/" : file.name;
            selector = this.itemSelector;
            selector += "[data-type='" + type + "']";
            selector += "[data-key='" + escape(key.toLowerCase()) + "']";
            $el = $(selector, this.$el);
            return $el.length > 0 ? $el : null;
        }
    });

    FileOpenView = SyncportView.extend({
        modalTmpl:"#tmpl-modal-file",
        imageTmpl:"#tmpl-modal-image-preview",
        previewExtensions: ["txt", "doc", "docx", "docm", "pdf",
            "rtf", "ppt", "pptx", "xls", "xlsx"],
        initialize: function(options) {
            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "hide", "show", "onActionClicked", "adjustModal");

        },
        showAsImagePreview: function() {
            var that = this,
                context = {};
            $.extend(context, this._context, {
                file: this.file
            });

            if (!this._imageTmpl) {
                this._imageTmpl = _.template($(this.imageTmpl).html());
            }

            this.$imagePreview = $(this._imageTmpl(context));
            this.$imagePreview
                .on("click", ".btn-close, .shadow", this.hide)
                .on("click", ".action", this.onActionClicked);
            $("#modal").append(this.$imagePreview);
            $("#modal-image-preview").fadeIn();

            $sp.FileTransfer.registerReceiveFileWorker({
                deviceId: this.file.getDevice().get("deviceId"),
                path: this.file.path,
                callback: function(path) {
                    var $photoWrapper = $(".photo-wrapper", that.$imagePreview),
                        $photo = $(".photo", that.$imagePreview),
                        $btnClose = $(".btn-close", that.$imagePreview),
                        $loader = $(".loader-window", that.$imagePreview),
                        $img = $("<img class='loaded' src='" + path + "'/>"),
                        height;
                    $photo.html($img);
                    $img.load(function() {
                        var imgOriHeight = $(this).height(),
                            imgOriWidth = $(this).width(),
                            imgAspectRatio,
                            css, degree, resize;


                        css = {
                            "maxWidth": "100%",
                            "maxHeight": "100%"
                        };
                        degree = 360 + -1 * parseInt(that.file.getImageOrientation()) * 90;
                        if (degree %360 !== 0) {
                            $.extend(css, {
                                "-webkit-transform": "rotate(" + degree + "deg)",
                                "-moz-transform": "rotate(" + degree + "deg)",
                                "-o-transform": "rotate(" + degree + "deg)",
                                "-ms-transform": "rotate(" + degree + "deg)",
                                "transform": "rotate(" + degree + "deg)"
                            });
                        }
                        if (degree % 180 == 0) {
                            imgAspectRatio = imgOriWidth / imgOriHeight;
                        } else {
                            imgAspectRatio = imgOriHeight / imgOriWidth;
                        }

                        resize = function() {
                            var winHeight,
                                winWidth,
                                winAspectRatio,
                                height, width;

                            winHeight = $(window).height()
                                - parseInt($photoWrapper.css("top"))
                                - parseInt($photoWrapper.css("bottom"));
                            winWidth = $(window).width()
                                - parseInt($photoWrapper.css("left"))
                                - parseInt($photoWrapper.css("right"));

                            winAspectRatio = winWidth / winHeight;

                            if (winAspectRatio > imgAspectRatio) {
                                if (degree % 180 == 0) {
                                    height = Math.min(imgOriHeight, winHeight);
                                    width = height * imgAspectRatio;
                                } else {
                                    width = Math.min(imgOriWidth, winHeight);
                                    height = width * imgAspectRatio;
                                }
                            } else {
                                if (degree % 180 == 0) {
                                    width = Math.min(imgOriWidth, winWidth);
                                    height = width / imgAspectRatio;
                                } else {
                                    height = Math.min(imgOriHeight, winWidth);
                                    width = height / imgAspectRatio;
                                }
                            }

                            $photoWrapper.css({
                                height: height,
                                width: width
                            });
                        };

                        resize();
                        $loader.hide();
                        $photo.css("opacity", 1);

                        $img.css(css);
                        $btnClose.show();
                        that.onResize = resize;
                        $(window).on("resize", that.onResize);
                    });
                }
            });
        },
        showAsModal: function() {
            var that = this,
                context = {},
                $titleName,
                $name,
                callback,
                extension,
                usePreview = false,
                idx,
                encodedPath,
                googlePath;

            idx = this.file.name.lastIndexOf(".");
            if (idx > -1) {
                extension = this.file.name.substring(idx + 1).toLowerCase();
                usePreview = Boolean(this.previewExtensions
                    .indexOf(extension) > -1);
            }

            $.extend(context, this._context, {
                file: this.file,
                usePreview: usePreview
            });

            if (!this._modalTmpl) {
                this._modalTmpl = _.template($(this.modalTmpl).html());
            }
            this.$modal = $(this._modalTmpl(context));
            $titleName = $(".modal-title", this.$modal);
            $name = $(".name", this.$modal);

            $("#modal").append(this.$modal);
            $titleName.ellipsis({ width:400, ellipsis: "middle"});

            $("#modal-file").modal("show")
                .on("hidden.bs.modal", function() {
                    $(this).remove();
                    delete that._shownAs;
                    that.enableScroll();
                })
                .on("click", ".action", this.onActionClicked);

            if (usePreview) {
                this.adjustModal();
                $(window).on("resize", this.adjustModal);
            }

            if (usePreview) {
                callback = function(path) {
                    var $modalBody = $(".modal-body", that.$modal);

                    path += "&force_dl=0";
                    encodedPath = encodeURIComponent(path);
                    googlePath = "https://docs.google.com/viewer?url=" +
                        encodedPath + "&embedded=true&rm=minimal";

                    $modalBody.append("<iframe src='" + googlePath + "'></iframe>");
                    $("iframe", $modalBody).on("load", function() {
                        if (that.googleLoadTimeout) {
                            clearTimeout(that.googleLoadTimeout);
                        }
                        $(".loader-wrapper", $modalBody).remove();
                    });
                    that.googleLoadTimeout = setTimeout(function() {
                        var failureHtml;
                        if ($sp.context.locale == "ko_KR") {
                            failureHtml = "<div class='failed-to-load-by-google'>" +
                                "미리보기를 생성하는데 평소보다 더 시간이 걸리고 있습니다.<br/>" +
                                "<a class='download' data-action='download'>여기</a>를 눌러 파일을 다운받으실 수 있습니다.</div>";
                        } else {
                            failureHtml = "<div class='failed-to-load-by-google'>" +
                                "Taking much time than usual to creating preview...<br/>you can downaload with " +
                                "<a class='download' data-action='download'>here</a></div>";
                        }
                        $(".loader-wrapper", $modalBody).append(failureHtml);
                        $(".failed-to-load-by-google .download", $modalBody)
                            .on("click", that.onActionClicked);
                        }, 10000)
                };
                $sp.FileTransfer.registerReceiveFileWorker({
                        deviceId: this.file.getDevice().get("deviceId"),
                        path: this.file.path,
                        callback: callback,
                        forceRelay: true
                    });
            }
        },
        disableScroll: function() {
            $("body").addClass("prevent-scroll");
        },
        enableScroll: function() {
            $("body").removeClass("prevent-scroll");
        },
        adjustModal: function() {
            var $header,
                headerHeight,
                headerTop,
                bodyHeight;

            $header = $(".modal-header", this.$modal);
            headerHeight = $header.outerHeight();
            headerTop = $header.offset().top;

            bodyHeight = $(window).height() - 150;
            $(".modal-body", this.$modal).css("height", bodyHeight);
        },
        show: function(file) {
            this.file = file;
            this.listenTo(this.file, "remove", this.hide);
            if (file.isImageFile()) {
                this._shownAs = "imagePreview";
                this.showAsImagePreview();
            } else {
                this._shownAs = "modal";
                this.showAsModal();
            }
            this.disableScroll();
        },
        hide: function() {
            var that = this;
            if (this._shownAs === "modal") {
                $("#modal-file").modal("hide");
            } else if (this._shownAs === "imagePreview") {
                $("#modal-image-preview").fadeOut(function() {
                    $(this).remove();
                    $(window).off("resize", that.onResize);
                    that.enableScroll();
                });
            }
            delete this.file;
            delete this._shownAs;
            $(window).off("resize", this.adjustModal);
            if (this.onResize) {
                $(window).off("resize", this.onResize);
            }
        },
        onActionClicked: function(e) {
            var $target = $(e.currentTarget),
                that = this,
                device = this.file.getDevice(),
                doDownload;
            if ($target.data("action") === "download") {
                doDownload = function() {
                    $sp.FileTransfer.registerReceiveFileWorker({
                            deviceId: device.get("deviceId"),
                            path: that.file.path
                        });
                };
                if (device.get("connectionState") === "charged") {
                    $sp.confirm({
                            title: $sp.string(
                                "warning_data_charge__device_charged_connected__title"),
                            text: $sp.string(
                                "warning_data_charge__device_charged_connected__text"),
                            confirmText: $sp.string("proceed_download"),
                            closeText: $sp.string("cancel")
                        }, function(confirmed) {
                            if (confirmed) {
                                doDownload();
                            }
                        });
                } else {
                    doDownload();
                }
            } else if ($target.data("action") === "delete") {
                $sp.confirm({
                        title: $sp.string("are_you_sure"),
                        text: "<span>" +
                            $sp.string("are_you_sure_delete_file%s", this.file.name) +
                            "</span><br/><strong>" +
                            $sp.string("warning_file_delete") +
                            "</strong>",
                        confirmText: $sp.string("proceed_delete"),
                        closeText: $sp.string("cancel")
                    }, function(confirmed) {
                        if (!confirmed) {
                            return;
                        }
                        that.sendRemoteAddDel(
                            that.file.getRoot(),
                            [],
                            [that.file.path],
                            function() {
                                that.hide()
                            });
                    })
            }
        },
        sendRemoteAddDel: function() {
            // proxy function
            var contentView = this.parentView.subViews.contentView;
            contentView
                .sendRemoteAddDel.apply(contentView, arguments);
        }
    });

    BaseContextMenuView = SyncportView.extend({
        id: "context-menu-wrapper",
        templateGeneral: "#tmpl-context-menu-general",
        templateFiles: "#tmpl-context-menu-files",
        templateFile: "#tmpl-context-menu-file",
        templateDisconnected: "#tmpl-context-menu-disconnected",
        initialize: function(options) {
            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "hide", "show", "isVisible");
            this._itemSelector = options.itemSelector || ".item";
            this._applyItems = [];
            this.$el.hide();
            $(options.elHolder).html(this.$el);
        },
        events: function() {
            var events = SyncportView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend ({}, events, {
                "click .menu": "onMenuClicked"
            });
        },
        onMenuClicked: function(e) {
            var contentView = this.parentView.subViews.contentView;
            contentView.onSelectedItemMenuClicked(e);
            this.hide();
        },
        show: function(e, parentDir, items, $target) {
            // need implement
        },
        hide: function(e) {
            this._applyItems.length = 0;
            $("#filter").click();
            $("#filter").hide();
            $(this._itemSelector + ".temp-selected")
                .removeClass("temp-selected");
            this.$el.hide();
        },
        isVisible: function() {
            return this.$el.is(":visible");
        },
        remove: function() {
            $("#fileter").hide();
            Backbone.View.prototype.remove.call(this);
            return this;
        }
    });

    FileContextMenuView = BaseContextMenuView.extend({
        show: function(e, parentDir, files, $target) {
            var that = this,
                html,
                contentView = this.parentView.subViews.contentView;

            $target = $target || $(e.currentTarget);
            this.hide()

            if ($target.is(this._itemSelector) && !$target.hasClass("selected")) {
                $target.addClass("temp-selected");
            }

            this._parentDir = parentDir;
            this._applyItems = files || [];
            if (!parentDir.getDevice().get("isTurnOn")) {
                if (!this._contextMenuDisconnectedTmpl) {
                    html = $(this.templateDisconnected).html();
                    this._contextMenuDisconnectedTmpl = _.template(html);
                }
                html = this._contextMenuDisconnectedTmpl();
            } else if (this._applyItems.length > 1 ||
                    (this._applyItems.length > 0 && this._applyItems[0].isDirectory())) {
                // show files context menus
                if (!this._contextMenuFilesTmpl) {
                    html = $(this.templateFiles).html();
                    this._contextMenuFilesTmpl = _.template(html);
                }
                html = this._contextMenuFilesTmpl();
            } else if (this._applyItems.length > 0) {
                // show file context menu
                if (!this._contextMenuFileTmpl) {
                    html = $(this.templateFile).html();
                    this._contextMenuFileTmpl = _.template(html);
                }
                html = this._contextMenuFileTmpl();
            } else {
                // show general context menu
                if (!this._contextMenuGeneralTmpl) {
                    html = $(this.templateGeneral).html();
                    this._contextMenuGeneralTmpl = _.template(html);
                }
                html = this._contextMenuGeneralTmpl();
            }
            this.$el.html(html)
                .find(".context-menu").css({
                    'left':e.pageX,
                    'top':e.pageY
                });
            this.$el.show();
            $("#filter").show();
            $("#filter").one("click", function() {
                that.hide.apply(that, arguments);
            });

            this._onScroll = function() {
                that.hide.apply(that, arguments);
            };
            $(window).one("scroll", this._onScroll);

            return false;
        }
    });

    AlbumContextMenuView = BaseContextMenuView.extend({
        show: function(e, parentDir, albums, $target) {
            var that = this,
                html,
                contentView = this.parentView.subViews.contentView;

            $target = $target || $(e.currentTarget);
            this.hide()

            if ($target.is(this._itemSelector) && !$target.hasClass("selected")) {
                $target.addClass("temp-selected");
            }

            this._parentDir = parentDir;
            this._applyItems = albums || [];
            if (!parentDir.getDevice().get("isTurnOn")) {
                if (!this._contextMenuDisconnectedTmpl) {
                    html = $(this.templateDisconnected).html();
                    this._contextMenuDisconnectedTmpl = _.template(html);
                }
                html = this._contextMenuDisconnectedTmpl();
            } else if (this._applyItems.length > 0) {
                // show files context menus
                if (!this._contextMenuFilesTmpl) {
                    html = $(this.templateFiles).html();
                    this._contextMenuFilesTmpl = _.template(html);
                }
                html = this._contextMenuFilesTmpl();
            } else {
                return;
            }
            this.$el.html(html)
                .find(".context-menu").css({
                    'left':e.pageX,
                    'top':e.pageY
                });
            this.$el.show();
            $("#filter").show();
            $("#filter").one("click", function() {
                that.hide.apply(that, arguments);
            });

            this._onScroll = function() {
                that.hide.apply(that, arguments);
            };
            $(window).one("scroll", this._onScroll);

            return false;
        }
    });


    MinimizableModalView = SyncportView.extend({
        modalTmpl: undefined,
        minibarTmpl: undefined,
        initialize: function(options) {
            var modalTmpl, minTmpl;
            SyncportView.prototype.initialize.call(this, options);
            _.bindAll(this, "maximize", "minimize", "show", "hide");

            modalTmpl = _.template($(this.modalTmpl).html());
            this.$modal = $(modalTmpl(this._context));
            $("#modal").append(this.$modal);
            this.$modal.on("hide.bs.modal", this.minimize);

            minTmpl = _.template($(this.minibarTmpl).html());
            this.$minibar = $(minTmpl(this._context));
            $("#minibar-holder").append(this.$minibar);
            this.$minibar.on("click", ".header", this.maximize);
        },
        minimize: function() {
            if (!this._hideOnMinimize) {
                this.showMinibar();
            }
            this.hideModal();
        },
        maximize: function() {
            this.hideMinibar();
            this.showModal();
        },
        showModal: function() {
            this.$modal.modal("show");
        },
        hideModal: function() {
            var that = this;
            if (!this.$modal._hiding) {
                this.$modal._hiding = true;
                this.$modal.modal("hide")
                    .one("hidden.bs.modal", function(){
                        that.$modal._hiding = false;
                        if (that._hideOnMinimize) {
                            that.onHidden();
                        }
                    });
            }
        },
        showMinibar: function() {
            this.$minibar.show();
            this.$minibar.animate({
                "marginTop":0
            }, 300);
            this._isMinimized = true;
        },
        hideMinibar: function() {
            this.$minibar.animate({
                "marginTop":this.$minibar.height() + "px"
            }, 300, function() {
                $(this).hide();
            });
            this._isMinimized = false;
        },
        isVisible: function() {
            return this.$modal.is(":visible") || this.$minibar.is(":visible");
        },
        show: (function() {
            return _.throttle(function() {
                if (!this.isVisible()) {
                    this.maximize();
                }
                this._hideOnMinimize = false;
            }, 500);
        }()),
        hide: (function() {
            return _.throttle(function() {
                this.hideMinibar();
                this._hideOnMinimize = true;
            }, 500);
        }()),
        remove: function() {
            this.$modal.remove();
            this.$minibar.remove();
            SyncportView.prototype.remove.apply(this, arguments);
        },
        onHidden: function() {
        }
    });

    views.SigninView = SyncportPageView.extend({
        template: "#tmpl-page-signin",
        tagName: "div",
        id: "page-signin",

        initialize: function(options) {
            SyncportPageView.prototype.initialize.call(this, options);
            $(pageFrame).html(this.el);
            this.render();
        },

        events: function() {
            var events = SyncportPageView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend ({}, events, {
                "click .link-signup" : "navSignup",
                "click .btn-signin" : "doSignin",
                "submit #form-signin" : "doSignin"
            });
        },

        navSignup: function() {
            $sp.router.navigate('signup', { trigger: true });
        },

        doSignin: function(e) {
            var that = this,
                email = this.$inputEmail.val(),
                passwd = this.$inputPasswd.val();
            if (!$sp.context.browser.support) {
                $sp.notify("Not supported browser");
                return;
            }
            if ($sp.context.isMobile) {
                $sp.alert("Syncport does not support mobile web currently");
                return;
            }
            $sp.AccountManager.signin(email, passwd, function(result) {
                if (result.success && result.account) {
                    $sp.router.navigate('', { trigger: true });
                } else {
                    that.showError(result.errorMsg || "Failed to signin");
                }
            });
            e.preventDefault();
            return false;
        },

        render: function() {
            this.$el.html(this._template(this._context));
            this.$inputEmail = $("#input_email", this.$el);
            this.$inputPasswd = $("#input_passwd", this.$el);
        },

        showError: function(error) {
            $sp.notify(error, "error");
        }
    });

    views.SignupView = SyncportPageView.extend({
        template: "#tmpl-page-signup",
        tagName: "div",
        id: "page-signup",

        initialize: function(options) {
            SyncportPageView.prototype.initialize.call(this, options);
            $(pageFrame).html(this.el);
            this.render();
        },

        events: function() {
            var events = SyncportPageView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend ({}, events, {
                "click .link-signin": "navSignin",
                "click .btn-signup" : "doSignup",
                "submit #form-signup" : "doSignup"
            });
        },

        navSignin: function() {
            $sp.router.navigate('signin', { trigger: true });
        },

        doSignup: function(e) {
            var that = this,
            data;
            e.preventDefault();

            if ($sp.context.isMobile) {
                $sp.alert("Syncport does not support mobile web currently");
                return;
            }
            //if (!this.$inputAgreeTerm.is(":checked")) {
            //    this.showError($sp.string("need_term_agreement"));
            //    return false;
            //}


            data = {
                email: this.$inputEmail.val(),
                password: this.$inputPasswd.val(),
                first_name: this.$inputFirstName.val(),
                last_name: this.$inputLastName.val()
            };


            $sp.AccountManager.signup(data, function(result) {
                if (result.success && result.account) {
                    $sp.router.navigate("", { trigger: true });
                } else {
                    that.showError(result.errorMsg || "failed to signup");
                }
            });
            return false;
        },

        render: function() {
            this.$el.html(this._template(this._context));
            this.$inputEmail = $("#input-email", this.$el);
            this.$inputPasswd = $("#input-passwd", this.$el);
            this.$inputFirstName = $("#input-firstname", this.$el);
            this.$inputLastName = $("#input-lastname", this.$el);
            this.$inputAgreeTerm = $("#input-agree-term", this.$el);
        },

        showError: function(error) {
            $sp.notify(error, "error");
        }
    });

    views.CompressingProgressView = MinimizableModalView.extend({
        modalTmpl: "#tmpl-modal-compressing-list",
        minibarTmpl: "#tmpl-minibar-compressing-list",
        itemTmpl: "#tmpl-compressing-item",
        initialize: function(options) {
            MinimizableModalView.prototype.initialize.call(this, options);
            _.bindAll(this, "addJob");

            this.$header = $(".header", this.$minibar);
            this.$items = $(".items", this.$modal);
            this._itemTmpl = _.template($(this.itemTmpl).html());

            this._compressingFilesCount = 0;

            this.$modal
                .on("click", ".btn-retry", function() {
                    var $item = $(this).closest(".item"),
                        key = $item.data("originalKey");
                    $sp.FileTransfer.restartCompressWorker(unescape(key));
                })
                .on("click", ".btn-cancel", function() {
                    var $item = $(this).closest(".item"),
                        key = $item.data("key");
                    $sp.FileTransfer.cancelCompressWorker(unescape(key));
                });

        },
        getItemElById: function(id) {
            return $(".item[data-key='" + escape(id) + "']", this.$items);
        },
        getCompressingFilesCount: function() {
            var selector =".item",
                excludeKeys = ['cancelled', 'finished', 'error'];
            selector += ":not(";
            selector += $.map(excludeKeys, function(key) {
                            return "[data-key='" + key + "']";
                        }).join(",");
            selector += ")";
            return $(selector, this.$items).size();
        },
        addJob: function(job) {
            var that = this,
                handler,
                filter = job.filter,
                device = job.device,
                paths = job.paths;

            handler = {
                onInit: function(id, paths) {
                    var $el, $progressbar,
                        title, idx, path;

                    path = paths[0];
                    if (path.endsWith("/")) {
                        path = path.substring(0, path.length - 1);
                    }
                    idx = path.lastIndexOf("/");
                    title = path.substring(idx + 1);

                    if (paths.length > 1) {
                        title += " and " + (paths.length - 1) + " files";
                    }

                    that.$items.append(that._itemTmpl({
                        key: id,
                        title: title,
                        device: device.toJSON(),
                        status: $sp.string("compress_status__initialized")
                    }));
                    $el = that.getItemElById(id);
                    $el.find(".peer .name").ellipsis({ width: 128, ellipsis: "end" });
                    $el.find(".filename").ellipsis({ width: 284, ellipsis: "middle" });
                    $el.find(".status-inverse-wrapper").css("width", 100 + "%");

                    $progressbar = $(".progress", $el);
                    $progressbar.progressbar({value: false});

                    that.updateView();

                    if (!that.isVisible()) {
                        that.show();
                    }

                    // push event
                    $sp.DeviceEventManager
                        .pushCompressRequestEvent(device.get("deviceId"), paths);
                },
                onCanceling: function(id) {
                    var $el = that.getItemElById(id),
                        $progressbar;

                    $el.find(".status").html(
                        $sp.string("compress_status__canceling"));
                    $el.find(".status-inverse-wrapper").css("width", 100 + "%");
                    $el.addClass("canceling");
                    $progressbar = $(".progress", $el)
                    $progressbar.progressbar({value: false});
                },
                onCancelled: function(id) {
                    var $el = that.getItemElById(id),
                        $statusWrapper,
                        resultHtml = "<i class='glyphicons glyphicons-remove-2'></i>" +
                            "<span class='text'>" +
                            $sp.string("compress_status__cancelled") +
                            "</span>";

                    $statusWrapper = $el.find(".status-wrapper");
                    $statusWrapper.html(resultHtml);

                    $el.removeClass("canceling");
                    $el.addClass("cancelled");
                    $el.attr("data-key", "cancelled")
                        .attr("data-original-key", id);

                    that.updateView();
                    if (!that._isMinimized) {
                        if (that.getCompressingFilesCount() === 0) {
                            that.hide();
                        }
                        return;
                    }
                    setTimeout(function() {
                        if (that.getCompressingFilesCount() === 0) {
                            that.hide();
                        }
                    }, 2000);
                },
                onFailed: function(id, paths, msg) {
                    var $el = that.getItemElById(id),
                        $statusWrapper,
                        resultHtml = "<i class='glyphicons glyphicons-remove-2'></i>" +
                            "<span class='text' title='" + msg + "'>" +
                            $sp.string("compress_status__failed") +
                            "</span>";

                    $statusWrapper = $el.find(".status-wrapper");
                    $statusWrapper.html(resultHtml);

                    $el.addClass("failed");
                    $el.attr("data-key", "error")
                        .attr("data-original-key", id);

                    that.updateView();
                },
                onFinished: function(id, paths, path) {
                    var $el = that.getItemElById(id),
                        $statusWrapper,
                        resultHtml = "<i class='glyphicons glyphicons-ok-2'></i>" +
                            "<span class='text'>" +
                            $sp.string("compress_status__done") +
                            "</span>" +
                            "<a class='download'>" +
                            $sp.string("redownload_compressed_file") +
                            "</a>";

                    $statusWrapper = $el.find(".status-wrapper");
                    $statusWrapper.html(resultHtml);
                    $el.addClass("finished");
                    $el.attr("data-key", "finished");
                    $el.on("click", ".download", function() {
                        var doDownload;
                        doDownload = function() {
                            $sp.FileTransfer
                                .registerReceiveFileWorker({
                                        deviceId: device.get("deviceId"),
                                        path: path
                                    });
                        };
                        if (device.get("connectionState") === "charged") {
                            $sp.confirm({
                                title: $sp.string(
                                    "warning_data_charge__device_charged_connected__title"),
                                text: $sp.string(
                                    "warning_data_charge__device_charged_connected__text"),
                                confirmText: $sp.string("proceed_download"),
                                closeText: $sp.string("cancel")
                            }, function(confirmed) {
                                if (confirmed) {
                                    doDownload();
                                }
                            });
                        } else {
                            doDownload();
                        }
                    });

                    that.updateView();

                    if (!that._isMinimized) {
                        if (that.getCompressingFilesCount() === 0) {
                            that.hide();
                        }
                        return;
                    }
                    setTimeout(function() {
                        if (that.getCompressingFilesCount() === 0) {
                            that.hide();
                        }
                    }, 2000);

                    // push event
                    $sp.DeviceEventManager
                        .pushCompressCompleteEvent(device.get("deviceId"), paths, path);
                },
                publishProgress: function(id, paths, percentage) {
                    var $el = that.getItemElById(id),
                        $progressbar;

                    if ($el.hasClass("canceling")) {
                        return;
                    }
                    $el.find(".status").html(
                        $sp.string("compress_status__progress", percentage));
                    $el.find(".status-inverse-wrapper").css("width", percentage + "%");
                    $progressbar = $(".progress", $el)
                    $progressbar.progressbar({
                        value: percentage
                    });
                }
            };
            $sp.FileTransfer.registerReceiveCompressedFileWorker(
                    device.get("deviceId"), paths, filter, handler);
        },
        updateView: function() {
            var statusText,
                compressingFilesCount = this.getCompressingFilesCount();

            if (compressingFilesCount === 0) {
                this.$minibar.addClass("finished");
                this.$header.find(".status").html(
                    $sp.string("compress_header__compress_finished"));
                this.$header.find(".icon").hide();
                this.$header.find(".detail-btn").hide();
            } else {
                this.$minibar.removeClass("finished");
                statusText = $sp.string_n("compress_header__compressing", compressingFilesCount);
                this.$header.find(".status").html(statusText);
                this.$header.find(".icon").show();
                this.$header.find(".detail-btn").show();
            }
        },
        onHidden: function() {
            var selector = ".item[data-key='finished'], .item[data-key='cancelled']",
                $finishedJobs = $(selector, this.$items);
            $finishedJobs.remove();
        }
    });

    views.SendFilesView = MinimizableModalView.extend({
        modalTmpl: "#tmpl-modal-sending-list",
        minibarTmpl: "#tmpl-minibar-sending-list",
        sendingFileTmpl: "#tmpl-sending-item",
        initialize: function(options) {
            var that = this;

            MinimizableModalView.prototype.initialize.call(this, options);
            _.bindAll(this, "addJob");

            this.$header = $(".header", this.$minibar);
            this.$items = $(".items", this.$modal);
            this._itemTmpl = _.template($(this.sendingFileTmpl).html());

            this._sendingFilesCount = 0;
            this._failedFilesCount = 0;

            this.$modal
                .on("click", ".btn-retry", function() {
                    var $sendingFile = $(this).closest(".item"),
                        key = $sendingFile.data("originalKey");
                    $sp.FileTransfer.restartSendFileWorker(unescape(key));
                })
                .on("click", ".btn-cancel", function() {
                    var $sendingFile = $(this).closest(".item"),
                        key = $sendingFile.data("key");
                    $sp.FileTransfer.cancelSendFileWorker(unescape(key));
                })
                .on("click", ".path", function() {
                    var $sendingFile = $(this).closest(".item"),
                        deviceId = $sendingFile.data("deviceId"),
                        encodedDeviceId = $sp.Utils.encodeDeviceId(deviceId),
                        path = $(this).data("path");
                    that.minimize();
                    $sp.router.navigate(
                        "/device/" + encodedDeviceId + "/directory" + path, { trigger: true});
                });
        },
        getItemElByKey: function(key) {
            return $(".item[data-key='" + escape(key) + "']", this.$items);
        },
        getFailedItemElByKey: function(key) {
            return $(".item[data-original-key='" + escape(key) + "']", this.$items);
        },
        getSendingFilesCount: function() {
            var selector =".item",
                excludeKeys = ['cancelled', 'finished', 'error'];
            selector += ":not(";
            selector += $.map(excludeKeys, function(key) {
                            return "[data-key='" + key + "']";
                        }).join(",");
            selector += ")";
            return $(selector, this.$items).size();
        },
        getFailedFilesCount: function() {
            var selector =".item[data-key='error']";
            return $(selector).size();
        },
        addJob: function(job) {
            var registerSuccess,
                file = job.file,
                path = job.path,
                deviceId = job.peerId,
                toParentPath = job.toParentPath,
                fromParentPath = job.fromParentPath,
                key = deviceId + ":" + path,
                $fileRow,
                peerDevice = $sp.DeviceManager.getCachedDeviceWithId(deviceId),
                that = this,
                progressbar,
                $newEl;

            if (!peerDevice) {
                $sp.DeviceManager.getDeviceWithId(deviceId, function() {
                    addJob.call(that, job);
                });
                return;
            }

            this._sendingFilesCount += 1;
            this.updateView();

            $newEl = $(this._itemTmpl({
                file: file,
                key: key,
                path: toParentPath,
                peer: peerDevice.toJSON(),
                status: $sp.string("transfer_status__loading_file")
            }));

            this.$items.append($newEl);
            $newEl.find(".peer .name").ellipsis({ width: 105, ellipsis: "end" });
            $newEl.find(".filename").ellipsis({ width: 160, ellipsis: "middle" });
            $newEl.find(".path").ellipsis({ width: 140, ellipsis: "start" });

            registerSuccess = $sp.FileTransfer.registerSendFileWorker({
                file: file,
                deviceId: deviceId,
                deviceType: peerDevice.get("deviceType"),
                toParentPath: toParentPath,
                fromParentPath: fromParentPath,
                context: this,
                handler: {
                    onWait: function(key) {
                        var $el = this.getItemElByKey(key);
                        if ($el) {
                            $el.find(".status").html(
                                $sp.string("transfer_status__wait"));
                        }
                    },
                    onInitialized: function(key) {
                        var $el = this.getItemElByKey(key);
                        if ($el) {
                            $el.find(".status").html(
                                $sp.string("transfer_status__prepare"));
                        }
                    },
                    onProgress: function(key, percentage) {
                        var $el = this.getItemElByKey(key),
                            progressbar;

                        if ($el) {
                            $el.find(".status").html(
                                $sp.string("transfer_status__progress", percentage));
                            $el.find(".status-inverse-wrapper").css("width", percentage + "%");
                            progressbar = $(".progress", $el)
                            progressbar.progressbar({
                                value: percentage
                            });
                        }
                    },
                    onFinished: function(key) {
                        var $el = this.getItemElByKey(key),
                            that = this,
                            $statusWrapper,
                            resultHtml = "<i class='glyphicons glyphicons-ok-2'></i>" +
                                "<span class='text'>" +
                                $sp.string("transfer_status__sent") +
                                "</span>";
                        $statusWrapper = $el.find(".status-wrapper");
                        $statusWrapper.html(resultHtml);
                        $el.addClass("finished");
                        $el.attr("data-key", "finished");
                        this._sendingFilesCount -= 1;
                        this.updateView();
                        if (!this._isMinimized) {
                            if (that.getSendingFilesCount() === 0 &&
                                    that.getFailedFilesCount() === 0) {
                                this.hide();
                            }
                            return;
                        }
                        setTimeout(function() {
                            if (that.getSendingFilesCount() === 0 &&
                                    that.getFailedFilesCount() === 0) {
                                that.hide();
                            }
                        }, 2000);

                        // push event
                        $sp.DeviceEventManager.pushSendEvent(deviceId, path);
                    },
                    onCancelled: function(key) {
                        var $el = this.getItemElByKey(key),
                            that = this,
                            $statusWrapper,
                            resultHtml = "<i class='glyphicons glyphicons-remove-2'></i>" +
                                "<span class='text'>" +
                                $sp.string("transfer_status__cancelled") +
                                "</span>";

                        $statusWrapper = $el.find(".status-wrapper");
                        $statusWrapper.html(resultHtml);

                        $el.addClass("cancelled");
                        $el.attr("data-key", "cancelled");

                        this.updateView();
                        if (!this._isMinimized) {
                            if (that.getSendingFilesCount() === 0 &&
                                    that.getFailedFilesCount() === 0) {
                                this.hide();
                            }
                            return;
                        }
                        setTimeout(function() {
                            if (that.getSendingFilesCount() === 0 &&
                                    that.getFailedFilesCount() === 0) {
                                that.hide();
                            }
                        }, 2000);
                    },
                    onFailed: function(key, msg) {
                        var $el = this.getItemElByKey(key),
                            $btnRetry = $("<a title='retry' class='btn-retry'></a>");
                            that = this;

                        $el.attr("data-key", "error");
                        $el.attr("data-original-key", escape(key));
                        $el.addClass("failed");
                        $el.find(".status").html(msg);

                        $btnRetry.html("<i class='glyphicons glyphicons-refresh'></i>");
                        $el.find(".status-wrapper .btn-wrapper").html($btnRetry);
                        this.updateView();
                        return;
                    },
                    removeFailedTask: function(key) {
                        var $el = this.getFailedItemElByKey(key),
                            that = this;

                        this.updateView();
                        $el.fadeOut(function() {
                            $(this).remove();
                            if (that.getSendingFilesCount() === 0 &&
                                    that.getFailedFilesCount() === 0) {
                                that.hide();
                            }
                        });
                    },
                    onRestarted: function(key) {
                        var $el = this.getFailedItemElByKey(key);
                        $el.attr("data-key", $el.attr("data-original-key"));
                        $el.removeAttr("data-original-key");
                        $(".btn-retry", $el).remove();
                        $el.removeClass("failed");
                        this.updateView();
                    }
                }
            });
            if (registerSuccess) {
                this.updateView();
                this.show();
                progressbar = $(".progress", $newEl);
                $newEl.find(".status-inverse-wrapper").css("width", 100 + "%");
                progressbar.progressbar({value: false});

                return true;
            }
            return false;
        },
        updateView: function() {
            var statusText,
                sendingFilesCount = this.getSendingFilesCount(),
                failedFilesCount = this.getFailedFilesCount();

            if (sendingFilesCount === 0) {
                statusText = $sp.string("transfer_header__sent_finished");
                if (failedFilesCount > 0) {
                    statusText += " (";
                    statusText += $sp.string_n("transfer_header__failed", failedFilesCount);
                    statusText += ")";
                }
                this.$header.find(".status").html(statusText);
                this.$header.find(".icon").hide();
                this.$header.find(".detail-btn").show();
                this.$minibar.addClass("finished");
            } else {
                statusText = $sp.string_n("transfer_header__sending", sendingFilesCount);
                if (failedFilesCount > 0) {
                    statusText += " (";
                    statusText += $sp.string_n("transfer_header__failed", failedFilesCount);
                    statusText += ")";
                }
                this.$header.find(".status").html(statusText);
                this.$header.find(".icon").show();
                this.$header.find(".detail-btn").show();
                this.$minibar.removeClass("finished");
            }
        },
        onHidden: function() {
            var selector = ".item[data-key='finished'], .item[data-key='cancelled']",
                $finishedJobs = $(selector, this.$items);
            $finishedJobs.remove();
        }
    });

    views.VideoView = CollectionPageView.extend({
        menuType: "videos",
        getContentView: function(options) {
            return new VideoListView(options);
        }
    });

    views.DocumentView = CollectionPageView.extend({
        menuType: "documents",
        getContentView: function(options) {
            return new DocumentListView(options);
        }
    });

    views.MusicView = CollectionPageView.extend({
        menuType: "music",
        getContentView: function(options) {
            return new MusicListView(options);
        }
    });

    views.PhotoAlbumView = CollectionPageView.extend({
        menuType: "photo-album",
        getContentView: function(options) {
            return new AlbumView(options);
        },
        loadDevice: function(device, path, callback) {
            var menuUri,
                encodedDeviceId = $sp.Utils.encodeDeviceId(device.get("deviceId"));
            if (!path) {
                menuUri = "/device/" + encodedDeviceId + "/photos";
                $sp.router.navigate(menuUri, { trigger: true, replace: true });
                return;
            }
            CollectionPageView.prototype.loadDevice
                .call(this, device, path, callback);
        },
        getContextMenuView: function(options) {
            options.itemSelector = ".photo";
            return new FileContextMenuView(options);
        }
    });

    views.PhotoAlbumsView = CollectionPageView.extend({
        menuType: "photos",
        getContentView: function(options) {
            return new AlbumListView(options);
        },
        getContextMenuView: function(options) {
            options.itemSelector = ".album";
            return new AlbumContextMenuView(options);
        }
    });

    views.DirectoryView = CollectionPageView.extend({
        menuType: "directory",
        getContentView: function(options) {
            return new DirListView(options);
        }
    });

    views.IndexNonAuthView = SyncportPageView.extend({
        template: "#tmpl-page-nonauth-index",
        tagName: "div",
        id: "page-nonauth-index",

        initialize: function(options) {
            SyncportPageView.prototype.initialize.call(this, options);
            _.bindAll(this, "adjustMainPage");

            $(pageFrame).html(this.el);
            this.render();
            this.$betaRegiEmail = $("#input-beta-register-email", this.$el);
            this.$betaRegiBtn = $(".btn-beta-register", this.$el);
            this.$videoWindow = $(".video-wrapper", this.$el);
            this.$videoEl = $("#demo-video-holder", this.$el);
            this.$filter = $("#filter");
            $(window).on("resize", this.adjustMainPage);
            this.$el.find(".main-page img").load(this.adjustMainPage);
        },

        events: function() {
            var events = SyncportPageView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend({}, events, {
                "click .link-signin": "navSignin",
                "click .btn-signup": "navSignup",
                "click .btn-beta-register": "doRegister",
                "click .btn-show-video": "showVideo",
                "click .btn-show-demo-video": "showVideo",
                "click .btn-close-video": "hideVideo"
            });
        },

        navSignin: function() {
            $sp.router.navigate('signin', { trigger: true });
        },

        navSignup: function() {
            $sp.router.navigate('signup', { trigger: true });
        },

        doRegister: function(e) {
            var email = this.$betaRegiEmail.val(),
                that = this,
                $target = $(e.currentTarget);
            if ($target.attr("disabled")) {
                return;
            }
            if (!email || email === "") {
                this.$betaRegiEmail.notify({
                    msg: $sp.string("required_field")
                }, "error");
                return;
            }
            $target.attr("disabled", "true");
            $.ajax({
                url: $sp.Remote.api.REGISTER_TESTER,
                data: {
                    email: email
                },
                dataType: "json",
                type:"POST"
            }).done(function(res) {
                if (res.success) {
                    $sp.notify($sp.string("register_beta_response"), "success");
                    that.$betaRegiEmail.val("");
                } else {
                    $sp.notify(res.msg || "undefined error", "error");
                }
            }).always(function(res) {
                $target.removeAttr("disabled");
            });
        },

        initVideo: function(videoId) {
            var tag,
                firstScriptTag,
                that = this;
            if (!this._videoId || (this._videoId !== videoId)) {
                this._videoInitialized = false;
                this._videoId = videoId;
            }
            if ($(window).width() < 800) {
                return false;
            }
            if (this._videoInitialized) {
                return true;
            }

            if (!this._videoScriptLoading && !this._videoScriptLoaded) {

                // load google iframe api script
                tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                // init player
                window.onYouTubeIframeAPIReady = initPlayer;
                this._videoScriptLoading = true;
                return false;
            } else if (this._videoScriptLoading && !this._videoScriptLoaded) {
                window.onYouTubeIframeAPIReady = initPlayer;
                return false;
            } else {
                this._videoInitialized = true;
                this._videoPlayer.loadVideoById(videoId);
                this._videoPlayer.playVideo();
                return false;
            }


            function initPlayer() {
                that._videoScriptLoading = false;
                that._videoScriptLoaded = true;
                that._videoPlayer = new YT.Player("demo-video", {
                    height: '500',
                    width: '750',
                    videoId: videoId,
                    events: {
                        'onReady': function(e) {
                            that._videoInitialized = true;
                            if (that.$videoWindow.is(":visible")) {
                                e.target.playVideo && e.target.playVideo();
                            }
                        },
                        'onStateChange':function(e) {
                            if (e.data === 1 && !that.$videoEl.is(":visible")) {
                                that.$videoEl.show();
                            }
                            if (e.data === 0) {
                                that.hideVideo();
                            }
                        }
                    }
                });
            }
        },
        disableScroll: function() {
            $("body").addClass("prevent-scroll");
        },
        enableScroll: function() {
            $("body").removeClass("prevent-scroll");
        },

        showVideo: function(e) {
            var that = this,
                url,
                videoId;
            if ($(e.currentTarget).hasClass("btn-show-video")) {
                videoId = 'H7CoeMMF-zg';
                $.cookie("saw_video", true);
            } else {
                videoId = 'SjVdWPhhrh0';
            }
            $(".ic-play").addClass("no-animate");
            if ($(window).width() < 800) {
                if (/iphone|ipod|ipad/i.test(navigator.userAgent.toLowerCase())) {
                    url = "http://m.youtube.com/watch?v=";
                } else {
                    url = "http://www.youtube.com/watch?v=";
                }
                window.open(
                    url + videoId,
                    "_blank"
                );
                return;
            }
            this.filterHandler = function(e) {
                that.hideVideo();
            }
            this.$filter.addClass("dark-filter");
            this.$filter.show().one("click", this.filterHandler);
            this.disableScroll();
            this.$videoWindow.show();
            if (this.initVideo(videoId)) {
                this._videoPlayer.seekTo && this._videoPlayer.seekTo(0, true);
                this._videoPlayer.playVideo && this._videoPlayer.playVideo();
            }
        },

        hideVideo: function() {
            if (this._videoInitialized &&
                this._videoPlayer.getPlayerState() === 1) {
                this._videoPlayer.pauseVideo();
            }
            this.$videoWindow.hide();
            this.$filter.removeClass("dark-filter");
            this.$filter.click();
            this.$filter.hide();
            this.enableScroll();
        },

        adjustMainPage: function() {
            var $mainPage = $(".main-page", this.$el),
                $mainPageContents = $(".main-page .contents", this.$el),
                pageContentsHeight,
                winHeight;

            winHeight = $(window).height();
            pageContentsHeight = $mainPageContents.outerHeight();
            if (pageContentsHeight > winHeight - 80) {
                $mainPage.css({
                    height: "auto"
                });
                $mainPageContents.css({
                    position: "static",
                    top: "auto",
                    marginTop: "0"
                });
            } else {
                $mainPage.css({
                    height: winHeight
                });
                $mainPageContents.css({
                    position: "absolute",
                    top: "50%",
                    marginTop: -1 * pageContentsHeight / 2
                });
            }
        },

        render: function() {
            var that = this;
            this.$el.html(this._template(this._context));
            if ($.cookie("saw_video")) {
                $(".ic-play").addClass("no-animate");
            }
            setTimeout(function() {
                that.adjustMainPage();
            }, 0);
        }
    });

    views.AccountSettingsView = SyncportAuthPageView.extend({
        template: "#tmpl-page-account-settings",
        tagName: "div",
        id: "page-account-settings",
        initialize: function(options) {
            SyncportAuthPageView.prototype.initialize.call(this, options);
            $(pageFrame).html(this.el);
            this.render();
        },

        render: function() {
            this.$el.html(this._template(this._context ));
        }
    });

    views.DownloadView = SyncportAuthPageView.extend({
        template: "#tmpl-page-download",
        tagName: "div",
        id: "page-download",
        initialize: function(options) {
            _.bindAll(this, "adjustWindowHeight");
            SyncportAuthPageView.prototype.initialize.call(this, options);
            $(pageFrame).html(this.el);
            this.render();
        },

        render: function() {
            this.$el.html(this._template(this._context ));

            $(window).on("resize", this.adjustWindowHeight);
            this.adjustWindowHeight();
        },
        adjustWindowHeight: function() {
            var winHeight,
                frameHeight,
                headerHeight,
                $pageContents,
                topPosition;

            $pageContents = $(".page-contents", this.$el);
            frameHeight = $pageContents.outerHeight();
            headerHeight = $("header").outerHeight();
            winHeight = $(window).height();

            topPosition = (winHeight - frameHeight) / 2 - headerHeight;
            if (topPosition > headerHeight + 40) {
                $pageContents.css({
                    position: "absolute",
                    top:topPosition + "px"
                });
            } else {
                $pageContents.css({
                    position: "static",
                    top:"0"
                });
            }
            $("#page-download").css("minHeight", winHeight);
        },
        remove: function() {
            SyncportAuthPageView.prototype.remove.call(this);
            $(window).off("resize", this.adjustWindowHeight);
        }

    });

    views.HowtouseView = SyncportAuthPageView.extend({
        template: "#tmpl-page-how-to-use",
        tagName: "div",
        id: "page-how-to-use",
        initialize: function(options) {
            SyncportAuthPageView.prototype.initialize.call(this, options);
            $(pageFrame).html(this.el);
            this.render();
        },

        render: function() {
            this.$el.html(this._template(this._context ));
        }
    });

    views.ResetPasswdView = SyncportAuthPageView.extend({
        template: "#tmpl-page-reset-passwd",
        sentTmpl: "#tmpl-page-sent-reset-mail",
        tagName: "div",
        id: "page-reset-passwd",
        initialize: function(options) {
            this._sentTmpl = _.template($(this.sentTmpl).html());
            SyncportAuthPageView.prototype.initialize.call(this, options);
            $(pageFrame).html(this.el);
            this.render();
        },

        events: function() {
            var events = SyncportAuthPageView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend ({}, events, {
                "click .btn-reset-passwd" : "doReset",
                "submit #form-reset-passwd" : "doReset"
            });
        },

        doReset: function(e) {
            var that = this,
                email = this.$email.val(),
                $submitBtn = $(".btn-reset-passwd", this.$el),
                notifyOptions = {
                        showDuration: 0,
                        hideDuration: 0,
                        elementPosition: "right middle",
                        autoHide: false
                    };
            if ($submitBtn.attr("disabled")) {
                return;
            }

            if (!email) {
                this.$email.notify({
                    msg: $sp.string("required_field")
                }, notifyOptions, "error");
                return;
            }

            $submitBtn.attr("disabled", "true");
            $submitBtn.addClass("progress");
            $(".text", $submitBtn).html($sp.string("sending_reset_mail"));

            $sp.AccountManager.resetPasswd(email, function(result) {
                if (result.success) {
                    that.renderSent(email);
                } else {
                    that.$email.notify({
                        msg: result.errorMsg || "Failed to reset password"
                    }, notifyOptions, "error");
                    $submitBtn.removeAttr("disabled");
                    $submitBtn.removeClass("progress");
                    $(".text", $submitBtn).html($sp.string("reset_password_btn"));
                }
            });
            e.preventDefault();
            return false;
        },

        render: function() {
            this.$el.html(this._template(this._context ));
            this.$email = $("#input-email", this.$el);
        },

        renderSent: function(email) {
            var context = $.extend({}, this._context, { email:email });
            this.$el.html(this._sentTmpl(context));
        },

        showError: function(error) {
            var $promptHolder = $(".error-prompt", this.$el);
            $promptHolder.html(error);
            $promptHolder.slideDown();
        }
    });

    views.ChangePasswdView = SyncportAuthPageView.extend({
        template: "#tmpl-page-change-passwd",
        tagName: "div",
        id: "page-change-passwd",
        initialize: function(options) {
            SyncportAuthPageView.prototype.initialize.call(this, options);
            $(pageFrame).html(this.el);
            this.render();
        },

        events: function() {
            var events = SyncportAuthPageView.prototype.events;
            if (_.isFunction(events)) {
                events = events.call(this);
            }
            return $.extend ({}, events, {
                "click .btn-change-passwd" : "doChange",
                "submit #form-change-passwd" : "doChange"
            });
        },

        doChange: function(e) {
            var that = this,
                newPasswd = this.$newPasswd.val(),
                newPasswdConfirm = this.$newPasswdConfirm.val(),
                notifyOptions = {
                        showDuration: 0,
                        hideDuration: 0,
                        elementPosition: "right middle",
                        autoHide: false
                    },
                isValid = true,
                isPasswdConfirmValid = true;

            if (!newPasswd) {
                this.$newPasswd.notify({
                    msg: $sp.string("required_field")
                }, notifyOptions, "error");
                isValid = false;
            } else {
                this.$newPasswd.parents(".input-wrapper")
                    .find(".notifyjs-bootstrap-base").trigger("notify-hide");
            }

            if (!newPasswdConfirm) {
                this.$newPasswdConfirm.notify({
                    msg: $sp.string("required_field")
                }, notifyOptions, "error");
                isValid = false;
                isPasswdConfirmValid = false;
            } else {
                this.$newPasswdConfirm.parents(".input-wrapper")
                    .find(".notifyjs-bootstrap-base").trigger("notify-hide");
            }

            if (isPasswdConfirmValid && newPasswd !== newPasswdConfirm) {
                this.$newPasswdConfirm.notify({
                    msg: $sp.string("password_confirm_fail")
                }, notifyOptions, "error");
                isValid = false;
            } else if (isPasswdConfirmValid) {
                this.$newPasswdConfirm.parents(".input-wrapper")
                    .find(".notifyjs-bootstrap-base").trigger("notify-hide");
            }

            if (!isValid) {
                return;
            }

            $sp.AccountManager.changePasswd(newPasswd, function(result) {
                if (result.success) {
                    $sp.alert(
                        "<i class='glyphicons glyphicons-ok-2'></i>" + $sp.string("applied"),
                        function() {
                            $sp.router.navigate('', { trigger: true });
                        });
                } else {
                    that.showError(result.errorMsg || "Failed to change password");
                }
            });
            e.preventDefault();
            return false;
        },


        render: function() {
            var that = this,
                $inputSessionKey,
                sessionKey;

            this.$el.html(this._template(this._context ));
            this.$newPasswd = $("#input-new-passwd", this.$el);
            this.$newPasswdConfirm = $("#input-new-passwd-confirm", this.$el);

            // check has valid session key
            if (!$sp.context.account) {
                $inputSessionKey = $("#input-session-key", this.$el);
                sessionKey = $inputSessionKey.val();
                if (sessionKey ) {
                    $.cookie("sessionid", sessionKey,
                        { path: $sp.Remote.api.USER_SELF });
                    $.get($sp.Remote.api.USER_SELF)
                        .done(function(user) {
                            $(".page-holder", this.$el).removeClass("hidden");
                        })
                        .fail(function(res) {
                            that.showErrorPage(res ||
                                $sp.string("session-key-invalid"));
                            return;
                        });
                } else {
                    that.showErrorPage($sp.string("session-key-invalid"));
                }
            }
        },
        showErrorPage: function(error) {
            var brokenTmpl,
                context = $.extend({}, this._context, {
                    errorMsg: error
                });

            brokenTmpl = _.template($("#tmpl-page-broken").html());
            this.$el.html(brokenTmpl(context));
            this.$el.attr("id", "page-broken");
        },
        showError: function(error) {
            var $promptHolder = $(".error-prompt", this.$el);
            $promptHolder.html(error);
            $promptHolder.slideDown();
        }
    });

    views.BrokenView = SyncportAuthPageView.extend({
        template: "#tmpl-page-broken",
        tagName: "div",
        id: "page-broken",

        initialize: function(options) {
            SyncportAuthPageView.prototype.initialize.call(this, options);
            $(pageFrame).html(this.el);
            this.render(options.errorTitle, options.errorDesc);
        },

        render: function(title, msg) {
            var context = $.extend({}, this._context, {
                title: title || $sp.string("something_went_wrong"),
                msg: msg
            });

            this.$el.html(this._template(context));
        }
    });

    views.DeviceSettingsView = BaseDevicePageView.extend({
        menuType: "settings",
        renderSubViews: function() {
            this.subViews = this.subViews || {};
            this.subViews.contentView = new DeviceSettingsContentView(
                $.extend({}, this._options, {
                    el: "#device-content",
                    parentView: this
                }));

            BaseDevicePageView.prototype.renderSubViews.call(this);
        },
        loadDevice: function(device, path, callback) {
            this.subViews.contentView.loadDevice(device, path, callback);
        }
    });

    views.DeviceEventsView = BaseDevicePageView.extend({
        menuType: "events",
        renderSubViews: function() {
            this.subViews = this.subViews || {};
            this.subViews.contentView = new DeviceEventsContentView(
                $.extend({}, this._options, {
                    el: "#device-content",
                    parentView: this
                }));

            BaseDevicePageView.prototype.renderSubViews.call(this);
        },
        loadDevice: function(device, path, callback) {
            this.subViews.contentView.loadDevice(device, path, callback);
        }
    });

    views.AlertView = (function() {
        var AlertView = SyncportView.extend({
                template: "#tmpl-modal-alert",
                initialize: function(options) {
                    SyncportView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._text = options.text || "";
                    _.bindAll(this, "show", "hide");
                },
                show: function() {
                    var that = this,
                        $modal = $(this._template($.extend({}, this._context, {
                            text: this._text
                        })));
                    $("#modal").append($modal);
                    $modal.modal({ show: true, backdrop:"static"})
                        .on("hidden.bs.modal", function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    $("#modal-alert")
                        .on("hidden.bs.modal", function() {
                            callback && callback();
                        });
                    $("#modal-alert").modal("hide");
                },
                remove: function() {
                    SyncportView.prototype.remove.call(this);
                    $("#modal-alert").remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = $.proxy(function(text, callback) {
            if (view) {
                this.hide($.proxy(function() {
                    this.show(text, callback);
                }, this));
                return;
            }
            view = new AlertView({
                    text: text,
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = $.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    views.ConfirmView = (function() {
        var ConfirmView = SyncportView.extend({
                template: "#tmpl-modal-confirm",
                initialize: function(options) {
                    SyncportView.prototype.initialize.call(this, options);
                    this._onAlways = options.onAlways;
                    this._title = options.title || "";
                    this._text = options.text || "";
                    this._confirmText = options.confirmText || $sp.string("confirm");
                    this._closeText = options.closeText || $sp.string("close");
                    this._isConfirmed = false;
                    _.bindAll(this, "show", "hide");
                },
                show: function() {
                    var that = this,
                        $modal = $(this._template($.extend({}, this._context, {
                            title: this._title,
                            text: this._text,
                            closeText: this._closeText,
                            confirmText: this._confirmText
                        })));
                    $("#modal").append($modal);
                    $modal.modal({ show: true, backdrop:"static"})
                        .on("click", ".btn-confirm", function() {
                            that._isConfirmed = true;
                            that.hide();
                        })
                        .on("hidden.bs.modal", function() {
                            that._onAlways && that._onAlways(that._isConfirmed);
                        });
                },
                hide: function(callback) {
                    $("#modal-confirm")
                        .on("hidden.bs.modal", function() {
                            callback && callback();
                        });
                    $("#modal-confirm").modal("hide");
                },
                remove: function() {
                    SyncportView.prototype.remove.call(this);
                    $("#modal-confirm").remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = $.proxy(function(options, callback) {
            if (view) {
                this.hide($.proxy(function() {
                    this.show(options, callback);
                }, this));
                return;
            }
            view = new ConfirmView({
                    title: options.title,
                    text: options.text,
                    confirmText: options.confirmText,
                    closeText: options.closeText,
                    onAlways: function(confirmed) {
                        callback && callback(confirmed);
                        if (view) {
                            view.close ? view.close() : view.remove();
                            view = null;
                        }
                    }
                });
            view.show();
        }, subModule);

        subModule.hide = $.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module  = module || {};
    module.View = views;
    module.confirm = module.View.ConfirmView.show;
    module.alert = module.View.AlertView.show;
    return module;
}($sp));
