var $sp = (function(module) {
    module = module || {};

    function Account(params) {
        this.deviceId = params.deviceId;
        this.deviceType = params.deviceType;
        this.deviceName = params.deviceName;
        this.email = params.email;
        this.firstName = params.firstName;
        this.lastName = params.lastName;
    }

    module.AccountManager = (function(){
        var myAccount = null,
            manager;

        manager = {
            changePasswd: function(newPasswd, callback) {
                var that = this,
                    afterChangePasswd = function(email) {
                        that.signin.call(that, email, newPasswd, callback);
                    };

                $.get($sp.Remote.api.USER_SELF)
                .done(function(user) {
                    $.ajax({
                        url: $sp.Remote.api.USER_SELF,
                        data: {
                            password: newPasswd
                        },
                        dataType: "json",
                        type:"POST"
                    }).done(function(res, textStatus, jqXHR) {
                        afterChangePasswd(user.email);
                    })
                    .fail(function(res) {
                        callback && callback({
                            success: false,
                            errorMsg: res || "undefind"
                        });
                    });
                })
                .fail(function(res) {
                    callback && callback({
                        success: false,
                        errorMsg: res || "undefind"
                    });
                });
            },
            resetPasswd: function(email, callback) {
                $.ajax({
                    url: $sp.Remote.api.FORGOT,
                    data: {
                        email: email
                    },
                    type: "POST",
                    dataType: "json"
                }).done(function(res, textStatus, jqXHR) {
                    if (res.success) {
                        callback && callback({ success: true });
                    } else {
                        callback && callback({
                            success: false,
                            errorMsg: res.error
                        });
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    callback && callback({
                        success: false,
                        errorMsg: jqXHR.responseText
                    });
                });
            },
            signin: function(email, passwd, callback) {
                var data = {
                        email: email,
                        password: passwd,
                        device_name: "web",
                        device_type: "WEB",
                        device_key: navigator.userAgent
                    };

                $.ajax({
                    url: $sp.Remote.api.SIGNIN,
                    data: data,
                    type: "POST",
                    dataType: "json"
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    callback && callback({
                        success:false, 
                        errorMsg: jqXHR.responseText
                    });
                }).done(function(res, textStatus, jqXHR) {
                    var onGotAccount;
                    if (res.success) {
                        onGotAccount = function(account) {
                            $sp.authService.init(account, function() {
                                if (account) {
                                    $sp.authService.start();
                                    callback && callback({
                                        success: true, 
                                        account: account
                                    });
                                } else {
                                    callback && callback({
                                        success: false,
                                        errorMsg: undefined
                                    });
                                }
                            });
                        };
                        manager.getAccount(onGotAccount);
                    } else {
                        callback && callback({
                            success: false,
                            errorMsg: res.error
                        });
                    }
                });
            },
            signup: function(data, callback) {
                $.ajax({
                    url: $sp.Remote.api.SIGNUP,
                    data: data,
                    type: "POST",
                    dataType: "json"
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    callback && callback({
                        success: false,
                        errorMsg:jqXHR.responseText
                    });
                }).done(function(res, textStatus, jqXHR) {
                    if (res.success) {
                        manager.signin(data.email, data.password, callback);
                    } else {
                        callback && callback({
                            success: false,
                            errorMsg:res.error
                        });
                    }
                });
            },

            signout: function(callback) {
                $sp.authService.stop(function() {
                    $.ajax({
                        url: $sp.Remote.api.SIGNOUT,
                        dataType: "jsonp"
                    }).done(function(res, textStatus, jqXHR) {
                        myAccount = null;
                        delete $sp.context.account;
                        callback && callback();
                    });
                });
            },

            getAccount: function(callback, fromRemote) {
                if (!fromRemote && myAccount) {
                    callback && callback(myAccount);
                    return;
                }
                $.get($sp.Remote.api.USER_SELF)
                .done(function(user) {
                    $.get($sp.Remote.api.DEVICE_SELF)
                    .done(function(device) {
                        myAccount = new Account({
                            firstName: user.first_name,
                            lastName: user.last_name,
                            email: user.email,
                            deviceId: device.uid,
                            deviceName: device.name || "web",
                            deviceType: device.type || "WEB"
                        });
                        callback && callback(myAccount);

                    })
                    .fail(function(deviceRes, userRes) {
                        callback && callback();
                    });
                })
                .fail(function(deviceRes, userRes) {
                    callback && callback();
                });
                //$.when(
                //    $.get($sp.Remote.api.DEVICE_SELF),
                //    $.get($sp.Remote.api.USER_SELF))
                //.done(function(deviceRes, userRes) {
                //    var device = deviceRes[0],
                //        user = userRes[0];
                //    myAccount = new Account({
                //        firstName: user.first_name,
                //        lastName: user.last_name,
                //        email: user.email,
                //        deviceId: device.uid,
                //        deviceName: device.name,
                //        deviceType: device.type
                //    });
                //    callback && callback(myAccount);
                //})
                //.fail(function(deviceRes, userRes) {
                //    callback && callback();
                //});
            }
        };
        return manager;
    }());

    return module;
}($sp));
