var $sp = (function(module) {
    var Tour = {},
        defaultStepOptions = {
            showPrevButton: true
        },
        restoreStack = [];

    function generateStep(options, restoreStack) {
        var step = {},
            target = options.target,
            $target = $(target),
            restores = [];

        function onClose() {
            $.each(restores, function(idx, restore) {
                var $target = restore.$el;
                $target.css(restore.css);
            });
            $target.removeClass("tutorial-show");
        }

        restoreStack.push(restores);

        $.extend(step, defaultStepOptions, options, {
            target: target ? document.querySelector(target) : undefined,
            onShow: function() {
                restores.length = 0;
                $.each($target.parents(), function(idx, el) {
                    var $el = $(el),
                        position = $el.css("position"),
                        restore;
                    if (position === "fixed") {
                        restore = {
                            $el: $el,
                            css: {
                                "top": $el.css("top"),
                                "position": "fixed",
                                "zIndex": $el.css("z-index")
                            }
                        };
                        $el.css({
                            "position": "absolute",
                            "top": $el.offset().top,
                            "zIndex": "auto"
                        });
                        restores.push(restore);
                    }
                });
                $target.addClass("tutorial-show");
            },
            onNext: function() {
                onClose();
            },
            onPrev: function() {
                onClose();
            }
        });
        return step;
    }

    Tour.options = (function() {
        function onFilterClick() {
            hopscotch.endTour(true);
        }
        function onAfterFinish() {
            $.each(restoreStack, function(idx, restores) {
                $.each(restores, function(idx, restore) {
                    var $target = restore.$el;
                    $target.css(restore.css);
                });
            });
            $(".tutorial-show").removeClass("tutorial-show");
            $("#filter")
                .fadeOut(function() {
                    $(this).off("click", onFilterClick)
                        .removeClass("dark-filter");
                    $("body").removeClass("prevent-scroll");
                });
        }

        return {
            onStart: function() {
                $("body").addClass("prevent-scroll");
                $("#filter")
                    .addClass("dark-filter")
                    .on("click", onFilterClick)
                    .fadeIn();
            },
            onEnd: function() {
                onAfterFinish();
            },
            onClose: function() {
                onAfterFinish();
            },
            onError: function() {
                //onAfterFinish();
            },
            i18n: {
                nextBtn: $sp.string("btn_next"),
                prevBtn: $sp.string("btn_prev"),
                doneBtn: $sp.string("btn_done"),
                skipBtn: $sp.string("btn_skip"),
                closeTooltipBtn: $sp.string("btn_closetooltip")
            }
        };
    }());

    Tour.getDeviceTour = function() {
        return {
            id: "device-tour",
            steps: [
                generateStep({
                    title: $sp.string("tutorial__step1__title"),
                    content: $sp.string("tutorial__step1__desc"),
                    target: ".device-select-menu",
                    placement: "right",
                    fixedElement: true,
                    width:320
                }, restoreStack),
                generateStep({
                    title: $sp.string("tutorial__step2__title"),
                    content: $sp.string("tutorial__step2__desc"),
                    target: "#device-menus",
                    placement: "right",
                    fixedElement: true,
                    width:300
                }, restoreStack),
                generateStep({
                    title: $sp.string("tutorial__step3__title"),
                    content: $sp.string("tutorial__step3__desc"),
                    target: ".scroll-container .item, .scroll-container .photo," +
                        ".scroll-container .album-wrapper .album",
                    placement: "bottom",
                    width: 460
                }, restoreStack),
                generateStep({
                    title: $sp.string("tutorial__step4__title"),
                    content: $sp.string("tutorial__step4__desc"),
                    target: ".actions .action-show-uploader",
                    placement: "left",
                    fixedElement: true,
                    width: 360
                }, restoreStack)
            ]
        };
    };


    module = module || {};
    module.Tour = Tour;
    return module;
}($sp));

$(function() {
    hopscotch.configure($sp.Tour.options);
});
