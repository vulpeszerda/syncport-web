var $sp = (function(module) {
    var CONCURRENT_WORKER_MAX = 5,
        CONCURRENT_WINDOW_WORKER_MAX = 1,
        WEB_P2P_PORT = 61001,
        _sendFileWorkers = {},
        _sendingWorkers = {},
        _sendWaitingWorkerQueue = [],
        _receiveFileWorkers = {},
        _receiveCompressedFileWorker = {},
        _receivedFileWorkers = {},
        _peerProtocolCacheMap = {},
        _headSendListeners = [];

    module.intercom = module.intercom || Intercom.getInstance();
    module.intercom.emit("protocolCacheMapUpdate", { type: "init" });
    module.intercom.on("protocolCacheMapUpdate", function(data) {
        if (data.type === "init") {
            $.each(_peerProtocolCacheMap, function(key, val) {
                delete _peerProtocolCacheMap[key];
            });
        } else if (data.type === "put") {
            if (!_peerProtocolCacheMap[data.deviceId]) {
                _peerProtocolCacheMap[data.deviceId] = {};
            }
            if (data.key) {
                _peerProtocolCacheMap[data.deviceId] = {};
                _peerProtocolCacheMap[data.deviceId][data.key] = data.protocol;
            } else if (_peerProtocolCacheMap[data.deviceId]) {
                $.each(_peerProtocolCacheMap[data.deviceId], function(key, val) {
                    _peerProtocolCacheMap[data.deviceId][key] = data.protocol;
                });
            }
        }
    });

    function CompressedFileReceiveWorker(options) {
        var that = this;

        this.deviceId = options.deviceId;
        this.paths = options.paths;
        this.progress = 0;
        this.id = options.id;
        this.postInit = options.postInit;
        this.postCancel = options.postCancel;
        this.onCancelled = options.onCancelled;
        this.publishProgress = options.publishProgress;
        this.postFinished = options.postFinished;
        this.postFailed = options.postFailed;
        this.filter = options.filter;

        this.init = function() {
            var that = this;

            $($sp.Polling).on("tapingProgress." + this.id, this.progressUpdate);

            this.request = $.ajax({
                url: $sp.Remote.delta.PUSH,
                data: JSON.stringify({
                    delta: {
                        type: "remote_taping",
                        receiver: this.deviceId,
                        paths: this.paths,
                        id: this.id,
                        filter: this.filter
                    }
                }),
                dataType:"json",
                type:"POST",
                contentType:"application/json"
            }).done(function(res) {
                that.postInit && that.postInit(res);
            }).fail(function(jqXHR, textStatus, errorThron) {
                var text = jqXHR.responseText;
                text = text.replace(/<(?:.|\n)*?>/gm, '');
                $($sp.Polling).off("tapingProgress." + this.id);
                that.postFailed && that.postFailed(text);
            }).always(function() {
                delete that.request;
            });
        };

        this.cancel = function() {
            this.request = $.ajax({
                url: $sp.Remote.delta.PUSH,
                data: JSON.stringify({
                    delta: {
                        type: "cancel_taping",
                        receiver: this.deviceId,
                        id: this.id
                    }
                }),
                dataType:"json",
                type:"POST",
                contentType:"application/json"
            }).done(function(res) {
                that.postCancel && that.postCancel(res);
            }).fail(function(jqXHR, textStatus, errorThron) {
                var text = jqXHR.responseText;
                text = text.replace(/<(?:.|\n)*?>/gm, '');
                that.postFailed && that.postFailed(text);
            }).always(function() {
                delete that.request;
            });
        };

        this.progressUpdate = function(e, update) {
            if (update.progress == -1) {
                $($sp.Polling).off("tapingProgress." + this.id);
                this.onCancelled && this.onCancelled();
                return;
            }
            this.progress = update.progress;
            this.publishProgress && this.publishProgress(this.progress);
            if (this.progress === 100) {
                $($sp.Polling).off("tapingProgress." + this.id);
                _startReceive(update.path);
                this.postFinished && this.postFinished(update.path);
            }
        };

        _.bindAll(this, "progressUpdate");

        function _startReceive(path) {
            registerReceiveFileWorker({
                deviceId: that.deviceId,
                path: path
            });
        }
    }

    function FileReceiveWorker(options) {
        var deviceId = options.deviceId,
            path = options.path,
            callback = options.callback,
            worker = {};

        function init() {
            this.postInitialized && this.postInitialized();
            this.request = startTransaction();
        }

        function start(transaction, requestPath) {
            var idx = path.lastIndexOf("/"),
                filename = path.substring(idx + 1),
                url,
                that = this;
            url = requestPath || "http://" + transaction.relay_node;
            this.postFailed = options.postFailed;
            url += "/?transaction=recv" + transaction.id +
                "&filename=" + encodeURIComponent(filename);

            if (this.protocol === "unknown") {
                this.request = sendHead(this.deviceId, requestPath);
                return;
            }

            this.postStarted && this.postStarted();
            _receivedFileWorkers[transaction.id] = this;

            if (callback) {
                callback(url);
                return;
            }
            $("body").append(
                "<iframe style=\"height:0;width:0;border:none;\" src=\"" +
                url + "\"></iframe>");
            //$sp.Utils.downloadFile(url);
            //window.location.assign(url);
        }

        function startTransaction() {
            var device = $sp.DeviceManager.getCachedDeviceWithId(deviceId),
                handler;

            if (device.get("deviceType") !== "ANDROID") {
                handler = function(transaction) {
                    var path = transaction.path_from + transaction.filename,
                        key = deviceId + ":" + path,
                        worker = _receiveFileWorkers[key];
                    if (worker) {
                        delete worker.request;
                        worker.protocol = "relaying";
                        worker.start(transaction);
                    }
                };
            }
            return _startFilePullTransaction(deviceId, path, handler);
        }

        return $.extend(worker, {
            forceRelay: Boolean(options.forceRelay),
            path: path,
            init: init,
            start: start,
            startTransaction: startTransaction,
            deviceId: deviceId
        });
    }

    function FileSendWorker(options) {
        var file = options.file,
            toParentPath = options.toParentPath,
            fromParentPath = options.fromParentPath,
            deviceId = options.deviceId,
            deviceType = options.deviceType,
            formData,
            worker = {};

        function init() {
            this.postInitialized && this.postInitialized();
            this.request = startTransaction();
        }

        function createCORSRequest(method, url) {
            var xhr = new XMLHttpRequest();
            if ("withCredentials" in xhr) {
            // XHR for Chrome/Firefox/Opera/Safari.
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest !== "undefined") {
                // XDomainRequest for IE.
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                // CORS not supported.
                xhr = null;
            }
            return xhr;
        }

        function start(transaction, requestPath) {
            var xhr,
                that = this,
                url = requestPath ? requestPath :
                    "http://" + transaction.relay_node;

            if (this.protocol === "unknown") {
                this.request = sendHead(this.deviceId, requestPath);
                return;
            }
            this.postStarted && this.postStarted();

            formData = new FormData()
            formData.append("key", "send" + transaction.id);
            formData.append("filesize", file.size);
            formData.append("file", file);

            xhr = createCORSRequest("POST", url);
            xhr.upload.addEventListener("progress", function(e) {
                if (e.lengthComputable) {
                    var percentage;
                    percentage = Math.min(Math.round((e.loaded * 100) / file.size), 100);
                    that.postProgress && that.postProgress(percentage);
                }
            }, false);

            xhr.upload.addEventListener("load", function(e){
            }, false);

            xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
            xhr.send(formData);
            xhr.onreadystatechange = function(e) {
                if (xhr.readyState === 4 && xhr.statusText !== "abort") {
                    if (xhr.status === 200) {
                        that.postFinished && that.postFinished();
                    } else if (that.status !== "failed") {
                        that.postFailed && that.postFailed("failed to send");
                        that.status = "failed";
                    }
                }
            };
            this.request = xhr;
        };

        function startTransaction() {
            var device = $sp.DeviceManager.getCachedDeviceWithId(deviceId),
                handler;
            if (device.get("deviceType") !== "ANDROID") {
                handler = function(transaction) {
                    var path = transaction.path_to + transaction.filename,
                        key = deviceId + ":" + path,
                        worker = _sendFileWorkers[key];
                    if (worker) {
                        delete worker.request;
                        worker.protocol = "relaying";
                        worker.start(transaction);
                    }
                };
            }
            return _startFilePushTransaction(
                deviceId, toParentPath, fromParentPath, file.name, handler);
        }

        return $.extend(worker, {
            init: init,
            start: start,
            startTransaction: startTransaction,
            deviceId: deviceId,
            deviceType: deviceType
        });
    }

    function _prepareReceive(transaction) {
        var path = transaction.path_from + transaction.filename,
            key = transaction.sender + ":" + path,
            worker = _receiveFileWorkers[key];
        if (worker) {
            delete worker.request;
            worker.transaction = transaction;
        }
    }

    function _startReceive(transaction, requestPath) {
        var path = transaction.path_from + transaction.filename,
            key = transaction.sender + ":" + path,
            worker = _receiveFileWorkers[key];

        if (worker) {
            worker.start(transaction, requestPath);
        }
    }

    function _prepareSend(transaction) {
        var path = transaction.path_to + transaction.filename,
            key = transaction.receiver + ":" + path,
            worker = _sendFileWorkers[key];
        if (worker) {
            delete worker.request;
            worker.transaction = transaction;
        }
    }

    function _startSend(transaction) {
        var path = transaction.path_to + transaction.filename,
            key = transaction.receiver + ":" + path,
            worker = _sendFileWorkers[key];

        if (worker) {
            worker.start(transaction);
        }
    }

    function _startFileTransaction(data, handler) {
        return $.ajax({
            url: $sp.Remote.delta.START_FILE_TRANSACTION,
            data: data,
            type: "POST",
            dataType: "json"
        }).fail(function(res, textStatus, xhr) {
            if (textStatus !== "abort") {
                console.error(res);
            }
        }).done(function(res, statusText, jqXHR) {
            handler && handler(res.transaction);
        });
    }

    function _startFilePullTransaction(deviceId, path, handler) {
        var idx = path.lastIndexOf("/"),
            filename = path.substring(idx + 1),
            parentPathFrom = path.substring(0, idx+1),
            data = {
                filename: filename,
                peer_id: deviceId,
                type: "pull",
                path_from: parentPathFrom,
                path_to: ""
            };
        return _startFileTransaction(data, handler ? handler : _prepareReceive);
    }

    function _startFilePushTransaction(
        deviceId, parentPathTo, parentPathFrom, filename, handler) {
        var data = {
                filename: filename,
                peer_id: deviceId,
                type: "push",
                path_from: parentPathFrom,
                path_to: parentPathTo
            };
        return _startFileTransaction(data, handler ? handler : _prepareSend);
    }

    function registerReceiveFileWorker(options) {
        var worker,
            deviceId, path, callback,
            key;

        deviceId = options.deviceId;
        path = options.path;
        key = deviceId + ":" + path;

        if (_receiveFileWorkers[key]) {
            return false;
        }

        worker  = new FileReceiveWorker(options);


        worker.key = key;
        worker.postInitialized = function() {
        };

        worker.postStarted = function() {
            delete _receiveFileWorkers[this.key]
        }

        _receiveFileWorkers[key] = worker;
        worker.init();
    }

    function registerReceiveCompressedFileWorker(deviceId, paths, filter, handler) {
        var worker,
            key,
            seed;
        seed = deviceId + ">" + $sp.context.account.deviceId;
        paths.sort();
        seed += "[" + paths.join(",") + "]";

        key = md5(seed);

        if ((worker = _receiveCompressedFileWorker[key])) {
            $sp.confirm({
                title: $sp.string("are_you_sure"),
                text: "already have compressed job. Proceed?"
            }, function(confirmed) {
                var _onCancelled;
                if (confirmed) {
                    _onCancelled = worker.onCancelled;
                    worker.onCancelled = function() {
                        _onCancelled.call(worker);
                        registerReceiveCompressedFileWorker(deviceId, paths, handler);
                    }
                    worker.cancel();
                }
            })
            return;
        }
        worker = new CompressedFileReceiveWorker({
            deviceId: deviceId,
            paths: paths,
            filter: filter,
            id: key,
            postInit: function() {
                handler && handler.onInit &&
                    handler.onInit(worker.id, worker.paths);
            },
            postCancel: function() {
                handler && handler.onCanceling &&
                    handler.onCanceling(worker.id, worker.paths);
            },
            onCancelled: function() {
                delete _receiveCompressedFileWorker[key];
                handler && handler.onCancelled &&
                    handler.onCancelled(worker.id, worker.paths);
            },
            publishProgress: function(progress) {
                handler && handler.publishProgress &&
                    handler.publishProgress(
                        worker.id, worker.paths, worker.progress);
            },
            postFinished: function(path) {
                delete _receiveCompressedFileWorker[key];
                handler && handler.onFinished &&
                    handler.onFinished(worker.id, worker.paths, path);
            },
            postFailed: function(msg) {
                delete _receiveCompressedFileWorker[key];
                handler && handler.onFailed &&
                    handler.onFailed(worker.id, worker.paths, msg);
            }
        });
        _receiveCompressedFileWorker[key] = worker;
        worker.init();
    }

    function hasSendFileWorker(deviceId, path) {
        var key = deviceId + ":" + path;
        return Boolean(_sendFileWorkers[key] &&
            _sendFileWorkers[key].status !== "failed");
    }


    function registerSendFileWorker(obj) {
        var deviceId = obj.deviceId,
            deviceType = obj.deviceType,
            toParentPath = obj.toParentPath,
            fromParentPath = obj.fromParentPath,
            handler = obj.handler,
            context = obj.context,
            key;

        key = deviceId + ":" + toParentPath + obj.file.name;

        if (_sendFileWorkers[key] && _sendFileWorkers[key].status !== "failed") {
            return false;
        }
        if (_sendFileWorkers[key] && _sendFileWorkers[key].status === "failed") {
            _sendFileWorkers[key].onRemoveFailedTask(key);
            delete _sendFileWorkers[key];
        }

        var worker = new FileSendWorker(obj);
        _sendFileWorkers[key] = worker;

        worker.key = key;

        worker.postInitialized = function() {
            handler.onInitialized.apply(context, [this.key]);
        };
        worker.postStarted = function() {
        };
        worker.postProgress = function(percentage) {
            handler.onProgress.apply(context, [this.key, percentage]);
        };
        worker.postFinished = function() {
            handler.onFinished.apply(context, [key]);
            delete _sendFileWorkers[this.key];
            delete _sendingWorkers[this.key];
            initNextSendWorker();
        }
        worker.postFailed = function(msg) {
            handler.onFailed.apply(context, [this.key, msg]);
            //delete _sendFileWorkers[this.key];
            delete _sendingWorkers[this.key];
            initNextSendWorker();
        };
        worker.onRemoveFailedTask= function() {
            handler.removeFailedTask.apply(context, [key]);
        };
        worker.onRestarted = function() {
            handler.onRestarted.apply(context, [key]);
        }
        worker.onWait = function() {
            handler.onWait.apply(context, [key]);
        }
        worker.onCancelled = function() {
            handler.onCancelled.apply(context, [key]);
        }

        if (worker.deviceType === "WINDOWS") {
            var windowWorkerCount = 0;
            $.each(_sendingWorkers, function(key, worker) {
                if (worker.deviceType === "WINDOWS") {
                    windowWorkerCount += 1;
                }
            });
            if (windowWorkerCount >= CONCURRENT_WINDOW_WORKER_MAX) {
                worker.onWait();
                _sendWaitingWorkerQueue.push(worker);
                return true;
            }
        }
        if (Object.keys(_sendingWorkers).length < CONCURRENT_WORKER_MAX) {
            _sendingWorkers[key] = worker;
            worker.init();
        } else {
            worker.onWait();
            _sendWaitingWorkerQueue.push(worker);
        }

        return true;
    }

    function hasRegisteredWorker() {
        return Boolean(Object.keys(_sendFileWorkers).length ||
                      Object.keys(_receiveFileWorkers).length);
    }

    function webP2pNegotiationHandler(res) {
        if (res.protocol !== "relaying") {
            return;
        }
        if (haveActiveTransactionWith(res.sender)) {
            if (_peerProtocolCacheMap[res.sender]) {
                $.each(_peerProtocolCacheMap[res.sender], function(key, val) {
                    _peerProtocolCacheMap[res.sender][key] = "relaying";
                });
            }

            module.intercom.emit("protocolCacheMapUpdate", {
                type: "put",
                deviceId: res.sender,
                protocol: "relaying"
            });
            restartAllTransferAsProtocol(res.sender, "relaying");
        }
    }

    function webP2pAddressHandler(res) {
        var requestUrl, key;
        key = res["public"] + "_" + res["private"];
        if (res["public"] === $sp.context.publicIp &&
            (!_peerProtocolCacheMap[res.sender] ||
                !_peerProtocolCacheMap[res.sender][key] ||
                _peerProtocolCacheMap[res.sender][key] !== "relaying")) {

            if (!_peerProtocolCacheMap[res.sender] ||
                !_peerProtocolCacheMap[res.sender][key]) {

                _peerProtocolCacheMap[res.sender] = {};
                _peerProtocolCacheMap[res.sender][key] = "unknown";

                module.intercom.emit("protocolCacheMapUpdate", {
                    type: "put",
                    deviceId: res.sender,
                    key: key,
                    protocol: "unknown"
                });
            }
            requestUrl = "http://" + res["private"] + ":" + WEB_P2P_PORT;
        } else {

            _peerProtocolCacheMap[res.sender] = {};
            _peerProtocolCacheMap[res.sender][key] = "relaying";

            module.intercom.emit("protocolCacheMapUpdate", {
                type: "put",
                deviceId: res.sender,
                key: key,
                protocol: "relaying"
            });
            if (haveActiveTransactionWith(res.sender)) {
                sendNegotiate(res.sender);
            }
        }
        if (haveActiveTransactionWith(res.sender)) {
            restartAllTransferAsProtocol(
                res.sender,
                _peerProtocolCacheMap[res.sender][key],
                requestUrl
            );
        }
    }

    function haveActiveTransactionWith(deviceId) {
        var haveActiveTransaction = false;
        $.each(_receiveFileWorkers, function(path, worker) {
            if (worker.deviceId === deviceId && worker.transaction) {
                haveActiveTransaction = true;
                return false;
            }
        });
        $.each(_sendingWorkers, function(path, worker) {
            if (worker.deviceId === deviceId && worker.transaction) {
                haveActiveTransaction = true;
                return false;
            }
        });
        return haveActiveTransaction;
    }

    function notifyFailureHandler(res) {
        var worker = _receivedFileWorkers[res.transaction],
            filename,
            idx;
        if (worker) {
            idx = worker.path.lastIndexOf("/");
            filename = idx > -1 ? worker.path.substring(idx + 1) : worker.path;
            $sp.notify($sp.string("failed_to_download", filename), "error");
        }
        $.each(_sendingWorkers, function(path, worker) {
            if (worker.transaction &&
                    worker.transaction.id === res.transaction &&
                    worker.request) {
                worker.request.abort();
                worker.postFailed && worker.postFailed("failed to send");
                delete worker.postFailed;
                return false;
            }
        });
    }

    function sendNegotiate(deviceId, onSuccess, onFailure) {
        return $.ajax({
            url: $sp.Remote.delta.PUSH,
            data: JSON.stringify(
                {
                    delta: {
                        type: "negotiation",
                        protocol: "relaying",
                        receiver: deviceId
                    }
                }),
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(function(res, textStatus, xhr) {
            onSuccess && onSuccess(res, textStatus, xhr);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.error(jqXHR.responseText);
            onFailure && onFailure(jqXHR, textStatus, errorThrown);
        });
    }

    function sendHead(deviceId, url) {
        $.each(_headSendListeners, function(idx, listener) {
            listener.onStart(deviceId);
        });
        return $.ajax({
                url: url,
                type: "HEAD",
                timeout: 10000
            }).done(function(res) {
                $.each(_peerProtocolCacheMap[deviceId], function(key, val) {
                    _peerProtocolCacheMap[deviceId][key] = "p2p";
                });
                module.intercom.emit("protocolCacheMapUpdate", {
                    type: "put",
                    deviceId: deviceId,
                    protocol: "p2p"
                });
                restartAllTransferAsProtocol(deviceId, "p2p", url);
            }).fail(function(xhr, textStatus) {
                if (textStatus === "timeout") {
                    $.each(_peerProtocolCacheMap[deviceId], function(key, val) {
                        _peerProtocolCacheMap[deviceId][key] = "relaying";
                    });
                    module.intercom.emit("protocolCacheMapUpdate", {
                        type: "put",
                        deviceId: deviceId,
                        protocol: "relaying"
                    });
                    sendNegotiate(deviceId);
                    restartAllTransferAsProtocol(deviceId, "relaying");
                }
            }).always(function() {
                $.each(_headSendListeners, function(idx, listener) {
                    listener.onFinished(deviceId);
                });
            });
    }

    function restartAllTransferAsProtocol(deviceId, protocol, requestUrl) {
        $.each(_receiveFileWorkers, function(path, worker) {
            if (worker.deviceId === deviceId && worker.transaction) {
                if (protocol !== "unknown" &&
                        worker.request && worker.protocol === "unknown") {
                    worker.request.abort();
                    delete worker.request;
                }
                if (!worker.request) {
                    if (worker.forceRelay && protocol !== "relaying") {
                        worker.protocol = "relaying";
                        sendNegotiate(deviceId, function() {
                                worker.start(worker.transaction);
                            }, function() {
                                $sp.notify("Failed to receive file", "error");
                            });
                        return;
                    }
                    worker.protocol = protocol;
                    worker.start(worker.transaction, requestUrl);
                }
            }
        });
        $.each(_sendingWorkers, function(path, worker) {
            if (worker.deviceId === deviceId && worker.transaction) {
                if (protocol !== "unknown" &&
                        worker.request && worker.protocol === "unknown") {
                    worker.request.abort();
                    delete worker.request;
                }
                if (!worker.request) {
                    if (worker.forceRelay && protocol !== "relaying") {
                        worker.protocol = "relaying";
                        sendNegotiate(deviceId, function() {
                                worker.start(worker.transaction);
                            }, function() {
                                $sp.notify("Failed to receive file", "error");
                            });
                        return;
                    }
                    worker.protocol = protocol;
                    worker.start(worker.transaction, requestUrl);
                }
            }
        });
    }


    function addHeadSendListener(listener) {
        _headSendListeners.push(listener);
    }

    function removeHeadSendListener(listener) {
        var idx = _headSendListeners.indexOf(listener);
        if (idx > -1) {
            _headSendListeners.splice(idx, 1);
        }
    }

    function restartSendFileWorker(key) {
        var worker = _sendFileWorkers[key];
        if (!worker) {
            $sp.notify("Failed to find proper send worker", "error");
            return;
        }
        worker.onRestarted();
        worker.status = null;
        worker.request = null;

        if (worker.deviceType === "WINDOWS") {
            var windowWorkerCount = 0;
            $.each(_sendingWorkers, function(key, worker) {
                if (worker.deviceType === "WINDOWS") {
                    windowWorkerCount += 1;
                }
            });
            if (windowWorkerCount >= CONCURRENT_WINDOW_WORKER_MAX) {
                worker.onWait();
                _sendWaitingWorkerQueue.push(worker);
                return;
            }
        }
        if (Object.keys(_sendingWorkers).length < CONCURRENT_WORKER_MAX) {
            _sendingWorkers[key] = worker;
            worker.init();
        } else {
            worker.onWait();
            _sendWaitingWorkerQueue.push(worker);
        }
    }

    function cancelSendFileWorker(key) {
        var worker = _sendFileWorkers[key],
            idx, i,
            isCancelled = false;

        if (!worker) {
            return;
        }

        // remove from waiting queue
        idx = _sendWaitingWorkerQueue.indexOf(worker);
        if (idx > -1) {
            _sendWaitingWorkerQueue.splice(idx, 1);
            isCancelled = true;
        }

        // remove from
        if (!isCancelled && _sendingWorkers[worker.key]) {
            if (worker.request) {
                delete worker.request;
            }
            delete _sendingWorkers[worker.key];
            initNextSendWorker();
        }

        delete _sendFileWorkers[worker.key];
        worker.onCancelled();
    }

    function cancelCompressWorker(key) {
        var worker = _receiveCompressedFileWorker[key];
        if (worker) {
            worker.cancel();
        }
    }

    function initNextSendWorker() {
        var nextWorker,
            found, idx;
        for (i = 0; i < _sendWaitingWorkerQueue.length; i++) {
            nextWorker = _sendWaitingWorkerQueue[i];
            if (nextWorker.deviceType === "WINDOWS") {
                var windowWorkerCount = 0;
                $.each(_sendingWorkers, function(key, worker) {
                    if (worker.deviceType === "WINDOWS") {
                        windowWorkerCount += 1;
                    }
                });
                if (windowWorkerCount >= CONCURRENT_WINDOW_WORKER_MAX) {
                    continue;
                }
            }
            found = nextWorker;
            break;
        }
        if (found) {
            idx = _sendWaitingWorkerQueue.indexOf(found);
            if (idx > -1) {
                _sendWaitingWorkerQueue.splice(idx, 1);
            }
            _sendingWorkers[found.key] = found;
            found.init();
        }
    }

    module = module || {};
    module.context = module.context || {};
    module.context.peerProtocolCacheMap = _peerProtocolCacheMap;

    module.FileTransfer = {
        registerReceiveFileWorker: registerReceiveFileWorker,
        registerReceiveCompressedFileWorker: registerReceiveCompressedFileWorker,
        registerSendFileWorker: registerSendFileWorker,
        hasSendFileWorker: hasSendFileWorker,
        hasRegisteredWorker: hasRegisteredWorker,
        webP2pNegotiationHandler: webP2pNegotiationHandler,
        webP2pAddressHandler: webP2pAddressHandler,
        notifyFailureHandler: notifyFailureHandler,
        addHeadSendListener: addHeadSendListener,
        removeHeadSendListener: removeHeadSendListener,
        restartSendFileWorker: restartSendFileWorker,
        cancelSendFileWorker: cancelSendFileWorker,
        cancelCompressWorker: cancelCompressWorker
    };
    return module;
} ($sp));
