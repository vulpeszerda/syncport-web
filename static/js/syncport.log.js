var $sp = (function(module) {

    module.clickLog = function(category, type) {
        var path = "/click/" + category + "/" + type;
        if (ga) {
            ga("send", "pageview", path);
        }
    };

    $(function() {
        $("body").on("click", "[data-log-click]", function() {
            var type = $(this).data("logClick"),
                $category = $(this).parents("[data-log-category]"),
                category = $category.length ? $category.data("logCategory") : "default";
            module.clickLog(category, type);
            return true;
        });
    });

    module = module || {};
    return module;
}($sp));
