var $sp = $sp || {};

$sp.notify = function(msg, type, options) {
    var klass,
        icHtml,
        opt = {},
        notifyObj = {};
    if (type === "success") {
        klass = "glyphicons-ok";
    } else if (type === "info") {
        klass = "glyphicons-circle-info";
    } else if (type === "error") {
        klass = "glyphicons-alert";
    } else if (type === "warn") {
        klass = "glyphicons-warning";
    }
    if (msg instanceof Object) {
        $.extend(notifyObj, msg);
    } else {
        notifyObj["msg"] = msg;
    }
    if (!options || !options.ignoreIcon) {
        notifyObj["ic"] = "<i style='margin-right:5px' class='glyphicons " + klass + "'></i>";
    }
    if (options) {
        $.extend(opt, options);
    }
    opt["className"] = type;
    $.notify(notifyObj, opt);
};

$sp.moment = (function() {
    var locale = $sp.context.locale || "en_US";
    if (locale === "ko_KR") {
        moment.locale("ko", {
            longDateFormat : {
                LT : 'A h:mm ',
                LTS : 'A h:m:ss ',
                L : 'YYYY.MM.DD',
                LL : 'YYYY년 MM월 D일',
                LLL : 'YYYY년 MM월 D일 LT',
                LLLL : 'YYYY년 MM월 D일 dddd LT'
            }
        });
    } else {
        moment.locale(locale);
    }
    return moment;
}())

$sp.init = function() {
    var browserSupportTmpl;

    $sp.context = $sp.context || {};

    $sp.context.debug = false;
    $sp.context.isMobile = $sp.Utils.isMobile;
    $sp.context.browser = getBrowser();
    if (!$sp.context.browser.support) {
        browserSupportTmpl = _.template($("#tmpl-browser-support").html());
        $("#noti-header").html(browserSupportTmpl($sp.context.browser));
    }
    $sp.context.os = $sp.Utils.os;
    $sp.AccountManager.getAccount(function(account) {
        if (account) {
            $sp.authService.init(account, function() {
                $sp.authService.start();
                Backbone.history.start();
            });
        } else {
            Backbone.history.start();
        }
    });

    $.getJSON("https://api.ipify.org/?format=json", function(data){
        $sp.context.publicIp = data.ip;
        if ($.cookie("myip")) {
            $sp.context.publicIp = $.cookie("myip");
        }
    });

    function getBrowser() {
        var browserVersion,
            browser,
            version;
        browserVersion = $sp.Utils.browserVersion.split(" ");
        browser = browserVersion[0];
        version = browserVersion[1];
        if (browser == 'MSIE') {
            version = Number(version);
            if (version < 10) {
                return {
                    support: false,
                    currentBrowser: $sp.Utils.browserVersion
                };
            }
        }
        return {
            support: true,
            currentBrowser: browserVersion
        };
    }
};

$sp.authService = (function (){
    var prevAccount,
        cprTimer,
        CPR_INTERVAL = 1 * 1000 * 60,
        isCPRTimerRunning = false,
        cprRequest;

        function registerCPRTimer() {
            isCPRTimerRunning = true;
            if (cprRequest) {
                cprRequest.abort();
            }
            if (cprTimer) {
                clearTimeout(cprTimer);
            }
            sendCPR(function() {
                if (cprTimer) {
                    clearTimeout(cprTimer);
                }
                cprTimer = setTimeout(function() {
                    if (isCPRTimerRunning && $sp.Utils.isVis()) {
                        registerCPRTimer();
                    }
                }, CPR_INTERVAL);
            });
        }

        function unregisterCPRTimer() {
            isCPRTimerRunning = false;
            if (cprRequest) {
                cprRequest.abort();
            }
            if (cprTimer) {
                clearTimeout(cprTimer);
                cprTimer = null;
            }
            sendRCPR();
        }

        function sendCPR(callback) {
            $sp.AccountManager.getAccount(function(account) {
                var devices, deviceIds;
                if (account && isCPRTimerRunning && $sp.Utils.isVis()) {
                    devices = $sp.DeviceManager.getDeviceCollection()
                        .where({"deviceType" :"ANDROID"});
                    if (devices.length > 0) {
                        // need to send cpr to androids
                        deviceIds = $.map(devices, function(device) {
                            return device.get("deviceId");
                        });
                        cprRequest = $.ajax({
                            url: $sp.Remote.api.CPR,
                            data: JSON.stringify({
                                device_ids: deviceIds
                            }),
                            dataType: "json",
                            type: "post",
                            contentType: "application/json"
                        }).done(function() {
                            // do nothing
                            callback && callback();
                        });
                    } else {
                        callback && callback();
                    }
                }
                if (!account || prevAccount.email !== account.email) {
                    location.reload();
                }
            }, true);
        }

        function sendRCPR() {
            $sp.AccountManager.getAccount(function(account) {
                if (!account) {
                    return;
                }
                devices = $sp.DeviceManager.getDeviceCollection()
                    .where({"deviceType" :"ANDROID"});
                if (devices.length > 0) {
                    // need to send cpr to androids
                    deviceIds = $.map(devices, function(device) {
                        return device.get("deviceId");
                    });
                    $.ajax({
                        url: $sp.Remote.api.RCPR,
                        data: JSON.stringify({
                            device_ids: deviceIds
                        }),
                        dataType: "json",
                        type: "post",
                        contentType: "application/json"
                    }).done(function() {
                        // do nothing
                    });
                }
            });
        }

        visHandler = function() {
            var isVisible = $sp.Utils.isVis();
            if (!isVisible) {
                // need to send rcpr to androids

                // XXX: this function should be called after 1sec
                // because when other syncport tab is selected,
                // that tab will call getSyncportTabCount function,
                // it will affect this getSyncportTabCount. so wait 1sec
                setTimeout(function() {
                    $sp.Utils.getSyncportTabCount(
                        function(count, isSyncportTabSelected) {
                            if (!isSyncportTabSelected) {
                                unregisterCPRTimer();
                            }
                        });
                }, 1000);
                return;
            }
            registerCPRTimer();
        };
    return {
        init: function(account, callback) {
            prevAccount = account;
            $sp.Utils.listenVisChange(visHandler);
            $sp.context.account = account;
            $sp.DeviceManager.init(account, callback);
        },
        start: function() {
            $sp.Utils.getSyncportTabCount(function(count, isSyncportTabSelected) {
                if ($sp.Utils.isVis()) {
                    registerCPRTimer();
                }
                // if opened Syncport tab is more than 1,
                // no need to resend connect
                if (count > 1) {
                    $sp.Polling.client.start();
                    return;
                }
                $sp.Polling.client.start(true);
            });

            // initialize global auth views
            $sp.view = $sp.view || {};

            if (!$sp.view.sendFilesView) {
                $sp.view.sendFilesView = new $sp.View.SendFilesView();
            }
            if (!$sp.view.downloadFilesView) {
                $sp.view.compressingProgressView =
                    new $sp.View.CompressingProgressView();
            }
        },
        stop: function(callback) {
            var view;

            $sp.Polling.client.stop(callback, true);
            $sp.Utils.stopListenVisChange(visHandler);

            // terminate global auth views
            view = $sp.view.sendFilesView;
            view && (view.close ? view.close() : view.remove());

            view = $sp.view.compressingProgressView;
            view && (view.close ? view.close() : view.remove());

            unregisterCPRTimer();
        }
    };
}());

$(function(){

    window.ondragstart = function() { return false; }
    window.onunload = function() {
        $sp.intercom.emit("pollEnd", {});
    }
    window.onbeforeunload = function (e) {
        var text = $sp.string("has_transfer_task");
        if ($sp.FileTransfer && $sp.FileTransfer.hasRegisteredWorker()) {
            e = e || window.event;

            //IE & Firefox
            if (e) {
                e.returnValue = text;
            }

            // For Safari
            return text;
        }
    };
    $sp.init();

});

// Global Ajax settings
$.ajaxSetup({
    dataType: "JSON",
    beforeSend: function(xhr, settings) {
        var csrftoken = $.cookie('csrftoken');
        function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


