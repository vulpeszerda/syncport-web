from django.conf.urls import patterns, include, url
import home.views as home_views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'syncport.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', home_views.main, name='main'),
    url(r'^beta_tester/?$', home_views.beta, name='beta'),
    url(r'^signin/?$', home_views.signin, name='signin'),
    url(r'^signup/?$', home_views.signup, name='signup'),
    url(r'^signout/?$', home_views.signout, name='signout'),
    url(r'^register_tester/?$', home_views.register_beta, name='register-tester'),
    url(r'^terms/?$', home_views.term, name='terms'),
    url(r'^change-password/?$', home_views.change_password, name='change-password'),
)
