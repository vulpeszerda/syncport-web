from django.contrib.gis.geoip import GeoIP
from django.shortcuts import render, redirect

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

class LocaleMiddleware(object):
    def process_request(self, request):
        locale = None
        if request.GET.has_key("locale"):
            locale = request.GET.get("locale")
        elif request.GET.has_key("fb_locale"):
            locale = request.GET.get("fb_locale")

        if locale is not None and \
                locale not in [u'ko_KR', u'en_US']:
            locale = None

        if locale is None:
            g = GeoIP()
            ip = get_client_ip(request)
            country = g.country(ip).get('country_code', None)
            if country == 'KR':
                locale = 'ko_KR'
            else:
                locale = 'en_US'
        request.locale = locale
        
