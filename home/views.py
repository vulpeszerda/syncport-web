import re
import json

from django.contrib.gis.geoip import GeoIP
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from home.decorator import jsonp

from utilities.mail import (
    global_email_sender, greeting_register_msg,
    greeting_register_subj
)



@ensure_csrf_cookie
def main(request):
    return render(request, 'main.html')

@ensure_csrf_cookie
def signin(request):
    return render(request, 'main.html')

@ensure_csrf_cookie
def signup(request):
    return render(request, 'main.html')

@ensure_csrf_cookie
def change_password(request):

    data = {}

    if request.GET.has_key("k"):
        key = request.GET.get("k")
        key = key[::-1]
        key = key[16:] + key[:16]
        data["temp_session_key"] = key 

    return render(request, 'main.html', data)

@ensure_csrf_cookie
def beta(request):
    request.locale = 'ko_KR'
    data = dict(
            use_beta_desc = True
        )

    return render(request, 'main.html', data)


def term(request):
    return render(request, 'term.html')

@jsonp
def signout(request):
    response = HttpResponse('{\"success\":true}', content_type='application/json')
    response.delete_cookie('sessionid');
    return response

class EmailHttpResponse(HttpResponse):
    """Just use for registering of beta-tester.

    """
    def __init__(self, *args, **kwargs):
        self._email = kwargs.pop("email", "")
        super(EmailHttpResponse, self).__init__(*args, **kwargs)

    def close(self):
        super(EmailHttpResponse, self).close()
        global_email_sender.send_email(
                self._email, greeting_register_subj, greeting_register_msg
            )


def register_beta(request):
    success = False
    msg = 'undefined'

    if request.POST.has_key('email'):
        email = request.POST['email']
        if not re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email):
            msg = 'invalid email format'
        else:
            f = open('/vol/src/syncport-dev/beta_testers', 'a')
            f.write(email + '\n')
            f.close()
            success = True
    else:
        msg = 'required email input'

    response_dict = dict(
            success = success,
            msg = msg
        )

    resp = EmailHttpResponse(
            json.dumps(response_dict), mimetype='application/json',
            email=email
        )

    return resp
