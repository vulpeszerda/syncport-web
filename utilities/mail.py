# -*- coding: utf-8 -*-
import smtplib
from email.header import Header


class EmailSender(object):
    def __init__(self, email, passwd):
        self._email = email
        self._passwd = passwd

    def send_email(self, to, subject, content):
        session = smtplib.SMTP('smtp.gmail.com', 587)
        session.ehlo()
        session.starttls()
        session.ehlo()
        session.login(self._email, self._passwd)
        self._session = session

        headers = "\r\n".join(
            ["from: " + self._email,
            "subject: " + Header(subject, 'utf-8').encode(),
            "to: " + to,
            "mime-version: 1.0",
            "content-type: text/html; charset=UTF-8"]
        )

        raw = headers + "\r\n\r\n" + content
        self._session.sendmail(self._email, to, raw.encode("utf8"))

    def __del__(self):
        self._session.close()


global_email_sender = EmailSender('needy.hacker@gmail.com', 'qkznlqjffp')

greeting_register_subj = u"""안녕하세요. Syncport 베타테스터 등록해주신분들께 드리는 메일입니다."""
#greeting_register_subj = 'Syncport beta-tester registration completed!'

greeting_register_msg = u"""
안녕하세요. Syncport team 입니다.<br/><br/>
먼저, 베타테스트를 신청 해 주셔서 감사합니다.<br/>
베타에는 미리 말씀드렸던대로 안드로이드앱과 웹사이트를 공개합니다.<br/>
안드로이드 앱을 설치한 후에 웹사이트에 들어가시면 웹을 통해서 안드로이드에 파일을 넣거나 가져올 수 있습니다.<br/>
혹은 앱을 설치한 여러 안드로이드 기기간에 파일을 교환할 수 있습니다.<br/>
베타테스트에 참여하시는 방법은 다음과 같습니다.<br/><br/>

1. 웹사이트에서 회원 가입을 합니다.<br/><br/>
<a href='http://syncport.io/#signup-for-beta'>http://syncport.io/#signup-for-beta</a><br/>
이 링크를 통해서 회원가입 페이지로 이동하실 수 있습니다.<br/><br/>

2. 안드로이드에 앱을 설치하고 로그인합니다.<br/>
앱은 현재 플레이 스토어에 등록되어있습니다.<br/><br/>
<a href='https://play.google.com/store/apps/details?id=io.syncport'>https://play.google.com/store/apps/details?id=io.syncport</a><br/>
(앱 이름에 syncport로 검색해도 나옵니다.)<br/><br/>
앱에서는 현재 회원가입을 막아놓은 상태이기 때문에 회원가입은 웹사이트에서 해주셔야 됩니다.<br/>
앱에 로그인까지 하셨으면 http://syncport.io 에서 핸드폰의 파일들을 보실 수 있습니다.<br/><br/>

<a href='http://www.youtube.com/watch?v=SjVdWPhhrh0'>http://www.youtube.com/watch?v=SjVdWPhhrh0</a><br/>
저희가 찍은 데모 시연 영상입니다. 한번 봐 주시면 감사하겠습니다.<br/>
웹사이트 우측 상단에 피드백을 보내실 수 있는 기능이 있습니다.<br/>
써보시고 서비스의 기능상 문제점이나 불편한 점 등에 대해 간단하게 적어주시면 앞으로 저희가 좋은 제품을 만드는데에 크게 도움이 될 것 같습니다.<br/>
감사합니다.<br/><br/>
"""
